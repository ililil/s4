#define UNICODE
#define _USE_MATH_DEFINES
#define WIN32_LEAN_AND_MEAN // Exclude rarely-used stuff from Windows headers

#include <SDKDDKVer.h>
#include <windows.h>
#include <stdlib.h>
#include <math.h>

#define szTitle L"3d"
#define szWindowClass L"3d"

#define refreshTime 70 // 16 ms

#define bgImageWidth 200
#define bgImageHeight 158

#define ImageWidth 755
#define ImageHeight 456

HINSTANCE hInst = NULL;

HBITMAP bg = NULL;
HBITMAP bmw[5] = {0};

int frame = 0;
int framek = 1;

BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	bg = (HBITMAP) LoadImage(NULL, L"bg.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	bmw[0] = (HBITMAP) LoadImage(NULL, L"1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	bmw[1] = (HBITMAP) LoadImage(NULL, L"2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	bmw[2] = (HBITMAP) LoadImage(NULL, L"3.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	bmw[3] = (HBITMAP) LoadImage(NULL, L"4.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	bmw[4] = (HBITMAP) LoadImage(NULL, L"5.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	if (!InitInstance(hInstance, nCmdShow))
		return 1;

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	DeleteObject(bmw);
	for (int i = 0; i < 5; i++)
		DeleteObject(bmw);

	return (int) msg.wParam;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
	WNDCLASSEXW wcex = {0};

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, IDI_WINLOGO);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	wcex.lpszMenuName = L"Menu";
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(hInstance, IDI_WINLOGO);

	RegisterClassEx(&wcex);
	hInst = hInstance;

	HWND hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
		return FALSE;

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		case WM_COMMAND: {
			int wmId = LOWORD(wParam);
			switch (wmId) {
				default: {
					return DefWindowProc(hWnd, message, wParam, lParam);
				}
			}
			break;
		}

		case WM_PAINT: {
			RECT rect;
			GetWindowRect(hWnd, &rect);
			int windowWidth = rect.right - rect.left;
			int windowHeight = rect.bottom - rect.top;

			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			HDC bgImageDC = CreateCompatibleDC(hdc);

			SelectObject(bgImageDC, bg);
			for (int x0 = 0; x0 < windowWidth + bgImageWidth; x0 += bgImageWidth)
				for (int y0 = 0; y0 < windowHeight + bgImageHeight; y0 += bgImageHeight)
					BitBlt(hdc, x0, y0, bgImageWidth, bgImageHeight, bgImageDC, 0, 0, SRCCOPY);

			while (1) {
				SelectObject(bgImageDC, bmw[frame]);

				BitBlt(hdc, 0, 0, ImageWidth, ImageHeight, bgImageDC, 0, 0, SRCINVERT);
				Sleep(refreshTime);
				BitBlt(hdc, 0, 0, ImageWidth, ImageHeight, bgImageDC, 0, 0, SRCINVERT);

				frame += framek;
				if (frame <= 0 || frame >= 4)
					framek *= -1;
			}

			DeleteDC(bgImageDC);
			EndPaint(hWnd, &ps);
			break;
		}

		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}

		default: {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	return 0;
}
