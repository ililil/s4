#include <iostream>
#include <string>

using namespace std;

class Product {
private:
	string name;
	string manufacturer;
	double price;
	int shelfLife;
	int quantity;

public:
	Product() :
			Product("", "", 0.0, 0, 0) {
	}

	Product(string n, string m, double p, int s, int q) :
			name(n), manufacturer(m), price(p), shelfLife(s), quantity(q) {
	}

	Product(Product &other) :
			name(other.name), manufacturer(other.manufacturer), price(other.price), shelfLife(other.shelfLife), quantity(other.quantity) {
	}

	~Product() {
	}

	void setName(string n) {
		name = n;
	}

	string getName() {
		return name;
	}

	void setManufacturer(string m) {
		manufacturer = m;
	}

	string getManufacturer() {
		return manufacturer;
	}

	void setPrice(double p) {
		price = p;
	}

	double getPrice() {
		return price;
	}

	void setShelfLife(int s) {
		shelfLife = s;
	}

	int getShelfLife() {
		return shelfLife;
	}

	void setQuantity(int q) {
		quantity = q;
	}

	int getQuantity() {
		return quantity;
	}
};

void printp(Product p) {
	cout << "Product: " << p.getName();
	cout << ", Manufacturer: " << p.getManufacturer();
	cout << ", Price: " << p.getPrice();
	cout << ", Shelf Life: " << p.getShelfLife();
	cout << ", Quantity: " << p.getQuantity() << endl;
}

int main() {
	int size = 3;
	Product products[size];

	products[0] = Product("Apple", "Apple Inc.", 1.5, 7, 100);
	products[1] = Product("Banana", "Banana Co.", 1.0, 5, 50);
	products[2] = Product("Orange", "Orange Ltd.", 2.0, 10, 75);

	// а) список товаров для заданного наименования
	string searchName = "Banana";
	for (int i = 0; i < size; i++)
		if (products[i].getName() == searchName)
			printp(products[i]);

	// б) список товаров для заданного наименования, цена которых не превышает указанной
	double maxPrice = 1.5;
	for (int i = 0; i < size; i++)
		if (products[i].getName() == searchName && products[i].getPrice() <= maxPrice)
			printp(products[i]);

	// в) список товаров, срок хранения которых больше заданного
	int minShelfLife = 7;
	for (int i = 0; i < size; i++)
		if (products[i].getShelfLife() > minShelfLife)
			printp(products[i]);

	return 0;
}
