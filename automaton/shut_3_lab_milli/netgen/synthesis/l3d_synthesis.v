////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2007 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: J.30
//  \   \         Application: netgen
//  /   /         Filename: l3d_synthesis.v
// /___/   /\     Timestamp: Fri May 24 14:19:34 2024
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/synthesis -ofmt verilog -sim l3d.ngc l3d_synthesis.v 
// Device	: xc3s50-4-tq144
// Input file	: l3d.ngc
// Output file	: D:\avto\3\shut_3_lab\netgen\synthesis\l3d_synthesis.v
// # of Modules	: 1
// Design Name	: l3d
// Xilinx        : C:\Xilinx91i
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Development System Reference Guide, Chapter 23
//     Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module l3d (
  CLK, CE, Y, RST_INPUT, X_INPUT
);
  input CLK;
  input CE;
  output Y;
  input RST_INPUT;
  input X_INPUT;
  wire CLK_IBUFG_0;
  wire CE_IBUF_1;
  wire D4in;
  wire XLXN_13;
  wire XLXN_14;
  wire XLXN_15;
  wire XLXN_16;
  wire XLXN_17;
  wire XLXN_50;
  wire XLXN_51;
  wire Q1;
  wire XLXN_52;
  wire Q2;
  wire XLXN_53;
  wire Q3;
  wire XLXN_54;
  wire Q4;
  wire XLXN_56;
  wire XLXN_57;
  wire XLXN_58;
  wire XLXN_59;
  wire XLXN_80;
  wire XLXN_76;
  wire XLXN_77;
  wire XLXN_91;
  wire X;
  wire XLXN_97;
  wire D1in;
  wire XLXN_2;
  wire XLXN_3;
  wire D2in;
  wire Yin;
  wire D3in;
  wire N4;
  wire N5;
  wire \XLXI_27/N1 ;
  wire \XLXI_27/N0 ;
  AND4B2 XLXI_1 (
    .I0(X),
    .I1(Q2),
    .I2(Q4),
    .I3(Q3),
    .O(XLXN_2)
  );
  AND4B1 XLXI_2 (
    .I0(Q4),
    .I1(Q3),
    .I2(Q2),
    .I3(X),
    .O(XLXN_3)
  );
  AND3B2 XLXI_3 (
    .I0(X),
    .I1(Q4),
    .I2(Q2),
    .O(XLXN_14)
  );
  AND2 XLXI_4 (
    .I0(Q1),
    .I1(X),
    .O(XLXN_13)
  );
  AND3B2 XLXI_5 (
    .I0(Q4),
    .I1(Q3),
    .I2(Q2),
    .O(XLXN_15)
  );
  AND3 XLXI_6 (
    .I0(Q4),
    .I1(Q2),
    .I2(X),
    .O(XLXN_16)
  );
  AND3B1 XLXI_7 (
    .I0(Q2),
    .I1(Q3),
    .I2(X),
    .O(XLXN_17)
  );
  AND3B2 XLXI_8 (
    .I0(X),
    .I1(Q4),
    .I2(Q3),
    .O(XLXN_54)
  );
  AND3B1 XLXI_9 (
    .I0(X),
    .I1(Q3),
    .I2(Q2),
    .O(XLXN_53)
  );
  AND2B1 XLXI_10 (
    .I0(X),
    .I1(Q1),
    .O(XLXN_52)
  );
  AND3B1 XLXI_11 (
    .I0(Q3),
    .I1(Q2),
    .I2(X),
    .O(XLXN_51)
  );
  AND4B3 XLXI_12 (
    .I0(Q4),
    .I1(Q3),
    .I2(Q1),
    .I3(X),
    .O(XLXN_50)
  );
  AND3B2 XLXI_14 (
    .I0(Q3),
    .I1(X),
    .I2(Q2),
    .O(XLXN_56)
  );
  AND2B2 XLXI_15 (
    .I0(Q4),
    .I1(X),
    .O(XLXN_57)
  );
  AND3B2 XLXI_16 (
    .I0(Q4),
    .I1(Q3),
    .I2(Q2),
    .O(XLXN_58)
  );
  AND4B2 XLXI_17 (
    .I0(Q3),
    .I1(Q2),
    .I2(Q4),
    .I3(X),
    .O(XLXN_59)
  );
  AND3B1 XLXI_18 (
    .I0(X),
    .I1(Q4),
    .I2(Q3),
    .O(XLXN_76)
  );
  AND2B1 XLXI_19 (
    .I0(Q3),
    .I1(X),
    .O(XLXN_77)
  );
  AND2B1 XLXI_20 (
    .I0(Q4),
    .I1(X),
    .O(XLXN_80)
  );
  OR2 XLXI_22 (
    .I0(XLXN_3),
    .I1(XLXN_2),
    .O(D1in)
  );
  OR5 XLXI_23 (
    .I0(XLXN_17),
    .I1(XLXN_16),
    .I2(XLXN_15),
    .I3(XLXN_13),
    .I4(XLXN_14),
    .O(D2in)
  );
  OR5 XLXI_24 (
    .I0(XLXN_50),
    .I1(XLXN_51),
    .I2(XLXN_52),
    .I3(XLXN_53),
    .I4(XLXN_54),
    .O(D3in)
  );
  OR4 XLXI_25 (
    .I0(XLXN_59),
    .I1(XLXN_58),
    .I2(XLXN_57),
    .I3(XLXN_56),
    .O(D4in)
  );
  OR3 XLXI_26 (
    .I0(XLXN_80),
    .I1(XLXN_77),
    .I2(XLXN_76),
    .O(Yin)
  );
  IBUF XLXI_28 (
    .I(RST_INPUT),
    .O(XLXN_97)
  );
  IBUF XLXI_29 (
    .I(X_INPUT),
    .O(X)
  );
  BUFG XLXI_30 (
    .I(CLK_IBUFG_0),
    .O(XLXN_91)
  );
  OBUF XLXI_31 (
    .I(Yin),
    .O(Y)
  );
  IBUFG CLK_IBUFG (
    .I(CLK),
    .O(CLK_IBUFG_0)
  );
  IBUF CE_IBUF (
    .I(CE),
    .O(CE_IBUF_1)
  );
  GND XST_GND (
    .G(N4)
  );
  VCC XST_VCC (
    .P(N5)
  );
  VCC \XLXI_27/XST_VCC  (
    .P(\XLXI_27/N1 )
  );
  GND \XLXI_27/XST_GND  (
    .G(\XLXI_27/N0 )
  );
  defparam \XLXI_27/I_Q3 .INIT = 1'b0;
  FDCE \XLXI_27/I_Q3  (
    .C(XLXN_91),
    .CE(CE_IBUF_1),
    .CLR(XLXN_97),
    .D(D4in),
    .Q(Q4)
  );
  defparam \XLXI_27/I_Q2 .INIT = 1'b0;
  FDCE \XLXI_27/I_Q2  (
    .C(XLXN_91),
    .CE(CE_IBUF_1),
    .CLR(XLXN_97),
    .D(D3in),
    .Q(Q3)
  );
  defparam \XLXI_27/I_Q1 .INIT = 1'b0;
  FDCE \XLXI_27/I_Q1  (
    .C(XLXN_91),
    .CE(CE_IBUF_1),
    .CLR(XLXN_97),
    .D(D2in),
    .Q(Q2)
  );
  defparam \XLXI_27/I_Q0 .INIT = 1'b0;
  FDCE \XLXI_27/I_Q0  (
    .C(XLXN_91),
    .CE(CE_IBUF_1),
    .CLR(XLXN_97),
    .D(D1in),
    .Q(Q1)
  );
endmodule


`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

    wire GSR;
    wire GTS;
    wire PRLD;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

