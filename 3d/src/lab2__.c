#define UNICODE
#define _USE_MATH_DEFINES
#define WIN32_LEAN_AND_MEAN // Exclude rarely-used stuff from Windows headers

#include <SDKDDKVer.h>
#include <windows.h>
#include <stdlib.h>
#include <math.h>

#define szTitle L"3d"
#define szWindowClass L"3d"

#define refreshTime 50 // 16 ms

#define bgImagePath L"bg.bmp"
#define bgImageWidth 200
#define bgImageHeight 158

#define angleRange 5
#define angleDelta 0.05

#define dim 3
#define point_count 22

double points[point_count][dim] = { // точки
		{30, 0, 1}, {10, 0, 1},

		{10, 0, 1}, {17.5, 7.5, 1},

		{17.5, 7.5, 1}, {10, 15, 1},

		{10, 15, 1}, {30, 15, 1},

		{30, 0, 1}, {30, 45, 1},

		{30, 45, 1}, {65, 7.5, 1},

		{65, 7.5, 1}, {97.5, 45, 1},

		{0, 45, 1}, {130, 45, 1},

		{130, 45, 1}, {105, 70, 1},

		{105, 70, 1}, {25, 70, 1},

		{25, 70, 1}, {0, 45, 1}};

double crds[point_count][dim] = {0};
double crds_calc[point_count][dim] = {0};

double angle = 0;

double dx = 100;
double dy = 50;

double scX = 5;
double scY = 5;

HINSTANCE hInst = NULL;
HBITMAP bmw = NULL;

int timerID = -1;
int angleK = 1;

int frame = 0;
int framek = 1;

BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

double radians(double deg) {
	return deg * M_PI / 180.0;
}

void mult_2d_matrix(double matrix1[point_count][dim], double matrix2[point_count][dim], double matrix_res[point_count][dim]) {
	for (unsigned int y1 = 0; y1 < point_count; y1++) {
		for (unsigned int x2 = 0; x2 < dim; x2++) {
			matrix_res[y1][x2] = 0;
			for (unsigned int y2 = 0; y2 < dim; y2++) {
				matrix_res[y1][x2] += matrix1[y1][y2] * matrix2[y2][x2];
			}
		}
	}
}

void calculate() {
	double reflect_matrix[3][3] = {
	/*  -X	0	0 */{1, 0, 0},
	/*  0	-Y	0 */{0, 1, 0},
	/*  0	0	1 */{0, 0, 1}};

	double scale_matrix[3][3] = {
	/*  scX	0	0 */{scX, 0, 0},
	/*  0	scY	0 */{0, scY, 0},
	/*  0	0	1 */{0, 0, 1}};

	double translate_matrix[3][3] = {
	/*  1	0	0 */{1, 0, 0},
	/*  0	1	0 */{0, 1, 0},
	/*  dx	dy	1 */{dx, dy, 1}};

	mult_2d_matrix(points, reflect_matrix, crds_calc);
	mult_2d_matrix(crds_calc, scale_matrix, crds);
	mult_2d_matrix(crds, translate_matrix, crds_calc);

	double minX = 0, minY = 0, maxX = 1, maxY = 1;
	for (int i = 0; i < point_count; i++) {
		if (crds[i][0] < minX)
			minX = crds[i][0];
		if (crds[i][0] > maxX)
			maxX = crds[i][0];

		if (crds[i][1] < minY)
			minY = crds[i][1];
		if (crds[i][1] > maxY)
			maxY = crds[i][1];
	}
	double cX = minX + (maxX - minX) / 2 + dx;
	double cY = minY + (maxY - minY) / 2 + dy;

	double angleR = radians(angle);

	double rotate_matrix[3][3] = {
	/*  cos	sin	0 */{cos(angleR), sin(angleR), 0},
	/* -sin	cos	0 */{-sin(angleR), cos(angleR), 0},
	/*  0	0	1 */{-cX * (cos(angleR) - 1) + cY * sin(angleR), -cY * (cos(angleR) - 1) - cX * sin(angleR), 1}};

	mult_2d_matrix(crds_calc, rotate_matrix, crds);
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	if (!InitInstance(hInstance, nCmdShow))
		return 1;
	bmw = (HBITMAP) LoadImage(NULL, bgImagePath, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	DeleteObject(bmw);
	return (int) msg.wParam;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
	WNDCLASSEXW wcex = {0};

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, IDI_WINLOGO);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	wcex.lpszMenuName = L"Menu";
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(hInstance, IDI_WINLOGO);

	RegisterClassEx(&wcex);
	hInst = hInstance;

	HWND hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
		return FALSE;

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		case WM_CREATE: {
			SetTimer(hWnd, timerID = 1, refreshTime, NULL);
			break;
		}

		case WM_COMMAND: {
			int wmId = LOWORD(wParam);
			switch (wmId) {
				default: {
					return DefWindowProc(hWnd, message, wParam, lParam);
				}
			}
			break;
		}

		case WM_PAINT: {
			RECT rect;
			GetWindowRect(hWnd, &rect);
			int windowWidth = rect.right - rect.left;
			int windowHeight = rect.bottom - rect.top;

			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);

			HDC buffHDC = CreateCompatibleDC(hdc);
			HBITMAP bufferBmp = CreateCompatibleBitmap(hdc, windowWidth, windowHeight);
			int savedDC = SaveDC(buffHDC);
			SelectObject(buffHDC, bufferBmp);

			HDC bgImageDC = CreateCompatibleDC(hdc);
			SelectObject(bgImageDC, bmw);

			BitBlt(hdc, 0, 0, bgImageWidth, bgImageHeight, bgImageDC, 0, 0, SRCCOPY);

			for (int x0 = 0; x0 < windowWidth + bgImageWidth; x0 += bgImageWidth)
				for (int y0 = 0; y0 < windowHeight + bgImageHeight; y0 += bgImageHeight)
					BitBlt(buffHDC, x0, y0, bgImageWidth, bgImageHeight, bgImageDC, 0, 0, SRCCOPY);
			DeleteDC(bgImageDC);

			calculate();
			for (int i = 0; i < point_count; i += 2) {
				MoveToEx(buffHDC, crds[i][0], crds[i][1], NULL);
				LineTo(buffHDC, crds[i + 1][0], crds[i + 1][1]);
			}

			BitBlt(hdc, 0, 0, windowWidth, windowHeight, buffHDC, 0, 0, SRCCOPY);
			RestoreDC(buffHDC, savedDC);
			DeleteObject(bufferBmp);
			DeleteDC(buffHDC);

			EndPaint(hWnd, &ps);
			break;
		}

		case WM_SIZE:
			break;

		case WM_TIMER:
			frame += framek;
			if (frame <= 0 || frame >= 4)
				framek *= -1;
			if (angle > angleRange || angle < -angleRange)
				angleK *= -1;
			angle += angleK * angleDelta;

			RedrawWindow(hWnd, NULL, NULL, RDW_ERASE | RDW_INVALIDATE);
			break;

		case WM_ERASEBKGND:
			return (LRESULT) 1;

		case WM_KEYDOWN: {
			int delta = 5;
			if (wParam == VK_ADD)
				angle += delta;
			else if (wParam == VK_SUBTRACT)
				angle -= delta;

			else if (wParam == VK_PRIOR) {
				scX += 1;
				scY += 1;
			} else if (wParam == VK_NEXT) {
				scX -= 1;
				scY -= 1;
			}

			else if (wParam == VK_LEFT)
				dx -= delta;
			else if (wParam == VK_RIGHT)
				dx += delta;
			else if (wParam == VK_UP)
				dy -= delta;
			else if (wParam == VK_DOWN)
				dy += delta;

			RedrawWindow(hWnd, NULL, NULL, RDW_ERASE | RDW_INVALIDATE);
			break;
		}

		case WM_DESTROY: {
			PostQuitMessage(0);
			KillTimer(hWnd, 1);
			timerID = -1;
			break;
		}

		default: {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	return 0;
}
