VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL XLXN_2
        SIGNAL XLXN_3
        SIGNAL D1in
        SIGNAL Q3
        SIGNAL Q4
        SIGNAL Q2
        SIGNAL X
        SIGNAL XLXN_13
        SIGNAL XLXN_14
        SIGNAL XLXN_15
        SIGNAL XLXN_16
        SIGNAL XLXN_17
        SIGNAL D2in
        SIGNAL Q1
        SIGNAL XLXN_50
        SIGNAL XLXN_51
        SIGNAL XLXN_52
        SIGNAL XLXN_53
        SIGNAL XLXN_54
        SIGNAL D3in
        SIGNAL XLXN_56
        SIGNAL XLXN_57
        SIGNAL XLXN_58
        SIGNAL XLXN_59
        SIGNAL D4in
        SIGNAL XLXN_76
        SIGNAL XLXN_77
        SIGNAL XLXN_80
        SIGNAL Yin
        SIGNAL XLXN_86
        SIGNAL XLXN_87
        SIGNAL XLXN_88
        SIGNAL X_INPUT
        SIGNAL Y
        SIGNAL XLXN_91
        SIGNAL CLK
        SIGNAL RST_INPUT
        SIGNAL XLXN_97
        SIGNAL CE
        SIGNAL XLXN_111
        PORT Input X_INPUT
        PORT Output Y
        PORT Input CLK
        PORT Input RST_INPUT
        PORT Input CE
        BEGIN BLOCKDEF and4b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -208 144 -208 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF and3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -176 144 -176 
            LINE N 144 -80 64 -80 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 64 -64 64 -192 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 40 -192 
            CIRCLE N 40 -204 64 -180 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 64 -48 64 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCKDEF or5
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 48 -128 
            LINE N 0 -192 72 -192 
            LINE N 0 -256 48 -256 
            LINE N 0 -320 48 -320 
            LINE N 256 -192 192 -192 
            ARC N 28 -320 204 -144 112 -144 192 -192 
            LINE N 112 -240 48 -240 
            LINE N 112 -144 48 -144 
            LINE N 48 -64 48 -144 
            LINE N 48 -320 48 -240 
            ARC N 28 -240 204 -64 192 -192 112 -240 
            ARC N -40 -248 72 -136 48 -144 48 -240 
        END BLOCKDEF
        BEGIN BLOCKDEF or4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 48 -256 
            LINE N 256 -160 192 -160 
            ARC N 28 -208 204 -32 192 -160 112 -208 
            LINE N 112 -208 48 -208 
            LINE N 112 -112 48 -112 
            LINE N 48 -256 48 -208 
            LINE N 48 -64 48 -112 
            ARC N -40 -216 72 -104 48 -112 48 -208 
            ARC N 28 -288 204 -112 112 -112 192 -160 
        END BLOCKDEF
        BEGIN BLOCKDEF or3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 72 -128 
            LINE N 0 -192 48 -192 
            LINE N 256 -128 192 -128 
            ARC N 28 -256 204 -80 112 -80 192 -128 
            ARC N -40 -184 72 -72 48 -80 48 -176 
            LINE N 48 -64 48 -80 
            LINE N 48 -192 48 -176 
            LINE N 112 -80 48 -80 
            ARC N 28 -176 204 0 192 -128 112 -176 
            LINE N 112 -176 48 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF fd4ce
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 384 -448 320 -448 
            LINE N 384 -384 320 -384 
            LINE N 0 -384 64 -384 
            LINE N 0 -448 64 -448 
            LINE N 0 -320 64 -320 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            LINE N 384 -320 320 -320 
            RECTANGLE N 64 -512 320 -64 
            LINE N 0 -192 64 -192 
            LINE N 0 -32 64 -32 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 0 -128 64 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF ibuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF bufg
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 0 
            LINE N 128 -32 64 -64 
            LINE N 64 0 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 and4b2
            PIN I0 X
            PIN I1 Q2
            PIN I2 Q4
            PIN I3 Q3
            PIN O XLXN_2
        END BLOCK
        BEGIN BLOCK XLXI_2 and4b1
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q2
            PIN I3 X
            PIN O XLXN_3
        END BLOCK
        BEGIN BLOCK XLXI_3 and3b2
            PIN I0 X
            PIN I1 Q4
            PIN I2 Q2
            PIN O XLXN_14
        END BLOCK
        BEGIN BLOCK XLXI_4 and2
            PIN I0 Q1
            PIN I1 X
            PIN O XLXN_13
        END BLOCK
        BEGIN BLOCK XLXI_5 and3b2
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q2
            PIN O XLXN_15
        END BLOCK
        BEGIN BLOCK XLXI_6 and3
            PIN I0 Q4
            PIN I1 Q2
            PIN I2 X
            PIN O XLXN_16
        END BLOCK
        BEGIN BLOCK XLXI_7 and3b1
            PIN I0 Q2
            PIN I1 Q3
            PIN I2 X
            PIN O XLXN_17
        END BLOCK
        BEGIN BLOCK XLXI_8 and3b2
            PIN I0 X
            PIN I1 Q4
            PIN I2 Q3
            PIN O XLXN_54
        END BLOCK
        BEGIN BLOCK XLXI_9 and3b1
            PIN I0 X
            PIN I1 Q3
            PIN I2 Q2
            PIN O XLXN_53
        END BLOCK
        BEGIN BLOCK XLXI_10 and2b1
            PIN I0 X
            PIN I1 Q1
            PIN O XLXN_52
        END BLOCK
        BEGIN BLOCK XLXI_11 and3b1
            PIN I0 Q3
            PIN I1 Q2
            PIN I2 X
            PIN O XLXN_51
        END BLOCK
        BEGIN BLOCK XLXI_12 and4b3
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q1
            PIN I3 X
            PIN O XLXN_50
        END BLOCK
        BEGIN BLOCK XLXI_14 and3b2
            PIN I0 Q3
            PIN I1 X
            PIN I2 Q2
            PIN O XLXN_56
        END BLOCK
        BEGIN BLOCK XLXI_15 and2b2
            PIN I0 Q4
            PIN I1 X
            PIN O XLXN_57
        END BLOCK
        BEGIN BLOCK XLXI_16 and3b2
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q2
            PIN O XLXN_58
        END BLOCK
        BEGIN BLOCK XLXI_17 and4b2
            PIN I0 Q3
            PIN I1 Q2
            PIN I2 Q4
            PIN I3 X
            PIN O XLXN_59
        END BLOCK
        BEGIN BLOCK XLXI_18 and3b1
            PIN I0 X
            PIN I1 Q4
            PIN I2 Q3
            PIN O XLXN_76
        END BLOCK
        BEGIN BLOCK XLXI_19 and2b1
            PIN I0 Q3
            PIN I1 X
            PIN O XLXN_77
        END BLOCK
        BEGIN BLOCK XLXI_20 and2b1
            PIN I0 Q4
            PIN I1 X
            PIN O XLXN_80
        END BLOCK
        BEGIN BLOCK XLXI_22 or2
            PIN I0 XLXN_3
            PIN I1 XLXN_2
            PIN O D1in
        END BLOCK
        BEGIN BLOCK XLXI_23 or5
            PIN I0 XLXN_17
            PIN I1 XLXN_16
            PIN I2 XLXN_15
            PIN I3 XLXN_13
            PIN I4 XLXN_14
            PIN O D2in
        END BLOCK
        BEGIN BLOCK XLXI_24 or5
            PIN I0 XLXN_50
            PIN I1 XLXN_51
            PIN I2 XLXN_52
            PIN I3 XLXN_53
            PIN I4 XLXN_54
            PIN O D3in
        END BLOCK
        BEGIN BLOCK XLXI_25 or4
            PIN I0 XLXN_59
            PIN I1 XLXN_58
            PIN I2 XLXN_57
            PIN I3 XLXN_56
            PIN O D4in
        END BLOCK
        BEGIN BLOCK XLXI_26 or3
            PIN I0 XLXN_80
            PIN I1 XLXN_77
            PIN I2 XLXN_76
            PIN O Yin
        END BLOCK
        BEGIN BLOCK XLXI_27 fd4ce
            PIN C XLXN_91
            PIN CE CE
            PIN CLR XLXN_97
            PIN D0 D1in
            PIN D1 D2in
            PIN D2 D3in
            PIN D3 D4in
            PIN Q0 Q1
            PIN Q1 Q2
            PIN Q2 Q3
            PIN Q3 Q4
        END BLOCK
        BEGIN BLOCK XLXI_29 ibuf
            PIN I X_INPUT
            PIN O X
        END BLOCK
        BEGIN BLOCK XLXI_30 bufg
            PIN I CLK
            PIN O XLXN_91
        END BLOCK
        BEGIN BLOCK XLXI_31 obuf
            PIN I Yin
            PIN O Y
        END BLOCK
        BEGIN BLOCK XLXI_28 ibuf
            PIN I RST_INPUT
            PIN O XLXN_97
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_1 224 384 R0
        INSTANCE XLXI_2 224 656 R0
        INSTANCE XLXI_3 176 944 R0
        INSTANCE XLXI_4 176 1088 R0
        INSTANCE XLXI_5 176 1280 R0
        INSTANCE XLXI_6 176 1488 R0
        INSTANCE XLXI_7 176 1664 R0
        INSTANCE XLXI_8 160 1920 R0
        INSTANCE XLXI_10 144 2224 R0
        INSTANCE XLXI_11 160 2416 R0
        INSTANCE XLXI_12 160 2688 R0
        INSTANCE XLXI_14 1072 384 R0
        INSTANCE XLXI_15 1056 576 R0
        INSTANCE XLXI_16 1072 800 R0
        INSTANCE XLXI_17 1072 1072 R0
        INSTANCE XLXI_18 1040 1376 R0
        INSTANCE XLXI_19 1040 1536 R0
        INSTANCE XLXI_20 1040 1712 R0
        INSTANCE XLXI_23 496 1344 R0
        INSTANCE XLXI_24 480 2320 R0
        INSTANCE XLXI_25 1376 768 R0
        INSTANCE XLXI_26 1360 1568 R0
        INSTANCE XLXI_22 624 464 R0
        BEGIN BRANCH XLXN_2
            WIRE 480 224 544 224
            WIRE 544 224 544 336
            WIRE 544 336 624 336
        END BRANCH
        BEGIN BRANCH XLXN_3
            WIRE 480 496 544 496
            WIRE 544 400 544 496
            WIRE 544 400 624 400
        END BRANCH
        BEGIN BRANCH D1in
            WIRE 880 368 960 368
            WIRE 960 368 992 368
            BEGIN DISPLAY 959 368 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 128 128 224 128
            BEGIN DISPLAY 128 128 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 128 192 224 192
            BEGIN DISPLAY 128 192 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 128 256 224 256
            BEGIN DISPLAY 128 256 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 128 320 224 320
            BEGIN DISPLAY 128 320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 128 400 224 400
            BEGIN DISPLAY 128 400 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 128 464 224 464
            BEGIN DISPLAY 128 464 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 128 528 224 528
            BEGIN DISPLAY 128 528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 128 592 224 592
            BEGIN DISPLAY 128 592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_13
            WIRE 432 992 464 992
            WIRE 464 992 464 1088
            WIRE 464 1088 496 1088
        END BRANCH
        BEGIN BRANCH XLXN_14
            WIRE 432 816 496 816
            WIRE 496 816 496 1024
        END BRANCH
        BEGIN BRANCH XLXN_15
            WIRE 432 1152 496 1152
        END BRANCH
        BEGIN BRANCH XLXN_16
            WIRE 432 1360 464 1360
            WIRE 464 1216 464 1360
            WIRE 464 1216 496 1216
        END BRANCH
        BEGIN BRANCH XLXN_17
            WIRE 432 1536 496 1536
            WIRE 496 1280 496 1536
        END BRANCH
        BEGIN BRANCH D2in
            WIRE 752 1152 800 1152
            WIRE 800 1152 832 1152
            BEGIN DISPLAY 797 1152 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 752 176 752
            BEGIN DISPLAY 96 752 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 816 176 816
            BEGIN DISPLAY 96 816 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 880 176 880
            BEGIN DISPLAY 96 880 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 960 176 960
            BEGIN DISPLAY 96 960 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 96 1024 176 1024
            BEGIN DISPLAY 96 1024 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 1088 176 1088
            BEGIN DISPLAY 96 1088 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 96 1152 176 1152
            BEGIN DISPLAY 96 1152 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 1216 176 1216
            BEGIN DISPLAY 96 1216 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 1296 176 1296
            BEGIN DISPLAY 80 1296 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 1360 176 1360
            BEGIN DISPLAY 96 1360 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 1424 176 1424
            BEGIN DISPLAY 96 1424 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 1472 176 1472
            BEGIN DISPLAY 96 1472 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 96 1536 176 1536
            BEGIN DISPLAY 96 1536 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 1600 176 1600
            BEGIN DISPLAY 96 1600 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 96 1728 160 1728
            BEGIN DISPLAY 96 1728 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 1792 160 1792
            BEGIN DISPLAY 96 1792 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 1856 160 1856
            BEGIN DISPLAY 96 1856 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 80 1904 96 1904
            WIRE 96 1888 128 1888
            WIRE 96 1888 96 1904
            BEGIN DISPLAY 80 1904 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 80 1968 96 1968
            WIRE 96 1952 128 1952
            WIRE 96 1952 96 1968
            BEGIN DISPLAY 80 1968 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 2032 96 2032
            WIRE 96 2016 128 2016
            WIRE 96 2016 96 2032
            BEGIN DISPLAY 80 2032 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 80 2096 144 2096
            BEGIN DISPLAY 80 2096 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 2160 144 2160
            BEGIN DISPLAY 80 2160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 2224 160 2224
            BEGIN DISPLAY 80 2224 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 80 2288 160 2288
            BEGIN DISPLAY 80 2288 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 80 2352 160 2352
            BEGIN DISPLAY 80 2352 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 2432 160 2432
            BEGIN DISPLAY 96 2432 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 96 2496 160 2496
            BEGIN DISPLAY 96 2496 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 96 2560 160 2560
            BEGIN DISPLAY 96 2560 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 2624 160 2624
            BEGIN DISPLAY 96 2624 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_50
            WIRE 416 2528 480 2528
            WIRE 480 2256 480 2528
        END BRANCH
        BEGIN BRANCH XLXN_51
            WIRE 416 2288 448 2288
            WIRE 448 2192 448 2288
            WIRE 448 2192 480 2192
        END BRANCH
        BEGIN BRANCH XLXN_52
            WIRE 400 2128 480 2128
        END BRANCH
        BEGIN BRANCH XLXN_53
            WIRE 384 1952 464 1952
            WIRE 464 1952 464 2064
            WIRE 464 2064 480 2064
        END BRANCH
        BEGIN BRANCH XLXN_54
            WIRE 416 1792 480 1792
            WIRE 480 1792 480 2000
        END BRANCH
        BEGIN BRANCH D3in
            WIRE 736 2128 784 2128
            WIRE 784 2128 848 2128
            BEGIN DISPLAY 784 2128 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_56
            WIRE 1328 256 1376 256
            WIRE 1376 256 1376 512
        END BRANCH
        BEGIN BRANCH XLXN_57
            WIRE 1312 480 1344 480
            WIRE 1344 480 1344 576
            WIRE 1344 576 1376 576
        END BRANCH
        BEGIN BRANCH XLXN_58
            WIRE 1328 672 1344 672
            WIRE 1344 640 1344 672
            WIRE 1344 640 1376 640
        END BRANCH
        BEGIN BRANCH XLXN_59
            WIRE 1328 912 1376 912
            WIRE 1376 704 1376 912
        END BRANCH
        BEGIN BRANCH D4in
            WIRE 1632 608 1696 608
            WIRE 1696 608 1744 608
            BEGIN DISPLAY 1692 608 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 960 192 1072 192
            BEGIN DISPLAY 960 192 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 960 256 1072 256
            BEGIN DISPLAY 960 256 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 960 320 1072 320
            BEGIN DISPLAY 960 320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 960 448 1056 448
            BEGIN DISPLAY 960 448 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 960 512 1056 512
            BEGIN DISPLAY 960 512 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 960 608 1072 608
            BEGIN DISPLAY 960 608 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 960 672 1072 672
            BEGIN DISPLAY 960 672 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 960 736 1072 736
            BEGIN DISPLAY 960 736 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 960 816 1072 816
            BEGIN DISPLAY 960 816 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 960 880 1072 880
            BEGIN DISPLAY 960 880 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 960 944 1072 944
            BEGIN DISPLAY 960 944 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 960 1008 1072 1008
            BEGIN DISPLAY 960 1008 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 944 1184 1040 1184
            BEGIN DISPLAY 944 1184 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 944 1248 1040 1248
            BEGIN DISPLAY 944 1248 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 944 1312 1040 1312
            BEGIN DISPLAY 944 1312 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_76
            WIRE 1296 1248 1360 1248
            WIRE 1360 1248 1360 1376
        END BRANCH
        BEGIN BRANCH XLXN_77
            WIRE 1296 1440 1360 1440
        END BRANCH
        BEGIN BRANCH XLXN_80
            WIRE 1296 1616 1360 1616
            WIRE 1360 1504 1360 1616
        END BRANCH
        BEGIN BRANCH X
            WIRE 944 1408 1040 1408
            BEGIN DISPLAY 944 1408 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 944 1472 1040 1472
            BEGIN DISPLAY 944 1472 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 944 1584 1040 1584
            BEGIN DISPLAY 944 1584 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 944 1648 1040 1648
            BEGIN DISPLAY 944 1648 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Yin
            WIRE 1616 1440 1696 1440
            WIRE 1696 1440 1744 1440
            BEGIN DISPLAY 1697 1440 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_9 128 2080 R0
        BEGIN BRANCH Y
            WIRE 2080 1072 2096 1072
            WIRE 2096 1072 2208 1072
        END BRANCH
        INSTANCE XLXI_27 2096 864 R0
        BEGIN BRANCH XLXN_91
            WIRE 2064 736 2080 736
            WIRE 2080 736 2096 736
        END BRANCH
        IOMARKER 1808 736 CLK R180 28
        IOMARKER 2080 144 X_INPUT R180 28
        INSTANCE XLXI_31 1856 1104 R0
        INSTANCE XLXI_28 1776 880 R0
        BEGIN BRANCH RST_INPUT
            WIRE 1744 848 1776 848
        END BRANCH
        IOMARKER 1744 848 RST_INPUT R180 28
        BEGIN BRANCH XLXN_97
            WIRE 2000 848 2096 848
            WIRE 2096 832 2096 848
        END BRANCH
        BEGIN BRANCH CE
            WIRE 2064 672 2096 672
        END BRANCH
        IOMARKER 2064 672 CE R180 28
        BEGIN BRANCH D1in
            WIRE 2016 416 2096 416
            BEGIN DISPLAY 2016 416 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 2480 416 2528 416
            WIRE 2528 416 2560 416
            BEGIN DISPLAY 2522 416 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 2480 480 2528 480
            WIRE 2528 480 2560 480
            BEGIN DISPLAY 2536 480 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 2480 544 2512 544
            WIRE 2512 544 2560 544
            BEGIN DISPLAY 2509 544 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 2480 608 2544 608
            WIRE 2544 608 2560 608
            BEGIN DISPLAY 2539 608 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D2in
            WIRE 2016 480 2096 480
            BEGIN DISPLAY 2016 480 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D3in
            WIRE 2032 544 2096 544
            BEGIN DISPLAY 2032 544 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D4in
            WIRE 2032 608 2096 608
            BEGIN DISPLAY 2032 608 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Yin
            WIRE 1728 1072 1744 1072
            WIRE 1744 1072 1856 1072
            BEGIN DISPLAY 1728 1072 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 1748 1072 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X_INPUT
            WIRE 2080 144 2432 144
        END BRANCH
        INSTANCE XLXI_29 2432 176 R0
        BEGIN BRANCH X
            WIRE 2656 144 2704 144
            WIRE 2704 144 2752 144
            BEGIN DISPLAY 2707 144 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1808 736 1824 736
            WIRE 1824 736 1840 736
        END BRANCH
        INSTANCE XLXI_30 1840 768 R0
        IOMARKER 2208 1072 Y R0 28
    END SHEET
END SCHEMATIC
