#include <iostream>
#include <string>

using namespace std;

class PrintedEdition {

private:
	string name;
	string author;
	double price;

public:
	PrintedEdition() :
			PrintedEdition("", "", 0) {

	}

	PrintedEdition(string n, string a, double p) :
			name(n), author(a), price(p) {
	}

	const string& getAuthor() const {
		return author;
	}

	void setAuthor(const string &a) {
		this->author = a;
	}

	const string& getName() const {
		return name;
	}

	void setName(const string &n) {
		this->name = n;
	}

	double getPrice() const {
		return price;
	}

	void setPrice(double p) {
		this->price = p;
	}
};

class Book: public PrintedEdition {
public:
	Book(string n, string a, double p) :
			PrintedEdition(n, a, p) {
	}
};

class TextBook: public PrintedEdition {
public:
	TextBook(string n, string a, double p) :
			PrintedEdition(n, a, p) {
	}

};

class Magazine: public PrintedEdition {
public:
	Magazine(string n, string a, double p) :
			PrintedEdition(n, a, p) {
	}

};

void printp(PrintedEdition p) {
	cout << "Product: " << p.getName();
	cout << ", Manufacturer: " << p.getAuthor();
	cout << ", Price: " << p.getPrice();
	cout << endl;
}

int main() {
	unsigned int size = 4;
	PrintedEdition shelf[size];

	shelf[0] = PrintedEdition("Example", "Unknown", 0);
	shelf[1] = Book("1984", "Orwell", 1000);
	shelf[2] = TextBook("ABC", "Teachers", 1000);
	shelf[3] = Magazine("PC Gamer", "Future plc.", 300);

	// а) список товаров для заданного наименования
	for (unsigned int i = 0; i < size; i++) {
		printp(shelf[i]);
	}

	return 0;
}
