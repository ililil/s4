#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <math.h>
#include "3d.h"

#define dim 4

double radians(double deg) {
	return deg * M_PI / 180.0;
}

DoubleMatrix gen_scale_matrix(double scaleX, double scaleY, double scaleZ) {
	DoubleMatrix matrix = double_matrix_new(dim, dim);

	matrix.arr[0][0] = -scaleX;
	matrix.arr[1][1] = scaleY;
	matrix.arr[2][2] = scaleZ;
	matrix.arr[3][3] = 1;

	/*  scaleX	0		0		0 */
	/*  0		scaleY	0		0 */
	/*  0		0		scaleZ	0 */
	/*  0		0		0		1 */
	return matrix;
}

DoubleMatrix gen_view_matrix(double angleX, double angleY, double R) {
	DoubleMatrix matrix = double_matrix_new(dim, dim);
	double angleXR = radians(angleX);
	double angleYR = radians(angleY);

	matrix.arr[0][0] = -sin(angleXR);
	matrix.arr[0][1] = -cos(angleYR) * cos(angleXR);
	matrix.arr[0][2] = -sin(angleYR) * cos(angleXR);

	matrix.arr[1][0] = cos(angleXR);
	matrix.arr[1][1] = -cos(angleYR) * sin(angleXR);
	matrix.arr[1][2] = -sin(angleYR) * sin(angleXR);

	matrix.arr[2][1] = sin(angleYR);
	matrix.arr[2][2] = -cos(angleYR);

	matrix.arr[3][2] = R;
	matrix.arr[3][3] = 1;

	/*  -sin(X)		-cos(y) * cos(x)	-sin(y) * cos(x)	0 */
	/*  cos(X)		-cos(y) * sin(x)	-sin(y) * sin(x)	0 */
	/*  0			sin(y)				-cos(y)				0 */
	/*  0			0					R					1 */
	return matrix;
}

DoubleMatrix gen_perspective_matrix(double deltaX, double deltaY, double deltaZ) {
	DoubleMatrix matrix = double_matrix_new(dim, dim);

	matrix.arr[0][0] = 1;
	matrix.arr[1][1] = 1;
	matrix.arr[2][2] = 1;

	matrix.arr[3][0] = deltaX;
	matrix.arr[3][1] = deltaY;
	matrix.arr[3][2] = deltaZ;
	matrix.arr[3][3] = 1;

	/*  1		0		0		0	*/
	/*  0		1		0		0	*/
	/*  0		0		1		0	*/
	/*	deltaX	deltaY	deltaZ	1	*/
	return matrix;
}
