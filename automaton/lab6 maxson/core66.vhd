LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY core66 IS
END core66 ;
ARCHITECTURE behavioral OF core66  IS 

   COMPONENT core66
   PORT( x0_in	:	IN	STD_LOGIC; 
          x1_in	:	IN	STD_LOGIC; 
          x2_in	:	IN	STD_LOGIC; 
          x3_in	:	IN	STD_LOGIC; 
          CE	:	IN	STD_LOGIC; 
          CLK	:	IN	STD_LOGIC; 
          RST_INPUT	:	IN	STD_LOGIC; 
	  y0	:	OUT	STD_LOGIC; 
          y1	:	OUT	STD_LOGIC; 
          y2	:	OUT	STD_LOGIC; 
          y3	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL x0_in	:	STD_LOGIC;
   SIGNAL x1_in	:	STD_LOGIC;
   SIGNAL x2_in	:	STD_LOGIC;
   SIGNAL x3_in	:	STD_LOGIC;
   SIGNAL CE	:	STD_LOGIC;
   SIGNAL CLK	:	STD_LOGIC;
   SIGNAL RST_INPUT	:	STD_LOGIC;
   SIGNAL y0	:	STD_LOGIC;
   SIGNAL y1	:	STD_LOGIC;
   SIGNAL y2	:	STD_LOGIC;
   SIGNAL y3	:	STD_LOGIC;
	
	constant clk_period : time := 10 ns;
   constant CE_period : time := 10 ns;
BEGIN

   UUT: core66  PORT MAP(
		x0_in => x0_in, 
		x1_in => x1_in, 
		x2_in => x2_in, 
		x3_in => x3_in, 
		CE => CE, 
		CLK => CLK, 
		RST_INPUT => RST_INPUT,
		y0 => y0, 
		y1 => y1, 
		y2 => y2, 
		y3 => y3
   );

-- *** Test Bench - User Defined Section ***
clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
	 CE_process :process
   begin
		CE <= '1';
		wait for CE_period/1;
   end process;
	 -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		RST_INPUT <= '1';
		wait for clk_period;
		RST_INPUT <= '0';
		x0_in <= '0'; 
		x1_in <= '0';
		x2_in <= '0';
		x3_in <= '1';
		wait for clk_period*1;
		x0_in <= '0';
		x1_in <= '0';
		x2_in <= '1';
		x3_in <= '0';
      wait for clk_period*5;
  		x0_in <= '0'; 
		x1_in <= '0';
		x2_in <= '1';
		x3_in <= '1';
		wait for clk_period*5;
		x0_in <= '0'; 
		x1_in <= '1';
		x2_in <= '0';
		x3_in <= '0';
      wait for clk_period*5;
		x0_in <= '0'; 
		x1_in <= '1';
		x2_in <= '0';
		x3_in <= '1';
		wait for clk_period*5;
		x0_in <= '0'; 
		x1_in <= '1';
		x2_in <= '1';
		x3_in <= '0';
      wait for clk_period*5;
		x0_in <= '0'; 
		x1_in <= '1';
		x2_in <= '1';
		x3_in <= '1';
      wait for clk_period*5;
		x0_in <= '1'; 
		x1_in <= '0';
		x2_in <= '0';
		x3_in <= '0';
      wait for clk_period*5;
		x0_in <= '1'; 
		x1_in <= '0';
		x2_in <= '0';
		x3_in <= '1';
      wait for clk_period*5;
		x0_in <= '1'; 
		x1_in <= '0';
		x2_in <= '1';
		x3_in <= '0';
      wait for clk_period*5;
		x0_in <= '1'; 
		x1_in <= '0';
		x2_in <= '1';
		x3_in <= '1';
      wait for clk_period*5;
		x0_in <= '1'; 
		x1_in <= '1';
		x2_in <= '0';
		x3_in <= '0';
      wait for clk_period*5;
		x0_in <= '1'; 
		x1_in <= '1';
		x2_in <= '0';
		x3_in <= '1';
      wait for clk_period*5;
		x0_in <= '1'; 
		x1_in <= '1';
		x2_in <= '1';
		x3_in <= '0';
		wait for clk_period*5;
		x0_in <= '1'; 
		x1_in <= '1';
		x2_in <= '1';
		x3_in <= '1';
      wait for clk_period*5;
	   wait;
   end process;

   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***
END;
