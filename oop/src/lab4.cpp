#include <iostream>
#include <string>

using namespace std;

template<class T> class MySet {

public:

    T *arr;
    int ind;

    MySet<T>() {
        arr = new T[50] { (T) NULL};
        ind = 0;
    }

    MySet<T> operator-(const T& e) {
        for (int i = 0; i < 50; i++)
            if (this->arr[i] == e)
                arr[i] = (T) NULL;

        return *this;
    }

    bool operator>(const MySet& e) {
        T* same = new T[50];
        int is = 0;

        for (int i2 = 0; i2 < 50; i2++) {
            for (int i = 0; i < 50; i++) {
                if (arr[i2] == e.arr[i] && arr[i2] != (T) NULL) {
                    same[is] = arr[i2];
                    is++;
                }
            }
        }
        return is >= e.ind;
    }

    bool operator!=(const MySet& ms)  {
        T* same = new T[50];
        int is = 0;

        for (int i2 = 0; i2 < 50; i2++) {
            for (int i = 0; i < 50; i++) {
                if (arr[i2] == ms.arr[i] && arr[i2] != (T) NULL) {
                    same[is] = arr[i2];
                    is++;
                }
            }
        }
        return is != ind;
    }

    void print() {
        for (int i = 0; i < 50; i++)
            if (arr[i] != (T) NULL)
                cout << arr[i] << " ";
    }

    void add(T t) {
        for (int i = 0; i < 50; i++)
            if (arr[i] == t)
                return;

        arr[ind] = t;
        ind++;
    }
};

int main() {

    MySet<int> set12;
    set12.add(1);
    set12.add(2);

    cout << "set12 = ";
    set12.print();
    cout << endl;

    MySet<int> set123;
    set123.add(1);
    set123.add(2);
    set123.add(3);

    cout << "set123 = ";
    set123.print();
    cout << endl;

    cout << endl << "set123 > set12 = " << (set123 > set12) << endl;

    MySet<int> set123_2;
    set123_2.add(1);
    set123_2.add(2);
    set123_2.add(3);

    cout << "set123 != set123_2 = " << (set123 != set123_2) << endl;
    cout << "set123 != set12 = " << (set123 != set12) << endl;

    set123 - 1;
    cout << endl << "set123 = ";
    set123.print();
    cout << endl;
    return 0;
}
