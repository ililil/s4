#ifndef THREE_D
#define THREE_D

#include "matrix.h"

double radians(double deg);

DoubleMatrix gen_scale_matrix(double scaleX, double scaleY, double scaleZ);

DoubleMatrix gen_view_matrix(double angleX, double angleY, double R);

DoubleMatrix gen_perspective_matrix(double deltaX, double deltaY, double deltaZ);

#endif
