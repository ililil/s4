VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL XLXN_2
        SIGNAL XLXN_3
        SIGNAL D1in
        SIGNAL Q3
        SIGNAL Q4
        SIGNAL Q2
        SIGNAL X
        SIGNAL XLXN_13
        SIGNAL XLXN_14
        SIGNAL XLXN_15
        SIGNAL XLXN_16
        SIGNAL XLXN_17
        SIGNAL D2in
        SIGNAL Q1
        SIGNAL D3in
        SIGNAL D4in
        SIGNAL CLK
        SIGNAL RST
        SIGNAL XLXN_151
        SIGNAL XLXN_152
        SIGNAL XLXN_153
        SIGNAL XLXN_154
        SIGNAL XLXN_168
        SIGNAL XLXN_169
        SIGNAL XLXN_379
        SIGNAL XLXN_381
        SIGNAL XLXN_383
        SIGNAL XLXN_50
        SIGNAL XLXN_51
        SIGNAL XLXN_52
        SIGNAL XLXN_53
        SIGNAL XLXN_54
        SIGNAL XLXN_56
        SIGNAL XLXN_59
        SIGNAL XLXN_58
        SIGNAL XLXN_57
        SIGNAL Yin
        SIGNAL Y
        SIGNAL X_INPUT
        SIGNAL RST_INPUT
        SIGNAL XLXN_80
        SIGNAL XLXN_77
        SIGNAL XLXN_76
        PORT Input CLK
        PORT Output Y
        PORT Input X_INPUT
        PORT Input RST_INPUT
        BEGIN BLOCKDEF and4b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -208 144 -208 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF and3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -176 144 -176 
            LINE N 144 -80 64 -80 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 64 -64 64 -192 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 40 -192 
            CIRCLE N 40 -204 64 -180 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 64 -48 64 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCKDEF or5
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 48 -128 
            LINE N 0 -192 72 -192 
            LINE N 0 -256 48 -256 
            LINE N 0 -320 48 -320 
            LINE N 256 -192 192 -192 
            ARC N 28 -320 204 -144 112 -144 192 -192 
            LINE N 112 -240 48 -240 
            LINE N 112 -144 48 -144 
            LINE N 48 -64 48 -144 
            LINE N 48 -320 48 -240 
            ARC N 28 -240 204 -64 192 -192 112 -240 
            ARC N -40 -248 72 -136 48 -144 48 -240 
        END BLOCKDEF
        BEGIN BLOCKDEF or4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 48 -256 
            LINE N 256 -160 192 -160 
            ARC N 28 -208 204 -32 192 -160 112 -208 
            LINE N 112 -208 48 -208 
            LINE N 112 -112 48 -112 
            LINE N 48 -256 48 -208 
            LINE N 48 -64 48 -112 
            ARC N -40 -216 72 -104 48 -112 48 -208 
            ARC N 28 -288 204 -112 112 -112 192 -160 
        END BLOCKDEF
        BEGIN BLOCKDEF or3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 72 -128 
            LINE N 0 -192 48 -192 
            LINE N 256 -128 192 -128 
            ARC N 28 -256 204 -80 112 -80 192 -128 
            ARC N -40 -184 72 -72 48 -80 48 -176 
            LINE N 48 -64 48 -80 
            LINE N 48 -192 48 -176 
            LINE N 112 -80 48 -80 
            ARC N 28 -176 204 0 192 -128 112 -176 
            LINE N 112 -176 48 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF ibuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF bufg
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 0 
            LINE N 128 -32 64 -64 
            LINE N 64 0 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF fdc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -128 64 -128 
            LINE N 0 -32 64 -32 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            RECTANGLE N 64 -320 320 -64 
            LINE N 64 -112 80 -128 
            LINE N 80 -128 64 -144 
            LINE N 192 -64 192 -32 
            LINE N 192 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 and4b2
            PIN I0 X
            PIN I1 Q2
            PIN I2 Q4
            PIN I3 Q3
            PIN O XLXN_2
        END BLOCK
        BEGIN BLOCK XLXI_2 and4b1
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q2
            PIN I3 X
            PIN O XLXN_3
        END BLOCK
        BEGIN BLOCK XLXI_3 and3b2
            PIN I0 X
            PIN I1 Q4
            PIN I2 Q2
            PIN O XLXN_14
        END BLOCK
        BEGIN BLOCK XLXI_4 and2
            PIN I0 Q1
            PIN I1 X
            PIN O XLXN_13
        END BLOCK
        BEGIN BLOCK XLXI_5 and3b2
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q2
            PIN O XLXN_15
        END BLOCK
        BEGIN BLOCK XLXI_6 and3
            PIN I0 Q4
            PIN I1 Q2
            PIN I2 X
            PIN O XLXN_16
        END BLOCK
        BEGIN BLOCK XLXI_7 and3b1
            PIN I0 Q2
            PIN I1 Q3
            PIN I2 X
            PIN O XLXN_17
        END BLOCK
        BEGIN BLOCK XLXI_22 or2
            PIN I0 XLXN_3
            PIN I1 XLXN_2
            PIN O D1in
        END BLOCK
        BEGIN BLOCK XLXI_23 or5
            PIN I0 XLXN_17
            PIN I1 XLXN_16
            PIN I2 XLXN_15
            PIN I3 XLXN_13
            PIN I4 XLXN_14
            PIN O D2in
        END BLOCK
        BEGIN BLOCK XLXI_8 and3b2
            PIN I0 X
            PIN I1 Q4
            PIN I2 Q3
            PIN O XLXN_54
        END BLOCK
        BEGIN BLOCK XLXI_10 and2b1
            PIN I0 X
            PIN I1 Q1
            PIN O XLXN_52
        END BLOCK
        BEGIN BLOCK XLXI_11 and3b1
            PIN I0 Q3
            PIN I1 Q2
            PIN I2 X
            PIN O XLXN_51
        END BLOCK
        BEGIN BLOCK XLXI_12 and4b3
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q1
            PIN I3 X
            PIN O XLXN_50
        END BLOCK
        BEGIN BLOCK XLXI_24 or5
            PIN I0 XLXN_50
            PIN I1 XLXN_51
            PIN I2 XLXN_52
            PIN I3 XLXN_53
            PIN I4 XLXN_54
            PIN O D3in
        END BLOCK
        BEGIN BLOCK XLXI_9 and3b1
            PIN I0 X
            PIN I1 Q3
            PIN I2 Q2
            PIN O XLXN_53
        END BLOCK
        BEGIN BLOCK XLXI_25 or4
            PIN I0 XLXN_59
            PIN I1 XLXN_58
            PIN I2 XLXN_57
            PIN I3 XLXN_56
            PIN O D4in
        END BLOCK
        BEGIN BLOCK XLXI_17 and4b2
            PIN I0 Q3
            PIN I1 Q2
            PIN I2 Q4
            PIN I3 X
            PIN O XLXN_59
        END BLOCK
        BEGIN BLOCK XLXI_16 and3b2
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q2
            PIN O XLXN_58
        END BLOCK
        BEGIN BLOCK XLXI_15 and2b2
            PIN I0 Q4
            PIN I1 X
            PIN O XLXN_57
        END BLOCK
        BEGIN BLOCK XLXI_14 and3b2
            PIN I0 Q3
            PIN I1 X
            PIN I2 Q2
            PIN O XLXN_56
        END BLOCK
        BEGIN BLOCK XLXI_31 obuf
            PIN I Yin
            PIN O Y
        END BLOCK
        BEGIN BLOCK XLXI_35 fdc
            PIN C CLK
            PIN CLR RST
            PIN D D3in
            PIN Q Q3
        END BLOCK
        BEGIN BLOCK XLXI_34 fdc
            PIN C CLK
            PIN CLR RST
            PIN D D4in
            PIN Q Q4
        END BLOCK
        BEGIN BLOCK XLXI_33 fdc
            PIN C CLK
            PIN CLR RST
            PIN D D2in
            PIN Q Q2
        END BLOCK
        BEGIN BLOCK XLXI_39 fdc
            PIN C CLK
            PIN CLR RST
            PIN D D1in
            PIN Q Q1
        END BLOCK
        BEGIN BLOCK XLXI_30 bufg
            PIN I CLK
            PIN O CLK
        END BLOCK
        BEGIN BLOCK XLXI_29 ibuf
            PIN I X_INPUT
            PIN O X
        END BLOCK
        BEGIN BLOCK XLXI_28 ibuf
            PIN I RST_INPUT
            PIN O RST
        END BLOCK
        BEGIN BLOCK XLXI_26 or3
            PIN I0 XLXN_80
            PIN I1 XLXN_77
            PIN I2 XLXN_76
            PIN O Yin
        END BLOCK
        BEGIN BLOCK XLXI_20 and2b1
            PIN I0 Q4
            PIN I1 X
            PIN O XLXN_80
        END BLOCK
        BEGIN BLOCK XLXI_19 and2b1
            PIN I0 Q3
            PIN I1 X
            PIN O XLXN_77
        END BLOCK
        BEGIN BLOCK XLXI_18 and3b1
            PIN I0 X
            PIN I1 Q4
            PIN I2 Q3
            PIN O XLXN_76
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_1 224 384 R0
        INSTANCE XLXI_2 224 656 R0
        INSTANCE XLXI_3 176 944 R0
        INSTANCE XLXI_4 176 1088 R0
        INSTANCE XLXI_5 176 1280 R0
        INSTANCE XLXI_6 176 1488 R0
        INSTANCE XLXI_7 176 1664 R0
        INSTANCE XLXI_23 496 1344 R0
        INSTANCE XLXI_22 624 464 R0
        BEGIN BRANCH XLXN_2
            WIRE 480 224 544 224
            WIRE 544 224 544 336
            WIRE 544 336 624 336
        END BRANCH
        BEGIN BRANCH XLXN_3
            WIRE 480 496 544 496
            WIRE 544 400 544 496
            WIRE 544 400 624 400
        END BRANCH
        BEGIN BRANCH D1in
            WIRE 880 368 960 368
            WIRE 960 368 992 368
            BEGIN DISPLAY 960 368 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 128 128 224 128
            BEGIN DISPLAY 128 128 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 128 192 224 192
            BEGIN DISPLAY 128 192 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 128 256 224 256
            BEGIN DISPLAY 128 256 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 128 320 224 320
            BEGIN DISPLAY 128 320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 128 400 224 400
            BEGIN DISPLAY 128 400 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 128 464 224 464
            BEGIN DISPLAY 128 464 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 128 528 224 528
            BEGIN DISPLAY 128 528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 128 592 224 592
            BEGIN DISPLAY 128 592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_13
            WIRE 432 992 464 992
            WIRE 464 992 464 1088
            WIRE 464 1088 496 1088
        END BRANCH
        BEGIN BRANCH XLXN_14
            WIRE 432 816 496 816
            WIRE 496 816 496 1024
        END BRANCH
        BEGIN BRANCH XLXN_15
            WIRE 432 1152 496 1152
        END BRANCH
        BEGIN BRANCH XLXN_16
            WIRE 432 1360 464 1360
            WIRE 464 1216 464 1360
            WIRE 464 1216 496 1216
        END BRANCH
        BEGIN BRANCH XLXN_17
            WIRE 432 1536 496 1536
            WIRE 496 1280 496 1536
        END BRANCH
        BEGIN BRANCH D2in
            WIRE 752 1152 800 1152
            WIRE 800 1152 832 1152
            BEGIN DISPLAY 796 1152 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 752 176 752
            BEGIN DISPLAY 96 752 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 816 176 816
            BEGIN DISPLAY 96 816 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 880 176 880
            BEGIN DISPLAY 96 880 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 960 176 960
            BEGIN DISPLAY 96 960 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 96 1024 176 1024
            BEGIN DISPLAY 96 1024 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 1088 176 1088
            BEGIN DISPLAY 96 1088 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 96 1152 176 1152
            BEGIN DISPLAY 96 1152 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 1216 176 1216
            BEGIN DISPLAY 96 1216 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 1296 176 1296
            BEGIN DISPLAY 80 1296 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 1360 176 1360
            BEGIN DISPLAY 96 1360 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 1424 176 1424
            BEGIN DISPLAY 96 1424 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 1472 176 1472
            BEGIN DISPLAY 96 1472 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 96 1536 176 1536
            BEGIN DISPLAY 96 1536 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 1600 176 1600
            BEGIN DISPLAY 96 1600 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_8 160 1952 R0
        INSTANCE XLXI_10 144 2256 R0
        INSTANCE XLXI_11 160 2448 R0
        INSTANCE XLXI_12 160 2720 R0
        INSTANCE XLXI_24 480 2352 R0
        BEGIN BRANCH Q3
            WIRE 96 1760 160 1760
            BEGIN DISPLAY 96 1760 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 1824 160 1824
            BEGIN DISPLAY 96 1824 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 1888 160 1888
            BEGIN DISPLAY 96 1888 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 80 1936 96 1936
            WIRE 96 1920 128 1920
            WIRE 96 1920 96 1936
            BEGIN DISPLAY 80 1936 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 80 2000 96 2000
            WIRE 96 1984 128 1984
            WIRE 96 1984 96 2000
            BEGIN DISPLAY 80 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 2064 96 2064
            WIRE 96 2048 128 2048
            WIRE 96 2048 96 2064
            BEGIN DISPLAY 80 2064 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 80 2128 144 2128
            BEGIN DISPLAY 80 2128 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 2192 144 2192
            BEGIN DISPLAY 80 2192 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 2256 160 2256
            BEGIN DISPLAY 80 2256 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 80 2320 160 2320
            BEGIN DISPLAY 80 2320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 80 2384 160 2384
            BEGIN DISPLAY 80 2384 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 2464 160 2464
            BEGIN DISPLAY 96 2464 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 96 2528 160 2528
            BEGIN DISPLAY 96 2528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 96 2592 160 2592
            BEGIN DISPLAY 96 2592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 2656 160 2656
            BEGIN DISPLAY 96 2656 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_50
            WIRE 416 2560 480 2560
            WIRE 480 2288 480 2560
        END BRANCH
        BEGIN BRANCH XLXN_51
            WIRE 416 2320 448 2320
            WIRE 448 2224 448 2320
            WIRE 448 2224 480 2224
        END BRANCH
        BEGIN BRANCH XLXN_52
            WIRE 400 2160 480 2160
        END BRANCH
        BEGIN BRANCH XLXN_53
            WIRE 384 1984 464 1984
            WIRE 464 1984 464 2096
            WIRE 464 2096 480 2096
        END BRANCH
        BEGIN BRANCH XLXN_54
            WIRE 416 1824 480 1824
            WIRE 480 1824 480 2032
        END BRANCH
        BEGIN BRANCH D3in
            WIRE 736 2160 784 2160
            WIRE 784 2160 848 2160
            BEGIN DISPLAY 784 2160 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_9 128 2112 R0
        INSTANCE XLXI_25 1456 2400 R0
        INSTANCE XLXI_17 1152 2704 R0
        INSTANCE XLXI_16 1152 2432 R0
        INSTANCE XLXI_15 1136 2208 R0
        INSTANCE XLXI_14 1152 2016 R0
        BEGIN BRANCH XLXN_56
            WIRE 1408 1888 1456 1888
            WIRE 1456 1888 1456 2144
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 1040 2640 1152 2640
            BEGIN DISPLAY 1040 2640 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 1040 2576 1152 2576
            BEGIN DISPLAY 1040 2576 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 1040 2512 1152 2512
            BEGIN DISPLAY 1040 2512 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1040 2448 1152 2448
            BEGIN DISPLAY 1040 2448 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 1040 2368 1152 2368
            BEGIN DISPLAY 1040 2368 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 1040 2304 1152 2304
            BEGIN DISPLAY 1040 2304 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 1040 2240 1152 2240
            BEGIN DISPLAY 1040 2240 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 1040 2144 1136 2144
            BEGIN DISPLAY 1040 2144 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1040 2080 1136 2080
            BEGIN DISPLAY 1040 2080 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D4in
            WIRE 1712 2240 1776 2240
            WIRE 1776 2240 1824 2240
            BEGIN DISPLAY 1772 2240 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_59
            WIRE 1408 2544 1456 2544
            WIRE 1456 2336 1456 2544
        END BRANCH
        BEGIN BRANCH XLXN_58
            WIRE 1408 2304 1424 2304
            WIRE 1424 2272 1424 2304
            WIRE 1424 2272 1456 2272
        END BRANCH
        BEGIN BRANCH XLXN_57
            WIRE 1392 2112 1424 2112
            WIRE 1424 2112 1424 2208
            WIRE 1424 2208 1456 2208
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 1040 1952 1152 1952
            BEGIN DISPLAY 1040 1952 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1040 1888 1152 1888
            BEGIN DISPLAY 1040 1888 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 1040 1824 1152 1824
            BEGIN DISPLAY 1040 1824 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_31 2992 1344 R0
        BEGIN BRANCH Y
            WIRE 3216 1312 3344 1312
        END BRANCH
        BEGIN BRANCH Yin
            WIRE 2864 1312 2880 1312
            WIRE 2880 1312 2992 1312
            BEGIN DISPLAY 2864 1312 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 2884 1312 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        IOMARKER 3344 1312 Y R0 28
        INSTANCE XLXI_35 1920 1744 R0
        BEGIN BRANCH RST
            WIRE 1824 1712 1920 1712
            BEGIN DISPLAY 1824 1712 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D3in
            WIRE 1808 1488 1920 1488
            BEGIN DISPLAY 1808 1488 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1808 1616 1920 1616
            BEGIN DISPLAY 1808 1616 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 2304 1488 2384 1488
            WIRE 2384 1488 2416 1488
            BEGIN DISPLAY 2384 1488 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_34 1920 2128 R0
        BEGIN BRANCH D4in
            WIRE 1824 1872 1920 1872
            BEGIN DISPLAY 1824 1872 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1808 2000 1920 2000
            BEGIN DISPLAY 1808 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH RST
            WIRE 1808 2096 1920 2096
            BEGIN DISPLAY 1808 2096 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 2304 1872 2384 1872
            WIRE 2384 1872 2416 1872
            BEGIN DISPLAY 2384 1872 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 2288 1056 2368 1056
            WIRE 2368 1056 2400 1056
            BEGIN DISPLAY 2368 1056 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D2in
            WIRE 1792 1056 1904 1056
            BEGIN DISPLAY 1792 1056 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1808 1184 1904 1184
            BEGIN DISPLAY 1808 1184 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH RST
            WIRE 1808 1280 1904 1280
            BEGIN DISPLAY 1808 1280 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_33 1904 1312 R0
        INSTANCE XLXI_39 1904 896 R0
        BEGIN BRANCH RST
            WIRE 1792 864 1904 864
            BEGIN DISPLAY 1792 864 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1792 768 1904 768
            BEGIN DISPLAY 1792 768 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D1in
            WIRE 1792 640 1904 640
            BEGIN DISPLAY 1792 640 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 2288 640 2368 640
            WIRE 2368 640 2400 640
            BEGIN DISPLAY 2368 640 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1296 848 1328 848
            BEGIN DISPLAY 1328 848 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1040 848 1072 848
        END BRANCH
        INSTANCE XLXI_30 1072 880 R0
        BEGIN BRANCH X_INPUT
            WIRE 1008 736 1072 736
        END BRANCH
        INSTANCE XLXI_29 1072 768 R0
        INSTANCE XLXI_28 1072 1008 R0
        BEGIN BRANCH RST_INPUT
            WIRE 1040 976 1072 976
        END BRANCH
        BEGIN BRANCH RST
            WIRE 1296 976 1376 976
            WIRE 1376 976 1392 976
            WIRE 1392 976 1408 976
            BEGIN DISPLAY 1376 976 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1296 736 1328 736
            WIRE 1328 736 1360 736
            WIRE 1360 736 1632 736
            BEGIN DISPLAY 1632 736 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        IOMARKER 1040 848 CLK R180 28
        IOMARKER 1008 736 X_INPUT R180 28
        IOMARKER 1040 976 RST_INPUT R180 28
        BEGIN BRANCH Q4
            WIRE 2624 1040 2640 1040
            WIRE 2640 1040 2720 1040
            BEGIN DISPLAY 2624 1040 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 2624 864 2640 864
            WIRE 2640 864 2720 864
            BEGIN DISPLAY 2624 864 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 2624 640 2640 640
            WIRE 2640 640 2720 640
            BEGIN DISPLAY 2624 640 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 2624 576 2640 576
            WIRE 2640 576 2720 576
            BEGIN DISPLAY 2624 576 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_26 3040 960 R0
        INSTANCE XLXI_20 2720 1104 R0
        INSTANCE XLXI_19 2720 928 R0
        INSTANCE XLXI_18 2720 768 R0
        BEGIN BRANCH Yin
            WIRE 3296 832 3376 832
            WIRE 3376 832 3424 832
            BEGIN DISPLAY 3376 832 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 2624 976 2720 976
            BEGIN DISPLAY 2624 976 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 2624 800 2720 800
            BEGIN DISPLAY 2624 800 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_80
            WIRE 2976 1008 3040 1008
            WIRE 3040 896 3040 1008
        END BRANCH
        BEGIN BRANCH XLXN_77
            WIRE 2976 832 3040 832
        END BRANCH
        BEGIN BRANCH XLXN_76
            WIRE 2976 640 3040 640
            WIRE 3040 640 3040 768
        END BRANCH
        BEGIN BRANCH X
            WIRE 2624 704 2720 704
            BEGIN DISPLAY 2624 704 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
    END SHEET
END SCHEMATIC
