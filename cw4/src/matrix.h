#ifndef MATRIX_H
#define MATRIX_H

typedef struct DoubleMatrix {
	double **arr;
	unsigned int sizeA;
	unsigned int sizeB;
} DoubleMatrix;

DoubleMatrix double_matrix_new(unsigned int sizeA, unsigned int sizeB);
void double_matrix_del(DoubleMatrix *matrix);
void double_matrix_resizeA(DoubleMatrix *matrix, unsigned int newSizeA);
DoubleMatrix double_matrix_mult(DoubleMatrix matrix1, DoubleMatrix matrix2);

typedef struct IntMatrixDynamic {
	int **arr;
	unsigned int sizeA;
	unsigned int defaultSizeB;
	unsigned int *sizesB;
} IntMatrixDynamic;

IntMatrixDynamic int_matrix_dynamic_new(unsigned int sizeA, unsigned int defaultSizeB);
void int_matrix_dynamic_del(IntMatrixDynamic *matrix);
void int_matrix_dynamic_resizeA(IntMatrixDynamic *matrix, unsigned int newSizeA);
void int_matrix_dynamic_resizeB(IntMatrixDynamic *matrix, unsigned int b, unsigned int newSizeB);

#endif
