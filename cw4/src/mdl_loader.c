#include "mdl_loader.h"

#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 200

void strreplace(char *string, char from, char to) {
	for (unsigned int i = 0; i < strlen(string); i++)
		if (string[i] == from)
			string[i] = to;
}

unsigned int countLines(FILE *file) {
	unsigned int lines = 0;
	char buf[MAX_LINE_LENGTH];
	while (fgets(buf, sizeof(buf), file) != NULL)
		lines++;
	rewind(file);
	return lines;
}

MDL readMDL(FILE *file) {
	unsigned int lineCount = countLines(file);

	DoubleMatrix points = double_matrix_new(lineCount, 4);
	IntMatrixDynamic surfaces = int_matrix_dynamic_new(lineCount, 3);
	unsigned int pointCount = 0;
	unsigned int surfaceCount = 0;

	char currentLine[MAX_LINE_LENGTH] = {0};
	char *linePtr = NULL;

	unsigned int lineIndex = 0;
	while (fgets(currentLine, sizeof(currentLine), file) != NULL) {
		linePtr = currentLine;
		if (currentLine[1] != ' ')
			continue;

		if (currentLine[0] == 'v') {
			linePtr = &currentLine[1];
			for (unsigned int i = 0; i < 3; i++)
				points.arr[pointCount][i] = strtod(linePtr, &linePtr) - 2;
			pointCount++;

		} else if (currentLine[0] == 'f') {
			linePtr = &currentLine[1];

			unsigned int i = 0;
			while (linePtr[0] != '\n' && linePtr[0] != '\0' && (linePtr[0] != ' ' || linePtr[1] != '\0')) {
				linePtr = strchr(linePtr, ' ');
				if (!linePtr)
					break;
				surfaces.arr[surfaceCount][i] = strtol(linePtr, &linePtr, 10) - 1;
				i++;
				if (i >= surfaces.sizesB[surfaceCount])
					int_matrix_dynamic_resizeB(&surfaces, surfaceCount, (unsigned int) (i * 1.5));
			}
			int_matrix_dynamic_resizeB(&surfaces, surfaceCount, i);
			surfaceCount++;
		}
		lineIndex++;
	}

	double_matrix_resizeA(&points, pointCount);
	int_matrix_dynamic_resizeA(&surfaces, surfaceCount);

	MDL model = {0};
	model.points = points;
	model.surfaces = surfaces;

	return model;
}

