VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL Q(1:4)
        SIGNAL X
        SIGNAL IN_X
        SIGNAL IN_C
        SIGNAL C
        SIGNAL IN_RST
        SIGNAL RST
        SIGNAL XLXN_459
        SIGNAL XLXN_460
        SIGNAL Q(2)
        SIGNAL Q(1)
        SIGNAL Q(3)
        SIGNAL XLXN_27
        SIGNAL XLXN_360
        SIGNAL Y
        SIGNAL XLXN_35
        SIGNAL XLXN_100
        SIGNAL XLXN_363
        SIGNAL XLXN_854
        SIGNAL XLXN_560
        SIGNAL XLXN_84
        SIGNAL XLXN_562
        SIGNAL XLXN_96
        SIGNAL Q(4)
        SIGNAL XLXN_875
        SIGNAL XLXN_876
        SIGNAL XLXN_877
        SIGNAL XLXN_878
        SIGNAL XLXN_879
        SIGNAL XLXN_769
        SIGNAL XLXN_853
        SIGNAL XLXN_910
        SIGNAL XLXN_938
        SIGNAL XLXN_940
        SIGNAL XLXN_949
        SIGNAL XLXN_950
        SIGNAL XLXN_951
        SIGNAL XLXN_955
        SIGNAL XLXN_964
        SIGNAL XLXN_965
        SIGNAL XLXN_991
        SIGNAL XLXN_992
        SIGNAL XLXN_1006
        SIGNAL XLXN_1049
        SIGNAL XLXN_1050
        SIGNAL XLXN_1051
        SIGNAL XLXN_1052
        SIGNAL XLXN_1053
        SIGNAL XLXN_1054
        SIGNAL XLXN_1055
        PORT Input IN_X
        PORT Input IN_C
        PORT Input IN_RST
        PORT Output Y
        BEGIN BLOCKDEF ibuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF ibufg
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF or4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 48 -256 
            LINE N 256 -160 192 -160 
            ARC N 28 -208 204 -32 192 -160 112 -208 
            LINE N 112 -208 48 -208 
            LINE N 112 -112 48 -112 
            LINE N 48 -256 48 -208 
            LINE N 48 -64 48 -112 
            ARC N -40 -216 72 -104 48 -112 48 -208 
            ARC N 28 -288 204 -112 112 -112 192 -160 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -208 144 -208 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
        END BLOCKDEF
        BEGIN BLOCKDEF buf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
            LINE N 64 0 128 -32 
            LINE N 128 -32 64 -64 
            LINE N 64 -64 64 0 
        END BLOCKDEF
        BEGIN BLOCKDEF fdc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -128 64 -128 
            LINE N 0 -32 64 -32 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            RECTANGLE N 64 -320 320 -64 
            LINE N 64 -112 80 -128 
            LINE N 80 -128 64 -144 
            LINE N 192 -64 192 -32 
            LINE N 192 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF or5
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 48 -128 
            LINE N 0 -192 72 -192 
            LINE N 0 -256 48 -256 
            LINE N 0 -320 48 -320 
            LINE N 256 -192 192 -192 
            ARC N 28 -320 204 -144 112 -144 192 -192 
            LINE N 112 -240 48 -240 
            LINE N 112 -144 48 -144 
            LINE N 48 -64 48 -144 
            LINE N 48 -320 48 -240 
            ARC N 28 -240 204 -64 192 -192 112 -240 
            ARC N -40 -248 72 -136 48 -144 48 -240 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 40 -192 
            CIRCLE N 40 -204 64 -180 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
        END BLOCKDEF
        BEGIN BLOCKDEF or8
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 48 -128 
            LINE N 0 -192 48 -192 
            LINE N 0 -384 48 -384 
            LINE N 0 -448 48 -448 
            LINE N 0 -512 48 -512 
            LINE N 256 -288 192 -288 
            LINE N 0 -320 64 -320 
            LINE N 0 -256 64 -256 
            ARC N 28 -336 204 -160 192 -288 112 -336 
            LINE N 112 -240 48 -240 
            ARC N 28 -416 204 -240 112 -240 192 -288 
            ARC N -40 -344 72 -232 48 -240 48 -336 
            LINE N 112 -336 48 -336 
            LINE N 48 -336 48 -512 
            LINE N 48 -64 48 -240 
        END BLOCKDEF
        BEGIN BLOCK XLXI_149 ibuf
            PIN I IN_X
            PIN O X
        END BLOCK
        BEGIN BLOCK XLXI_150 ibufg
            PIN I IN_C
            PIN O C
        END BLOCK
        BEGIN BLOCK XLXI_218 ibuf
            PIN I IN_RST
            PIN O RST
        END BLOCK
        BEGIN BLOCK XLXI_221 and3b2
            PIN I0 Q(2)
            PIN I1 X
            PIN I2 Q(1)
            PIN O XLXN_459
        END BLOCK
        BEGIN BLOCK XLXI_153 buf
            PIN I XLXN_360
            PIN O Q(1)
        END BLOCK
        BEGIN BLOCK FF1 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_27
            PIN Q XLXN_360
        END BLOCK
        BEGIN BLOCK XLXI_183 obuf
            PIN I XLXN_854
            PIN O Y
        END BLOCK
        BEGIN BLOCK XLXI_152 buf
            PIN I XLXN_363
            PIN O Q(2)
        END BLOCK
        BEGIN BLOCK FF2 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_100
            PIN Q XLXN_363
        END BLOCK
        BEGIN BLOCK XLXI_326 or2
            PIN I0 XLXN_460
            PIN I1 XLXN_459
            PIN O XLXN_27
        END BLOCK
        BEGIN BLOCK XLXI_327 and4b3
            PIN I0 Q(4)
            PIN I1 Q(3)
            PIN I2 X
            PIN I3 Q(2)
            PIN O XLXN_460
        END BLOCK
        BEGIN BLOCK XLXI_331 or5
            PIN I0 XLXN_879
            PIN I1 XLXN_878
            PIN I2 XLXN_877
            PIN I3 XLXN_876
            PIN I4 XLXN_875
            PIN O XLXN_100
        END BLOCK
        BEGIN BLOCK XLXI_334 and4b2
            PIN I0 Q(3)
            PIN I1 X
            PIN I2 Q(4)
            PIN I3 Q(2)
            PIN O XLXN_877
        END BLOCK
        BEGIN BLOCK XLXI_335 and4b2
            PIN I0 Q(4)
            PIN I1 Q(1)
            PIN I2 Q(2)
            PIN I3 X
            PIN O XLXN_878
        END BLOCK
        BEGIN BLOCK XLXI_336 and4b2
            PIN I0 Q(3)
            PIN I1 Q(1)
            PIN I2 Q(4)
            PIN I3 X
            PIN O XLXN_879
        END BLOCK
        BEGIN BLOCK XLXI_329 and4b3
            PIN I0 Q(4)
            PIN I1 Q(2)
            PIN I2 X
            PIN I3 Q(1)
            PIN O XLXN_876
        END BLOCK
        BEGIN BLOCK XLXI_333 and4b2
            PIN I0 Q(2)
            PIN I1 X
            PIN I2 Q(4)
            PIN I3 Q(3)
            PIN O XLXN_875
        END BLOCK
        BEGIN BLOCK XLXI_324 buf
            PIN I XLXN_769
            PIN O Q(3)
        END BLOCK
        BEGIN BLOCK XLXI_325 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_853
            PIN Q XLXN_769
        END BLOCK
        BEGIN BLOCK XLXI_342 or4
            PIN I0 XLXN_950
            PIN I1 XLXN_949
            PIN I2 XLXN_940
            PIN I3 XLXN_938
            PIN O XLXN_853
        END BLOCK
        BEGIN BLOCK XLXI_343 and4b3
            PIN I0 Q(3)
            PIN I1 Q(2)
            PIN I2 X
            PIN I3 Q(4)
            PIN O XLXN_938
        END BLOCK
        BEGIN BLOCK XLXI_259 and4b1
            PIN I0 X
            PIN I1 Q(4)
            PIN I2 Q(3)
            PIN I3 Q(2)
            PIN O XLXN_940
        END BLOCK
        BEGIN BLOCK XLXI_345 and2b1
            PIN I0 Q(4)
            PIN I1 Q(3)
            PIN O XLXN_950
        END BLOCK
        BEGIN BLOCK XLXI_346 and2
            PIN I0 Q(2)
            PIN I1 X
            PIN O XLXN_949
        END BLOCK
        BEGIN BLOCK XLXI_355 buf
            PIN I XLXN_951
            PIN O Q(4)
        END BLOCK
        BEGIN BLOCK XLXI_356 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_955
            PIN Q XLXN_951
        END BLOCK
        BEGIN BLOCK XLXI_357 or4
            PIN I0 XLXN_992
            PIN I1 XLXN_991
            PIN I2 XLXN_965
            PIN I3 XLXN_964
            PIN O XLXN_955
        END BLOCK
        BEGIN BLOCK XLXI_362 and4b2
            PIN I0 Q(4)
            PIN I1 X
            PIN I2 Q(3)
            PIN I3 Q(2)
            PIN O XLXN_964
        END BLOCK
        BEGIN BLOCK XLXI_363 and2
            PIN I0 Q(1)
            PIN I1 X
            PIN O XLXN_965
        END BLOCK
        BEGIN BLOCK XLXI_364 and3b1
            PIN I0 Q(3)
            PIN I1 Q(2)
            PIN I2 X
            PIN O XLXN_991
        END BLOCK
        BEGIN BLOCK XLXI_365 and3b2
            PIN I0 Q(2)
            PIN I1 Q(1)
            PIN I2 X
            PIN O XLXN_992
        END BLOCK
        BEGIN BLOCK XLXI_373 or8
            PIN I0 XLXN_1055
            PIN I1 XLXN_1054
            PIN I2 XLXN_1053
            PIN I3 XLXN_1052
            PIN I4 Q(1)
            PIN I5 XLXN_1051
            PIN I6 XLXN_1050
            PIN I7 XLXN_1049
            PIN O XLXN_854
        END BLOCK
        BEGIN BLOCK XLXI_378 and4b3
            PIN I0 Q(2)
            PIN I1 Q(1)
            PIN I2 X
            PIN I3 Q(4)
            PIN O XLXN_1049
        END BLOCK
        BEGIN BLOCK XLXI_379 and4b2
            PIN I0 Q(1)
            PIN I1 X
            PIN I2 Q(4)
            PIN I3 Q(3)
            PIN O XLXN_1050
        END BLOCK
        BEGIN BLOCK XLXI_380 and4b3
            PIN I0 Q(2)
            PIN I1 Q(1)
            PIN I2 X
            PIN I3 Q(3)
            PIN O XLXN_1051
        END BLOCK
        BEGIN BLOCK XLXI_384 and4b2
            PIN I0 Q(3)
            PIN I1 Q(1)
            PIN I2 Q(2)
            PIN I3 X
            PIN O XLXN_1052
        END BLOCK
        BEGIN BLOCK XLXI_385 and4b3
            PIN I0 Q(4)
            PIN I1 Q(3)
            PIN I2 Q(1)
            PIN I3 X
            PIN O XLXN_1053
        END BLOCK
        BEGIN BLOCK XLXI_386 and4b1
            PIN I0 Q(2)
            PIN I1 Q(4)
            PIN I2 Q(3)
            PIN I3 X
            PIN O XLXN_1054
        END BLOCK
        BEGIN BLOCK XLXI_387 and4b2
            PIN I0 Q(4)
            PIN I1 Q(1)
            PIN I2 Q(2)
            PIN I3 X
            PIN O XLXN_1055
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 7040 5440
        BEGIN BRANCH Q(1:4)
            WIRE 4256 976 4416 976
            WIRE 4416 976 4736 976
            BEGIN DISPLAY 4416 976 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 480 752 624 752
            WIRE 624 752 768 752
            BEGIN DISPLAY 624 752 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_149 256 784 R0
        BEGIN BRANCH IN_X
            WIRE 224 752 240 752
            WIRE 240 752 256 752
            BEGIN DISPLAY 240 752 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_150 304 1712 R0
        BEGIN BRANCH IN_C
            WIRE 272 1680 304 1680
        END BRANCH
        BEGIN BRANCH C
            WIRE 528 1680 752 1680
            WIRE 752 1680 752 1680
            WIRE 752 1680 880 1680
            BEGIN DISPLAY 756 1680 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_218 288 1056 R0
        BEGIN BRANCH IN_RST
            WIRE 256 1024 288 1024
        END BRANCH
        BEGIN BRANCH RST
            WIRE 512 1024 656 1024
            WIRE 656 1024 800 1024
            BEGIN DISPLAY 656 1024 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_221 1808 624 R0
        BEGIN BRANCH XLXN_459
            WIRE 2064 496 2224 496
            WIRE 2224 496 2224 672
            WIRE 2224 672 2400 672
        END BRANCH
        BEGIN BRANCH XLXN_460
            WIRE 2064 816 2224 816
            WIRE 2224 736 2224 816
            WIRE 2224 736 2400 736
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 1696 432 1808 432
            BEGIN DISPLAY 1696 432 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1696 496 1808 496
            BEGIN DISPLAY 1696 496 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1696 560 1808 560
            BEGIN DISPLAY 1696 560 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_27
            WIRE 2656 704 2912 704
            WIRE 2912 704 3024 704
            WIRE 3024 704 3024 768
            WIRE 3024 768 3040 768
        END BRANCH
        BEGIN BRANCH C
            WIRE 2912 896 3040 896
            BEGIN DISPLAY 2912 896 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_360
            WIRE 3424 768 3568 768
        END BRANCH
        INSTANCE XLXI_153 3568 800 R0
        BEGIN BRANCH Q(1)
            WIRE 3792 768 3936 768
            WIRE 3936 768 4080 768
            BEGIN DISPLAY 3936 768 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE FF1 3040 1024 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 2768 992 2912 992
            WIRE 2912 992 3040 992
            BEGIN DISPLAY 2768 992 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 2912 992 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_183 5552 2448 R0
        BEGIN BRANCH Y
            WIRE 5776 2416 5808 2416
        END BRANCH
        BEGIN BRANCH XLXN_100
            WIRE 2656 1808 2768 1808
        END BRANCH
        BEGIN BRANCH C
            WIRE 2640 1936 2768 1936
            BEGIN DISPLAY 2640 1936 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_152 3296 1840 R0
        BEGIN BRANCH XLXN_363
            WIRE 3152 1808 3296 1808
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 3520 1808 3616 1808
            WIRE 3616 1808 3728 1808
            BEGIN DISPLAY 3616 1808 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE FF2 2768 2064 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 2528 2032 2624 2032
            WIRE 2624 2032 2624 2032
            WIRE 2624 2032 2768 2032
            BEGIN DISPLAY 2528 2032 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 2628 2032 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_854
            WIRE 5440 2416 5552 2416
        END BRANCH
        IOMARKER 224 752 IN_X R180 28
        IOMARKER 272 1680 IN_C R180 28
        IOMARKER 256 1024 IN_RST R180 28
        IOMARKER 5808 2416 Y R0 28
        INSTANCE XLXI_326 2400 800 R0
        INSTANCE XLXI_327 1808 976 R0
        BEGIN BRANCH Q(2)
            WIRE 1712 720 1808 720
            BEGIN DISPLAY 1712 720 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1712 784 1808 784
            BEGIN DISPLAY 1712 784 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1712 848 1808 848
            BEGIN DISPLAY 1712 848 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1712 912 1808 912
            BEGIN DISPLAY 1712 912 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_331 2400 2000 R0
        INSTANCE XLXI_334 1888 2032 R0
        INSTANCE XLXI_335 1888 2304 R0
        INSTANCE XLXI_336 1888 2576 R0
        INSTANCE XLXI_329 1888 1760 R0
        BEGIN BRANCH Q(1)
            WIRE 1792 1504 1888 1504
            BEGIN DISPLAY 1792 1504 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1792 1568 1888 1568
            BEGIN DISPLAY 1792 1568 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1792 1632 1888 1632
            BEGIN DISPLAY 1792 1632 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1792 1696 1888 1696
            BEGIN DISPLAY 1792 1696 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_333 1888 1472 R0
        BEGIN BRANCH XLXN_875
            WIRE 2144 1312 2400 1312
            WIRE 2400 1312 2400 1680
        END BRANCH
        BEGIN BRANCH XLXN_876
            WIRE 2144 1600 2272 1600
            WIRE 2272 1600 2272 1744
            WIRE 2272 1744 2400 1744
        END BRANCH
        BEGIN BRANCH XLXN_877
            WIRE 2144 1872 2272 1872
            WIRE 2272 1808 2272 1872
            WIRE 2272 1808 2400 1808
        END BRANCH
        BEGIN BRANCH XLXN_878
            WIRE 2144 2144 2288 2144
            WIRE 2288 1872 2288 2144
            WIRE 2288 1872 2400 1872
        END BRANCH
        BEGIN BRANCH XLXN_879
            WIRE 2144 2416 2400 2416
            WIRE 2400 1936 2400 2416
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1792 1216 1888 1216
            BEGIN DISPLAY 1792 1216 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1792 1280 1888 1280
            BEGIN DISPLAY 1792 1280 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1792 1344 1888 1344
            BEGIN DISPLAY 1792 1344 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1792 1408 1888 1408
            BEGIN DISPLAY 1792 1408 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1792 1776 1888 1776
            BEGIN DISPLAY 1792 1776 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1792 1840 1888 1840
            BEGIN DISPLAY 1792 1840 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1792 1904 1888 1904
            BEGIN DISPLAY 1792 1904 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1792 1968 1888 1968
            BEGIN DISPLAY 1792 1968 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1792 2048 1888 2048
            BEGIN DISPLAY 1792 2048 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1792 2112 1888 2112
            BEGIN DISPLAY 1792 2112 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 1792 2176 1888 2176
            BEGIN DISPLAY 1792 2176 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1792 2240 1888 2240
            BEGIN DISPLAY 1792 2240 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1792 2320 1888 2320
            BEGIN DISPLAY 1792 2320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1792 2384 1888 2384
            BEGIN DISPLAY 1792 2384 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 1792 2448 1888 2448
            BEGIN DISPLAY 1792 2448 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1792 2512 1888 2512
            BEGIN DISPLAY 1792 2512 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_769
            WIRE 3344 3168 3472 3168
            WIRE 3472 3168 3488 3168
        END BRANCH
        INSTANCE XLXI_324 3488 3200 R0
        BEGIN BRANCH Q(3)
            WIRE 3712 3168 3856 3168
            WIRE 3856 3168 4000 3168
            BEGIN DISPLAY 3856 3168 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH RST
            WIRE 2688 3392 2832 3392
            WIRE 2832 3392 2960 3392
            BEGIN DISPLAY 2688 3392 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 2832 3392 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH C
            WIRE 2832 3296 2848 3296
            WIRE 2848 3296 2960 3296
            BEGIN DISPLAY 2832 3296 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_853
            WIRE 2768 3168 2960 3168
        END BRANCH
        BEGIN INSTANCE XLXI_325 2960 3424 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        INSTANCE XLXI_342 2512 3328 R0
        INSTANCE XLXI_343 1888 2976 R0
        BEGIN BRANCH Q(4)
            WIRE 1792 2720 1888 2720
            BEGIN DISPLAY 1792 2720 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1792 2784 1888 2784
            BEGIN DISPLAY 1792 2784 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1792 2848 1888 2848
            BEGIN DISPLAY 1792 2848 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1792 2912 1888 2912
            BEGIN DISPLAY 1792 2912 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_259 1888 3248 R0
        BEGIN BRANCH Q(2)
            WIRE 1776 2992 1888 2992
            BEGIN DISPLAY 1776 2992 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1776 3056 1888 3056
            BEGIN DISPLAY 1776 3056 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1776 3120 1888 3120
            BEGIN DISPLAY 1776 3120 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1776 3184 1888 3184
            BEGIN DISPLAY 1776 3184 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_938
            WIRE 2144 2816 2512 2816
            WIRE 2512 2816 2512 3072
        END BRANCH
        BEGIN BRANCH XLXN_940
            WIRE 2144 3088 2320 3088
            WIRE 2320 3088 2320 3136
            WIRE 2320 3136 2512 3136
        END BRANCH
        INSTANCE XLXI_345 1888 3616 R0
        BEGIN BRANCH Q(3)
            WIRE 1776 3488 1888 3488
            BEGIN DISPLAY 1776 3488 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1776 3552 1888 3552
            BEGIN DISPLAY 1776 3552 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_346 1888 3456 R0
        BEGIN BRANCH X
            WIRE 1776 3328 1888 3328
            BEGIN DISPLAY 1776 3328 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1776 3392 1888 3392
            BEGIN DISPLAY 1776 3392 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_949
            WIRE 2144 3360 2320 3360
            WIRE 2320 3200 2320 3360
            WIRE 2320 3200 2512 3200
        END BRANCH
        BEGIN BRANCH XLXN_950
            WIRE 2144 3520 2512 3520
            WIRE 2512 3264 2512 3520
        END BRANCH
        BEGIN BRANCH XLXN_951
            WIRE 3344 4144 3472 4144
            WIRE 3472 4144 3488 4144
        END BRANCH
        INSTANCE XLXI_355 3488 4176 R0
        BEGIN BRANCH Q(4)
            WIRE 3712 4144 3856 4144
            WIRE 3856 4144 4000 4144
            BEGIN DISPLAY 3856 4144 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH RST
            WIRE 2688 4368 2832 4368
            WIRE 2832 4368 2960 4368
            BEGIN DISPLAY 2688 4368 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 2832 4368 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH C
            WIRE 2832 4272 2848 4272
            WIRE 2848 4272 2960 4272
            BEGIN DISPLAY 2832 4272 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_955
            WIRE 2768 4144 2960 4144
        END BRANCH
        BEGIN INSTANCE XLXI_356 2960 4400 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        INSTANCE XLXI_357 2512 4304 R0
        BEGIN BRANCH Q(2)
            WIRE 1792 3696 1888 3696
            BEGIN DISPLAY 1792 3696 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1792 3760 1888 3760
            BEGIN DISPLAY 1792 3760 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1792 3824 1888 3824
            BEGIN DISPLAY 1792 3824 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1792 3888 1888 3888
            BEGIN DISPLAY 1792 3888 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1776 4032 1888 4032
            BEGIN DISPLAY 1776 4032 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 1776 4096 1888 4096
            BEGIN DISPLAY 1776 4096 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_964
            WIRE 2144 3792 2512 3792
            WIRE 2512 3792 2512 4048
        END BRANCH
        BEGIN BRANCH XLXN_965
            WIRE 2144 4064 2320 4064
            WIRE 2320 4064 2320 4112
            WIRE 2320 4112 2512 4112
        END BRANCH
        INSTANCE XLXI_362 1888 3952 R0
        INSTANCE XLXI_363 1888 4160 R0
        INSTANCE XLXI_364 1888 4368 R0
        BEGIN BRANCH X
            WIRE 1792 4176 1888 4176
            BEGIN DISPLAY 1792 4176 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1792 4240 1888 4240
            BEGIN DISPLAY 1792 4240 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1792 4304 1888 4304
            BEGIN DISPLAY 1792 4304 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_365 1888 4592 R0
        BEGIN BRANCH X
            WIRE 1792 4400 1888 4400
            BEGIN DISPLAY 1792 4400 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 1792 4464 1888 4464
            BEGIN DISPLAY 1792 4464 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1792 4528 1888 4528
            BEGIN DISPLAY 1792 4528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_991
            WIRE 2144 4240 2320 4240
            WIRE 2320 4176 2320 4240
            WIRE 2320 4176 2512 4176
        END BRANCH
        BEGIN BRANCH XLXN_992
            WIRE 2144 4464 2512 4464
            WIRE 2512 4240 2512 4464
        END BRANCH
        INSTANCE XLXI_373 5184 2704 R0
        INSTANCE XLXI_378 4544 1760 R0
        BEGIN BRANCH Q(4)
            WIRE 4448 1504 4544 1504
            BEGIN DISPLAY 4448 1504 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 4448 1568 4544 1568
            BEGIN DISPLAY 4448 1568 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 4448 1632 4544 1632
            BEGIN DISPLAY 4448 1632 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 4448 1696 4544 1696
            BEGIN DISPLAY 4448 1696 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_379 4544 2064 R0
        BEGIN BRANCH Q(3)
            WIRE 4448 1808 4544 1808
            BEGIN DISPLAY 4448 1808 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 4448 1872 4544 1872
            BEGIN DISPLAY 4448 1872 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 4448 1936 4544 1936
            BEGIN DISPLAY 4448 1936 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 4448 2000 4544 2000
            BEGIN DISPLAY 4448 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_380 4544 2352 R0
        BEGIN BRANCH Q(3)
            WIRE 4448 2096 4544 2096
            BEGIN DISPLAY 4448 2096 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 4448 2160 4544 2160
            BEGIN DISPLAY 4448 2160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 4448 2224 4544 2224
            BEGIN DISPLAY 4448 2224 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 4448 2288 4544 2288
            BEGIN DISPLAY 4448 2288 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 4512 2384 4608 2384
            WIRE 4608 2384 5184 2384
            BEGIN DISPLAY 4512 2384 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_384 4560 2752 R0
        BEGIN BRANCH X
            WIRE 4464 2496 4560 2496
            BEGIN DISPLAY 4464 2496 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 4464 2560 4560 2560
            BEGIN DISPLAY 4464 2560 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 4464 2624 4560 2624
            BEGIN DISPLAY 4464 2624 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 4464 2688 4560 2688
            BEGIN DISPLAY 4464 2688 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_385 4560 3056 R0
        BEGIN BRANCH X
            WIRE 4464 2800 4560 2800
            BEGIN DISPLAY 4464 2800 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 4464 2864 4560 2864
            BEGIN DISPLAY 4464 2864 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 4464 2928 4560 2928
            BEGIN DISPLAY 4464 2928 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 4464 2992 4560 2992
            BEGIN DISPLAY 4464 2992 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_386 4560 3344 R0
        BEGIN BRANCH X
            WIRE 4448 3088 4560 3088
            BEGIN DISPLAY 4448 3088 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 4448 3152 4560 3152
            BEGIN DISPLAY 4448 3152 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 4448 3216 4560 3216
            BEGIN DISPLAY 4448 3216 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 4448 3280 4560 3280
            BEGIN DISPLAY 4448 3280 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_387 4560 3616 R0
        BEGIN BRANCH X
            WIRE 4464 3360 4560 3360
            BEGIN DISPLAY 4464 3360 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 4464 3424 4560 3424
            BEGIN DISPLAY 4464 3424 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 4464 3488 4560 3488
            BEGIN DISPLAY 4464 3488 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 4464 3552 4560 3552
            BEGIN DISPLAY 4464 3552 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_1049
            WIRE 4800 1600 5184 1600
            WIRE 5184 1600 5184 2192
        END BRANCH
        BEGIN BRANCH XLXN_1050
            WIRE 4800 1904 4992 1904
            WIRE 4992 1904 4992 2256
            WIRE 4992 2256 5184 2256
        END BRANCH
        BEGIN BRANCH XLXN_1051
            WIRE 4800 2192 4976 2192
            WIRE 4976 2192 4976 2320
            WIRE 4976 2320 5184 2320
        END BRANCH
        BEGIN BRANCH XLXN_1052
            WIRE 4816 2592 4992 2592
            WIRE 4992 2448 4992 2592
            WIRE 4992 2448 5184 2448
        END BRANCH
        BEGIN BRANCH XLXN_1053
            WIRE 4816 2896 5008 2896
            WIRE 5008 2512 5008 2896
            WIRE 5008 2512 5184 2512
        END BRANCH
        BEGIN BRANCH XLXN_1054
            WIRE 4816 3184 5024 3184
            WIRE 5024 2576 5024 3184
            WIRE 5024 2576 5184 2576
        END BRANCH
        BEGIN BRANCH XLXN_1055
            WIRE 4816 3456 5184 3456
            WIRE 5184 2640 5184 3456
        END BRANCH
    END SHEET
END SCHEMATIC
