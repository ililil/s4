VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL X1
        SIGNAL IN_X1
        SIGNAL X2
        SIGNAL IN_X2
        SIGNAL X3
        SIGNAL IN_X3
        SIGNAL S(0:3)
        SIGNAL X0
        SIGNAL IN_X0
        SIGNAL S(0)
        SIGNAL XLXN_548
        SIGNAL XLXN_561
        SIGNAL C
        SIGNAL XLXN_360
        SIGNAL RST
        SIGNAL XLXN_18
        SIGNAL XLXN_363
        SIGNAL S(1)
        SIGNAL XLXN_143
        SIGNAL XLXN_386
        SIGNAL S(2)
        SIGNAL Y0
        SIGNAL S(3)
        SIGNAL X4
        SIGNAL XLXN_934
        SIGNAL XLXN_935
        SIGNAL IN_RST
        SIGNAL IN_C
        SIGNAL IN_X4
        SIGNAL XLXN_957
        SIGNAL XLXN_958
        SIGNAL XLXN_960
        SIGNAL XLXN_961
        SIGNAL XLXN_975
        SIGNAL XLXN_976
        SIGNAL XLXN_977
        SIGNAL XLXN_523
        SIGNAL XLXN_73
        SIGNAL XLXN_985
        SIGNAL XLXN_799
        SIGNAL Y1
        SIGNAL Y2
        SIGNAL XLXN_1082
        SIGNAL Y3
        PORT Input IN_X1
        PORT Input IN_X2
        PORT Input IN_X3
        PORT Input IN_X0
        PORT Output Y0
        PORT Input IN_RST
        PORT Input IN_C
        PORT Input IN_X4
        PORT Output Y1
        PORT Output Y2
        PORT Output Y3
        BEGIN BLOCKDEF ibuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF or3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 72 -128 
            LINE N 0 -192 48 -192 
            LINE N 256 -128 192 -128 
            ARC N 28 -256 204 -80 112 -80 192 -128 
            ARC N -40 -184 72 -72 48 -80 48 -176 
            LINE N 48 -64 48 -80 
            LINE N 48 -192 48 -176 
            LINE N 112 -80 48 -80 
            ARC N 28 -176 204 0 192 -128 112 -176 
            LINE N 112 -176 48 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF buf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
            LINE N 64 0 128 -32 
            LINE N 128 -32 64 -64 
            LINE N 64 -64 64 0 
        END BLOCKDEF
        BEGIN BLOCKDEF fdc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -128 64 -128 
            LINE N 0 -32 64 -32 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            RECTANGLE N 64 -320 320 -64 
            LINE N 64 -112 80 -128 
            LINE N 80 -128 64 -144 
            LINE N 192 -64 192 -32 
            LINE N 192 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF ibufg
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF or4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 48 -256 
            LINE N 256 -160 192 -160 
            ARC N 28 -208 204 -32 192 -160 112 -208 
            LINE N 112 -208 48 -208 
            LINE N 112 -112 48 -112 
            LINE N 48 -256 48 -208 
            LINE N 48 -64 48 -112 
            ARC N -40 -216 72 -104 48 -112 48 -208 
            ARC N 28 -288 204 -112 112 -112 192 -160 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCK XLXI_149 ibuf
            PIN I IN_X1
            PIN O X1
        END BLOCK
        BEGIN BLOCK XLXI_220 ibuf
            PIN I IN_X2
            PIN O X2
        END BLOCK
        BEGIN BLOCK XLXI_222 ibuf
            PIN I IN_X3
            PIN O X3
        END BLOCK
        BEGIN BLOCK XLXI_328 ibuf
            PIN I IN_X0
            PIN O X0
        END BLOCK
        BEGIN BLOCK XLXI_260 or3
            PIN I0 XLXN_935
            PIN I1 XLXN_934
            PIN I2 XLXN_548
            PIN O XLXN_561
        END BLOCK
        BEGIN BLOCK XLXI_153 buf
            PIN I XLXN_360
            PIN O S(0)
        END BLOCK
        BEGIN BLOCK FF1 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_561
            PIN Q XLXN_360
        END BLOCK
        BEGIN BLOCK XLXI_152 buf
            PIN I XLXN_363
            PIN O S(1)
        END BLOCK
        BEGIN BLOCK FF2 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_18
            PIN Q XLXN_363
        END BLOCK
        BEGIN BLOCK XLXI_151 buf
            PIN I XLXN_386
            PIN O S(2)
        END BLOCK
        BEGIN BLOCK FF3 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_143
            PIN Q XLXN_386
        END BLOCK
        BEGIN BLOCK XLXI_329 and2b1
            PIN I0 X1
            PIN I1 S(0)
            PIN O XLXN_548
        END BLOCK
        BEGIN BLOCK XLXI_183 obuf
            PIN I S(0)
            PIN O Y0
        END BLOCK
        BEGIN BLOCK XLXI_330 and2
            PIN I0 X4
            PIN I1 S(3)
            PIN O XLXN_935
        END BLOCK
        BEGIN BLOCK XLXI_449 and3b1
            PIN I0 X2
            PIN I1 X3
            PIN I2 S(2)
            PIN O XLXN_934
        END BLOCK
        BEGIN BLOCK XLXI_218 ibuf
            PIN I IN_RST
            PIN O RST
        END BLOCK
        BEGIN BLOCK XLXI_150 ibufg
            PIN I IN_C
            PIN O C
        END BLOCK
        BEGIN BLOCK XLXI_452 ibuf
            PIN I IN_X4
            PIN O X4
        END BLOCK
        BEGIN BLOCK XLXI_251 and2b1
            PIN I0 X2
            PIN I1 S(1)
            PIN O XLXN_957
        END BLOCK
        BEGIN BLOCK XLXI_250 and2
            PIN I0 S(0)
            PIN I1 X1
            PIN O XLXN_958
        END BLOCK
        BEGIN BLOCK XLXI_254 and3b1
            PIN I0 X2
            PIN I1 S(2)
            PIN I2 X3
            PIN O XLXN_960
        END BLOCK
        BEGIN BLOCK XLXI_458 and2
            PIN I0 X1
            PIN I1 S(1)
            PIN O XLXN_961
        END BLOCK
        BEGIN BLOCK XLXI_459 or4
            PIN I0 XLXN_961
            PIN I1 XLXN_960
            PIN I2 XLXN_958
            PIN I3 XLXN_957
            PIN O XLXN_18
        END BLOCK
        BEGIN BLOCK XLXI_460 or3
            PIN I0 XLXN_977
            PIN I1 XLXN_976
            PIN I2 XLXN_975
            PIN O XLXN_143
        END BLOCK
        BEGIN BLOCK XLXI_343 and3b1
            PIN I0 X1
            PIN I1 X2
            PIN I2 S(1)
            PIN O XLXN_975
        END BLOCK
        BEGIN BLOCK XLXI_463 and2b1
            PIN I0 X3
            PIN I1 S(2)
            PIN O XLXN_976
        END BLOCK
        BEGIN BLOCK XLXI_255 and2
            PIN I0 S(2)
            PIN I1 X2
            PIN O XLXN_977
        END BLOCK
        BEGIN BLOCK XLXI_247 buf
            PIN I XLXN_523
            PIN O S(3)
        END BLOCK
        BEGIN BLOCK FF4 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_73
            PIN Q XLXN_523
        END BLOCK
        BEGIN BLOCK XLXI_467 or2
            PIN I0 XLXN_985
            PIN I1 X0
            PIN O XLXN_73
        END BLOCK
        BEGIN BLOCK XLXI_468 and2b1
            PIN I0 X4
            PIN I1 S(3)
            PIN O XLXN_985
        END BLOCK
        BEGIN BLOCK XLXI_325 obuf
            PIN I XLXN_799
            PIN O Y1
        END BLOCK
        BEGIN BLOCK XLXI_515 or2
            PIN I0 S(3)
            PIN I1 S(1)
            PIN O XLXN_799
        END BLOCK
        BEGIN BLOCK XLXI_326 obuf
            PIN I S(1)
            PIN O Y2
        END BLOCK
        BEGIN BLOCK XLXI_503 obuf
            PIN I XLXN_1082
            PIN O Y3
        END BLOCK
        BEGIN BLOCK XLXI_523 or2
            PIN I0 S(3)
            PIN I1 S(2)
            PIN O XLXN_1082
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 7040 5440
        BEGIN BRANCH X1
            WIRE 640 720 784 720
            WIRE 784 720 928 720
            BEGIN DISPLAY 784 720 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_149 416 752 R0
        BEGIN BRANCH IN_X1
            WIRE 336 720 400 720
            WIRE 400 720 416 720
            BEGIN DISPLAY 400 720 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 640 896 784 896
            WIRE 784 896 928 896
            BEGIN DISPLAY 784 896 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_220 416 928 R0
        BEGIN BRANCH IN_X2
            WIRE 336 896 400 896
            WIRE 400 896 416 896
            BEGIN DISPLAY 400 896 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X3
            WIRE 640 1072 784 1072
            WIRE 784 1072 928 1072
            BEGIN DISPLAY 784 1072 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_222 416 1104 R0
        BEGIN BRANCH IN_X3
            WIRE 336 1072 400 1072
            WIRE 400 1072 416 1072
            BEGIN DISPLAY 400 1072 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0:3)
            WIRE 256 368 416 368
            WIRE 416 368 736 368
            BEGIN DISPLAY 416 368 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X0
            WIRE 640 576 736 576
            WIRE 736 576 928 576
            BEGIN DISPLAY 736 576 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_328 416 608 R0
        BEGIN BRANCH IN_X0
            WIRE 336 576 400 576
            WIRE 400 576 416 576
            BEGIN DISPLAY 400 576 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 2576 176 2672 176
            BEGIN DISPLAY 2576 176 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X1
            WIRE 2576 240 2672 240
            BEGIN DISPLAY 2576 240 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_260 3136 528 R0
        BEGIN BRANCH XLXN_548
            WIRE 2928 208 3136 208
            WIRE 3136 208 3136 336
        END BRANCH
        BEGIN BRANCH XLXN_561
            WIRE 3392 400 3776 400
        END BRANCH
        BEGIN BRANCH C
            WIRE 3648 528 3776 528
            BEGIN DISPLAY 3648 528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_360
            WIRE 4160 400 4304 400
        END BRANCH
        INSTANCE XLXI_153 4304 432 R0
        BEGIN BRANCH S(0)
            WIRE 4528 400 4672 400
            WIRE 4672 400 4736 400
            BEGIN DISPLAY 4672 400 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE FF1 3776 656 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 3504 624 3776 624
            BEGIN DISPLAY 3504 624 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_18
            WIRE 3376 1024 3776 1024
        END BRANCH
        BEGIN BRANCH C
            WIRE 3648 1152 3776 1152
            BEGIN DISPLAY 3648 1152 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_152 4304 1056 R0
        BEGIN BRANCH XLXN_363
            WIRE 4160 1024 4304 1024
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 4528 1024 4624 1024
            WIRE 4624 1024 4736 1024
            BEGIN DISPLAY 4624 1024 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE FF2 3776 1280 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 3536 1248 3776 1248
            BEGIN DISPLAY 3536 1248 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_143
            WIRE 3408 1584 3776 1584
        END BRANCH
        BEGIN BRANCH C
            WIRE 3632 1712 3776 1712
            BEGIN DISPLAY 3632 1712 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_151 4256 1616 R0
        BEGIN BRANCH XLXN_386
            WIRE 4160 1584 4256 1584
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 4480 1584 4576 1584
            WIRE 4576 1584 4736 1584
            BEGIN DISPLAY 4576 1584 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE FF3 3776 1840 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 3552 1808 3776 1808
            BEGIN DISPLAY 3552 1808 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_329 2672 304 R0
        INSTANCE XLXI_183 1296 1728 R0
        BEGIN BRANCH S(0)
            WIRE 992 1696 1120 1696
            WIRE 1120 1696 1296 1696
            BEGIN DISPLAY 992 1696 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Y0
            WIRE 1520 1696 1552 1696
        END BRANCH
        BEGIN BRANCH X3
            WIRE 2576 400 2672 400
            BEGIN DISPLAY 2576 400 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 2576 464 2672 464
            BEGIN DISPLAY 2576 464 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 2576 336 2672 336
            BEGIN DISPLAY 2576 336 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_330 2672 672 R0
        BEGIN BRANCH S(3)
            WIRE 2576 544 2672 544
            BEGIN DISPLAY 2576 544 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X4
            WIRE 2576 608 2672 608
            BEGIN DISPLAY 2576 608 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_934
            WIRE 2928 400 3136 400
        END BRANCH
        BEGIN BRANCH XLXN_935
            WIRE 2928 576 3136 576
            WIRE 3136 464 3136 576
        END BRANCH
        INSTANCE XLXI_449 2672 528 R0
        INSTANCE XLXI_218 416 1296 R0
        BEGIN BRANCH IN_RST
            WIRE 352 1264 416 1264
        END BRANCH
        BEGIN BRANCH RST
            WIRE 640 1264 784 1264
            WIRE 784 1264 928 1264
            BEGIN DISPLAY 784 1264 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_150 416 1424 R0
        BEGIN BRANCH C
            WIRE 640 1392 864 1392
            WIRE 864 1392 864 1392
            WIRE 864 1392 928 1392
            BEGIN DISPLAY 868 1392 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH IN_C
            WIRE 320 1392 416 1392
        END BRANCH
        BEGIN BRANCH X4
            WIRE 640 1168 784 1168
            WIRE 784 1168 928 1168
            BEGIN DISPLAY 784 1168 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_452 416 1200 R0
        BEGIN BRANCH IN_X4
            WIRE 336 1168 400 1168
            WIRE 400 1168 416 1168
            BEGIN DISPLAY 400 1168 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_251 2672 864 R0
        BEGIN BRANCH S(1)
            WIRE 2576 736 2672 736
            BEGIN DISPLAY 2576 736 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 2576 800 2672 800
            BEGIN DISPLAY 2576 800 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_250 2672 1024 R0
        BEGIN BRANCH X1
            WIRE 2576 896 2672 896
            BEGIN DISPLAY 2576 896 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 2576 960 2672 960
            BEGIN DISPLAY 2576 960 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 2576 1104 2672 1104
            BEGIN DISPLAY 2576 1104 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 2576 1168 2672 1168
            BEGIN DISPLAY 2576 1168 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X3
            WIRE 2576 1040 2672 1040
            BEGIN DISPLAY 2576 1040 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_254 2672 1232 R0
        INSTANCE XLXI_458 2672 1376 R0
        BEGIN BRANCH S(1)
            WIRE 2576 1248 2672 1248
            BEGIN DISPLAY 2576 1248 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X1
            WIRE 2576 1312 2672 1312
            BEGIN DISPLAY 2576 1312 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_459 3120 1184 R0
        BEGIN BRANCH XLXN_957
            WIRE 2928 768 3120 768
            WIRE 3120 768 3120 928
        END BRANCH
        BEGIN BRANCH XLXN_958
            WIRE 2928 928 3024 928
            WIRE 3024 928 3024 992
            WIRE 3024 992 3120 992
        END BRANCH
        BEGIN BRANCH XLXN_960
            WIRE 2928 1104 3024 1104
            WIRE 3024 1056 3024 1104
            WIRE 3024 1056 3120 1056
        END BRANCH
        BEGIN BRANCH XLXN_961
            WIRE 2928 1280 3120 1280
            WIRE 3120 1120 3120 1280
        END BRANCH
        INSTANCE XLXI_460 3152 1712 R0
        BEGIN BRANCH X2
            WIRE 2544 1488 2640 1488
            BEGIN DISPLAY 2544 1488 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X1
            WIRE 2544 1552 2640 1552
            BEGIN DISPLAY 2544 1552 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 2544 1424 2640 1424
            BEGIN DISPLAY 2544 1424 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_343 2640 1616 R0
        INSTANCE XLXI_463 2640 1776 R0
        INSTANCE XLXI_255 2640 1936 R0
        BEGIN BRANCH X2
            WIRE 2544 1808 2640 1808
            BEGIN DISPLAY 2544 1808 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 2544 1872 2640 1872
            BEGIN DISPLAY 2544 1872 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 2544 1648 2640 1648
            BEGIN DISPLAY 2544 1648 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X3
            WIRE 2544 1712 2640 1712
            BEGIN DISPLAY 2544 1712 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_975
            WIRE 2896 1488 3152 1488
            WIRE 3152 1488 3152 1520
        END BRANCH
        BEGIN BRANCH XLXN_976
            WIRE 2896 1680 3024 1680
            WIRE 3024 1584 3024 1680
            WIRE 3024 1584 3152 1584
        END BRANCH
        BEGIN BRANCH XLXN_977
            WIRE 2896 1840 3152 1840
            WIRE 3152 1648 3152 1840
        END BRANCH
        BEGIN BRANCH C
            WIRE 3632 2256 3776 2256
            BEGIN DISPLAY 3632 2256 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_247 4256 2160 R0
        BEGIN BRANCH XLXN_523
            WIRE 4160 2128 4256 2128
        END BRANCH
        BEGIN BRANCH S(3)
            WIRE 4480 2128 4576 2128
            WIRE 4576 2128 4736 2128
            BEGIN DISPLAY 4576 2128 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE FF4 3776 2384 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 3552 2352 3776 2352
            BEGIN DISPLAY 3552 2352 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_73
            WIRE 3328 2128 3776 2128
        END BRANCH
        INSTANCE XLXI_467 3072 2224 R0
        BEGIN BRANCH X0
            WIRE 2976 2096 3072 2096
            BEGIN DISPLAY 2976 2096 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_468 2736 2288 R0
        BEGIN BRANCH XLXN_985
            WIRE 2992 2192 3024 2192
            WIRE 3024 2160 3024 2192
            WIRE 3024 2160 3072 2160
        END BRANCH
        BEGIN BRANCH S(3)
            WIRE 2640 2160 2736 2160
            BEGIN DISPLAY 2640 2160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X4
            WIRE 2640 2224 2736 2224
            BEGIN DISPLAY 2640 2224 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        IOMARKER 336 720 IN_X1 R180 28
        IOMARKER 336 896 IN_X2 R180 28
        IOMARKER 336 1072 IN_X3 R180 28
        IOMARKER 336 576 IN_X0 R180 28
        IOMARKER 1552 1696 Y0 R0 28
        IOMARKER 352 1264 IN_RST R180 28
        IOMARKER 320 1392 IN_C R180 28
        IOMARKER 336 1168 IN_X4 R180 28
        INSTANCE XLXI_325 1296 1856 R0
        BEGIN BRANCH XLXN_799
            WIRE 992 1824 1008 1824
            WIRE 1008 1824 1024 1824
            WIRE 1024 1824 1296 1824
        END BRANCH
        BEGIN BRANCH Y1
            WIRE 1520 1824 1552 1824
        END BRANCH
        INSTANCE XLXI_515 736 1920 R0
        BEGIN BRANCH S(1)
            WIRE 640 1792 736 1792
            BEGIN DISPLAY 640 1792 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(3)
            WIRE 640 1856 736 1856
            BEGIN DISPLAY 640 1856 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        IOMARKER 1552 1824 Y1 R0 28
        INSTANCE XLXI_326 1296 2032 R0
        BEGIN BRANCH Y2
            WIRE 1520 2000 1552 2000
        END BRANCH
        IOMARKER 1552 2000 Y2 R0 28
        BEGIN BRANCH S(1)
            WIRE 816 2000 944 2000
            WIRE 944 2000 1120 2000
            WIRE 1120 2000 1296 2000
            BEGIN DISPLAY 816 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_503 1296 2192 R0
        BEGIN BRANCH XLXN_1082
            WIRE 1120 2160 1296 2160
        END BRANCH
        BEGIN BRANCH Y3
            WIRE 1520 2160 1552 2160
        END BRANCH
        INSTANCE XLXI_523 864 2256 R0
        BEGIN BRANCH S(2)
            WIRE 768 2128 864 2128
            BEGIN DISPLAY 768 2128 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(3)
            WIRE 768 2192 864 2192
            BEGIN DISPLAY 768 2192 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        IOMARKER 1552 2160 Y3 R0 28
    END SHEET
END SCHEMATIC
