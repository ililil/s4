#ifndef MAIN_3D
#define MAIN_3D
#include "3d.h"

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow);

DoubleMatrix gen_coordinates();

#endif
