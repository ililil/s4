#define UNICODE
#define _USE_MATH_DEFINES
#define WIN32_LEAN_AND_MEAN // Exclude rarely-used stuff from Windows headers

#include <SDKDDKVer.h>
#include <windows.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>

#define szTitle L"3d"
#define szWindowClass L"3d"

#define MAX_ITER 256

#define mx0 -2
#define mx1 1
#define my0 -1
#define my1 1

double scale = 1;
double delta = 0;

int windowWidth = 800, windowHeight = 800;

HINSTANCE hInst = NULL;

BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	if (!InitInstance(hInstance, nCmdShow))
		return 1;

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int) msg.wParam;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
	WNDCLASSEXW wcex = {0};

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, IDI_WINLOGO);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	wcex.lpszMenuName = L"Menu";
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(hInstance, IDI_WINLOGO);

	RegisterClassEx(&wcex);
	hInst = hInstance;

	HWND hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, windowWidth, windowHeight, NULL, NULL, hInstance, NULL);

	if (!hWnd)
		return FALSE;

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

COLORREF HSV(float H, float S, float V) {
	float r, g, b;

	float h = H / 360;
	float s = S / 100;
	float v = V / 100;

	int i = (int) floor(h * 6);
	float f = h * 6 - i;
	float p = v * (1 - s);
	float q = v * (1 - f * s);
	float t = v * (1 - (1 - f) * s);

	switch (i % 6) {
		case 0:
			r = v, g = t, b = p;
			break;
		case 1:
			r = q, g = v, b = p;
			break;
		case 2:
			r = p, g = v, b = t;
			break;
		case 3:
			r = p, g = q, b = v;
			break;
		case 4:
			r = t, g = p, b = v;
			break;
		case 5:
			r = v, g = p, b = q;
			break;
		default:
			break;
	}

	return RGB(r * 255, g * 255, b * 255);
}

double mandle(_Complex double c) {
	_Complex double z = 0;
	int n = 0;

	while (cabs(z) <= 2 && n < MAX_ITER) {
		z = z * z + c;
		n += 1;
	}
	return n + 1; // - log(log2(abs(z)));
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		case WM_COMMAND: {
			int wmId = LOWORD(wParam);
			switch (wmId) {
				default: {
					return DefWindowProc(hWnd, message, wParam, lParam);
				}
			}
			break;
		}

		case WM_PAINT: {
			RECT rect;
			GetWindowRect(hWnd, &rect);
			windowWidth = rect.right - rect.left;
			windowHeight = rect.bottom - rect.top;

			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);

			while (1) {
				for (double x = 0; x < windowWidth; x++) {
					for (double y = 0; y < windowHeight; y++) {
						_Complex double complicated = (delta + mx0 + (x / windowWidth) * (mx1 - mx0)) * scale + //
								(delta / 2 + my0 + (y / windowHeight) * (my1 - my0)) * scale * I;

						int m = mandle(complicated);
						SetPixelV(hdc, (int) x, (int) y, HSV(255 * m / MAX_ITER, 255, m < MAX_ITER ? 100 : 0));
					}
					BitBlt(hdc, 0, 0, windowWidth, windowHeight, hdc, 0, 0, SRCCOPY);
				}
				scale -= 0.15;
				delta -= 0.6;
				break;
			}
			EndPaint(hWnd, &ps);
			break;
		}

		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}

		default: {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	return 0;
}
