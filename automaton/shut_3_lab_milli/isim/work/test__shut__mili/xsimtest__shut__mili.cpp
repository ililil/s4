static const char * HSimCopyRightNotice = "Copyright 2004-2005, Xilinx Inc. All rights reserved.";
#ifdef __MINGW32__
#include "xsimMinGW.h"
#else
#include "xsim.h"
#endif


static HSim__s6* IF0(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createworkMtest__shut__mili(const char*);
    HSim__s6 *blk = createworkMtest__shut__mili(label); 
    return blk;
}


static HSim__s6* IF1(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_f_m_a_p(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_f_m_a_p(label); 
    return blk;
}


static HSim__s6* IF2(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_o_r2(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_o_r2(label); 
    return blk;
}


static HSim__s6* IF3(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_o_r4(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_o_r4(label); 
    return blk;
}


static HSim__s6* IF4(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d2(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d2(label); 
    return blk;
}


static HSim__s6* IF5(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d2_b1(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d2_b1(label); 
    return blk;
}


static HSim__s6* IF6(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d3_b1(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d3_b1(label); 
    return blk;
}


static HSim__s6* IF7(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d3_b2(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d3_b2(label); 
    return blk;
}


static HSim__s6* IF8(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d4_b1(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d4_b1(label); 
    return blk;
}


static HSim__s6* IF9(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d4_b2(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d4_b2(label); 
    return blk;
}


static HSim__s6* IF10(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d4_b3(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d4_b3(label); 
    return blk;
}


static HSim__s6* IF11(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_b_u_f(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_b_u_f(label); 
    return blk;
}


static HSim__s6* IF12(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_f_d_c(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_f_d_c(label); 
    return blk;
}


static HSim__s6* IF13(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_i_b_u_f(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_i_b_u_f(label); 
    return blk;
}


static HSim__s6* IF14(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_i_b_u_f_g(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_i_b_u_f_g(label); 
    return blk;
}


static HSim__s6* IF15(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_o_b_u_f(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_o_b_u_f(label); 
    return blk;
}


static HSim__s6* IF16(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_o_r5(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_o_r5(label); 
    return blk;
}


static HSim__s6* IF17(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_o_r4(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_o_r4(label); 
    return blk;
}


static HSim__s6* IF18(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_o_r2(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_o_r2(label); 
    return blk;
}


static HSim__s6* IF19(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_f_m_a_p(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_f_m_a_p(label); 
    return blk;
}


static HSim__s6* IF20(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createworkM_o_r8___m_x_i_l_i_n_x__shut__3__mili(const char*);
    HSim__s6 *blk = createworkM_o_r8___m_x_i_l_i_n_x__shut__3__mili(label); 
    return blk;
}


static HSim__s6* IF21(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_o_r5(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_o_r5(label); 
    return blk;
}


static HSim__s6* IF22(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_o_r4(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_o_r4(label); 
    return blk;
}


static HSim__s6* IF23(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_o_r2(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_o_r2(label); 
    return blk;
}


static HSim__s6* IF24(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_o_b_u_f(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_o_b_u_f(label); 
    return blk;
}


static HSim__s6* IF25(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_i_b_u_f_g(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_i_b_u_f_g(label); 
    return blk;
}


static HSim__s6* IF26(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_i_b_u_f(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_i_b_u_f(label); 
    return blk;
}


static HSim__s6* IF27(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_f_d_c(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_f_d_c(label); 
    return blk;
}


static HSim__s6* IF28(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_b_u_f(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_b_u_f(label); 
    return blk;
}


static HSim__s6* IF29(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d4_b3(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d4_b3(label); 
    return blk;
}


static HSim__s6* IF30(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d4_b2(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d4_b2(label); 
    return blk;
}


static HSim__s6* IF31(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d4_b1(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d4_b1(label); 
    return blk;
}


static HSim__s6* IF32(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d3_b2(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d3_b2(label); 
    return blk;
}


static HSim__s6* IF33(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d3_b1(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d3_b1(label); 
    return blk;
}


static HSim__s6* IF34(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d2_b1(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d2_b1(label); 
    return blk;
}


static HSim__s6* IF35(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createunisim_ver_auxlibM_a_n_d2(const char*);
    HSim__s6 *blk = createunisim_ver_auxlibM_a_n_d2(label); 
    return blk;
}


static HSim__s6* IF36(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createworkMshut__3__mili(const char*);
    HSim__s6 *blk = createworkMshut__3__mili(label); 
    return blk;
}


static HSim__s6* IF37(HSim__s6 *Arch,const char* label,int nGenerics, 
va_list vap)
{
    extern HSim__s6 * createworkMglbl(const char*);
    HSim__s6 *blk = createworkMglbl(label); 
    return blk;
}

class _top : public HSim__s6 {
public:
    _top() : HSim__s6(false, "_top", "_top", 0, 0, HSim::VerilogModule) {}
    HSimConfigDecl * topModuleInstantiate() {
        HSimConfigDecl * cfgvh = 0;
        cfgvh = new HSimConfigDecl("default");
        (*cfgvh).addVlogModule("test_shut_mili", (HSimInstFactoryPtr)IF0);
        (*cfgvh).addVlogModule("FMAP", (HSimInstFactoryPtr)IF1);
        (*cfgvh).addVlogModule("OR2", (HSimInstFactoryPtr)IF2);
        (*cfgvh).addVlogModule("OR4", (HSimInstFactoryPtr)IF3);
        (*cfgvh).addVlogModule("AND2", (HSimInstFactoryPtr)IF4);
        (*cfgvh).addVlogModule("AND2B1", (HSimInstFactoryPtr)IF5);
        (*cfgvh).addVlogModule("AND3B1", (HSimInstFactoryPtr)IF6);
        (*cfgvh).addVlogModule("AND3B2", (HSimInstFactoryPtr)IF7);
        (*cfgvh).addVlogModule("AND4B1", (HSimInstFactoryPtr)IF8);
        (*cfgvh).addVlogModule("AND4B2", (HSimInstFactoryPtr)IF9);
        (*cfgvh).addVlogModule("AND4B3", (HSimInstFactoryPtr)IF10);
        (*cfgvh).addVlogModule("BUF", (HSimInstFactoryPtr)IF11);
        (*cfgvh).addVlogModule("FDC", (HSimInstFactoryPtr)IF12);
        (*cfgvh).addVlogModule("IBUF", (HSimInstFactoryPtr)IF13);
        (*cfgvh).addVlogModule("IBUFG", (HSimInstFactoryPtr)IF14);
        (*cfgvh).addVlogModule("OBUF", (HSimInstFactoryPtr)IF15);
        (*cfgvh).addVlogModule("OR5", (HSimInstFactoryPtr)IF16);
        (*cfgvh).addVlogModule("OR4", (HSimInstFactoryPtr)IF17);
        (*cfgvh).addVlogModule("OR2", (HSimInstFactoryPtr)IF18);
        (*cfgvh).addVlogModule("FMAP", (HSimInstFactoryPtr)IF19);
        (*cfgvh).addVlogModule("OR8_MXILINX_shut_3_mili", (HSimInstFactoryPtr)IF20);
        (*cfgvh).addVlogModule("OR5", (HSimInstFactoryPtr)IF21);
        (*cfgvh).addVlogModule("OR4", (HSimInstFactoryPtr)IF22);
        (*cfgvh).addVlogModule("OR2", (HSimInstFactoryPtr)IF23);
        (*cfgvh).addVlogModule("OBUF", (HSimInstFactoryPtr)IF24);
        (*cfgvh).addVlogModule("IBUFG", (HSimInstFactoryPtr)IF25);
        (*cfgvh).addVlogModule("IBUF", (HSimInstFactoryPtr)IF26);
        (*cfgvh).addVlogModule("FDC", (HSimInstFactoryPtr)IF27);
        (*cfgvh).addVlogModule("BUF", (HSimInstFactoryPtr)IF28);
        (*cfgvh).addVlogModule("AND4B3", (HSimInstFactoryPtr)IF29);
        (*cfgvh).addVlogModule("AND4B2", (HSimInstFactoryPtr)IF30);
        (*cfgvh).addVlogModule("AND4B1", (HSimInstFactoryPtr)IF31);
        (*cfgvh).addVlogModule("AND3B2", (HSimInstFactoryPtr)IF32);
        (*cfgvh).addVlogModule("AND3B1", (HSimInstFactoryPtr)IF33);
        (*cfgvh).addVlogModule("AND2B1", (HSimInstFactoryPtr)IF34);
        (*cfgvh).addVlogModule("AND2", (HSimInstFactoryPtr)IF35);
        (*cfgvh).addVlogModule("shut_3_mili", (HSimInstFactoryPtr)IF36);
        (*cfgvh).addVlogModule("glbl", (HSimInstFactoryPtr)IF37);
        HSim__s5 * topvl = 0;
        extern HSim__s6 * createworkMtest__shut__mili(const char*);
        topvl = (HSim__s5*)createworkMtest__shut__mili("test_shut_mili");
        topvl->moduleInstantiate(cfgvh);
        addChild(topvl);
        extern HSim__s6 * createworkMglbl(const char*);
        topvl = (HSim__s5*)createworkMglbl("glbl");
        topvl->moduleInstantiate(cfgvh);
        addChild(topvl);
        return cfgvh;
}
};

main(int argc, char **argv) {
  HSimDesign::initDesign();
  globalKernel->getOptions(argc,argv);
  HSim__s6 * _top_i = 0;
  try {
    HSimConfigDecl *cfg;
 _top_i = new _top();
  cfg =  _top_i->topModuleInstantiate();
    return globalKernel->runTcl(cfg, _top_i, "_top", argc, argv);
  }
  catch (HSimError& msg){
    try {
      globalKernel->error(msg.ErrMsg);
      return 1;
    }
    catch(...) {}
      return 1;
  }
  catch (...){
    globalKernel->fatalError();
    return 1;
  }
}
