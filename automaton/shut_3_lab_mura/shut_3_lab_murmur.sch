VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL X
        SIGNAL IN_X
        SIGNAL C
        SIGNAL IN_RST
        SIGNAL RST
        SIGNAL Q(1:4)
        SIGNAL XLXN_12
        SIGNAL XLXN_13
        SIGNAL Q(1)
        SIGNAL NO_Q(1)
        SIGNAL XLXN_22
        SIGNAL XLXN_23
        SIGNAL Q(2)
        SIGNAL NO_Q(2)
        SIGNAL R1
        SIGNAL Q(3)
        SIGNAL Q(4)
        SIGNAL S3
        SIGNAL R3
        SIGNAL XLXN_273
        SIGNAL XLXN_274
        SIGNAL NO_Q(3)
        SIGNAL XLXN_281
        SIGNAL XLXN_282
        SIGNAL NO_Q(4)
        SIGNAL XLXN_192
        SIGNAL XLXN_194
        SIGNAL S4
        SIGNAL XLXN_854
        SIGNAL NO_Q(1:4)
        SIGNAL IN_C
        SIGNAL XLXN_882
        SIGNAL XLXN_883
        SIGNAL R2
        SIGNAL S2
        SIGNAL R4
        SIGNAL Y
        SIGNAL XLXN_1038
        SIGNAL XLXN_1039
        SIGNAL XLXN_1040
        SIGNAL XLXN_1041
        SIGNAL XLXN_1049
        SIGNAL S1
        SIGNAL XLXN_1067
        SIGNAL XLXN_1080
        SIGNAL XLXN_1081
        SIGNAL XLXN_1082
        SIGNAL XLXN_1083
        SIGNAL XLXN_1092
        SIGNAL XLXN_1093
        SIGNAL XLXN_1094
        SIGNAL XLXN_1107
        SIGNAL XLXN_1109
        SIGNAL XLXN_1110
        SIGNAL XLXN_1111
        SIGNAL XLXN_998
        SIGNAL XLXN_1004
        SIGNAL XLXN_1003
        PORT Input IN_X
        PORT Input IN_RST
        PORT Output NO_Q(1)
        PORT Output NO_Q(2)
        PORT Output NO_Q(3)
        PORT Output NO_Q(4)
        PORT Input IN_C
        PORT Output Y
        BEGIN BLOCKDEF ibuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF ibufg
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF Flip_Flop_RS
            TIMESTAMP 2024 5 2 19 51 3
            RECTANGLE N 64 -256 320 0 
            LINE N 64 -224 0 -224 
            LINE N 64 -160 0 -160 
            LINE N 64 -96 0 -96 
            LINE N 64 -32 0 -32 
            LINE N 320 -224 384 -224 
            LINE N 320 -32 384 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF buf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
            LINE N 64 0 128 -32 
            LINE N 128 -32 64 -64 
            LINE N 64 -64 64 0 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 40 -192 
            CIRCLE N 40 -204 64 -180 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -208 144 -208 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF or4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 48 -256 
            LINE N 256 -160 192 -160 
            ARC N 28 -208 204 -32 192 -160 112 -208 
            LINE N 112 -208 48 -208 
            LINE N 112 -112 48 -112 
            LINE N 48 -256 48 -208 
            LINE N 48 -64 48 -112 
            ARC N -40 -216 72 -104 48 -112 48 -208 
            ARC N 28 -288 204 -112 112 -112 192 -160 
        END BLOCKDEF
        BEGIN BLOCKDEF or3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 72 -128 
            LINE N 0 -192 48 -192 
            LINE N 256 -128 192 -128 
            ARC N 28 -256 204 -80 112 -80 192 -128 
            ARC N -40 -184 72 -72 48 -80 48 -176 
            LINE N 48 -64 48 -80 
            LINE N 48 -192 48 -176 
            LINE N 112 -80 48 -80 
            ARC N 28 -176 204 0 192 -128 112 -176 
            LINE N 112 -176 48 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 40 -192 
            CIRCLE N 40 -204 64 -180 
            LINE N 0 -256 40 -256 
            CIRCLE N 40 -268 64 -244 
            LINE N 256 -160 192 -160 
            LINE N 64 -208 144 -208 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 40 -192 
            CIRCLE N 40 -204 64 -180 
            LINE N 256 -128 192 -128 
            LINE N 64 -176 144 -176 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCKDEF and5b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 144 -144 64 -144 
            ARC N 96 -240 192 -144 144 -144 144 -240 
            LINE N 64 -240 144 -240 
            LINE N 64 -64 64 -320 
            LINE N 256 -192 192 -192 
            LINE N 0 -320 64 -320 
            LINE N 0 -256 64 -256 
            LINE N 0 -192 64 -192 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF and5b3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 -320 
            LINE N 64 -240 144 -240 
            ARC N 96 -240 192 -144 144 -144 144 -240 
            LINE N 144 -144 64 -144 
            LINE N 256 -192 192 -192 
            LINE N 0 -320 64 -320 
            LINE N 0 -256 64 -256 
            LINE N 0 -192 40 -192 
            CIRCLE N 40 -204 64 -180 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCK XLXI_149 ibuf
            PIN I IN_X
            PIN O X
        END BLOCK
        BEGIN BLOCK XLXI_218 ibuf
            PIN I IN_RST
            PIN O RST
        END BLOCK
        BEGIN BLOCK XLXI_3 Flip_Flop_RS
            PIN RST RST
            PIN R R1
            PIN S S1
            PIN C C
            PIN Q XLXN_12
            PIN not_Q XLXN_13
        END BLOCK
        BEGIN BLOCK XLXI_152 buf
            PIN I XLXN_12
            PIN O Q(1)
        END BLOCK
        BEGIN BLOCK XLXI_220 buf
            PIN I XLXN_13
            PIN O NO_Q(1)
        END BLOCK
        BEGIN BLOCK XLXI_283 Flip_Flop_RS
            PIN RST RST
            PIN R R2
            PIN S S2
            PIN C C
            PIN Q XLXN_22
            PIN not_Q XLXN_23
        END BLOCK
        BEGIN BLOCK XLXI_284 buf
            PIN I XLXN_22
            PIN O Q(2)
        END BLOCK
        BEGIN BLOCK XLXI_285 buf
            PIN I XLXN_23
            PIN O NO_Q(2)
        END BLOCK
        BEGIN BLOCK XLXI_317 Flip_Flop_RS
            PIN RST RST
            PIN R R3
            PIN S S3
            PIN C C
            PIN Q XLXN_273
            PIN not_Q XLXN_274
        END BLOCK
        BEGIN BLOCK XLXI_318 buf
            PIN I XLXN_273
            PIN O Q(3)
        END BLOCK
        BEGIN BLOCK XLXI_319 buf
            PIN I XLXN_274
            PIN O NO_Q(3)
        END BLOCK
        BEGIN BLOCK XLXI_320 Flip_Flop_RS
            PIN RST RST
            PIN R R4
            PIN S S4
            PIN C C
            PIN Q XLXN_281
            PIN not_Q XLXN_282
        END BLOCK
        BEGIN BLOCK XLXI_321 buf
            PIN I XLXN_281
            PIN O Q(4)
        END BLOCK
        BEGIN BLOCK XLXI_322 buf
            PIN I XLXN_282
            PIN O NO_Q(4)
        END BLOCK
        BEGIN BLOCK XLXI_273 or3
            PIN I0 XLXN_194
            PIN I1 XLXN_1049
            PIN I2 XLXN_192
            PIN O R4
        END BLOCK
        BEGIN BLOCK XLXI_150 ibufg
            PIN I IN_C
            PIN O C
        END BLOCK
        BEGIN BLOCK XLXI_363 and4b4
            PIN I0 Q(4)
            PIN I1 Q(3)
            PIN I2 Q(1)
            PIN I3 X
            PIN O XLXN_882
        END BLOCK
        BEGIN BLOCK XLXI_364 and4b2
            PIN I0 Q(1)
            PIN I1 X
            PIN I2 Q(4)
            PIN I3 Q(3)
            PIN O XLXN_883
        END BLOCK
        BEGIN BLOCK XLXI_377 and4b4
            PIN I0 Q(4)
            PIN I1 Q(2)
            PIN I2 Q(1)
            PIN I3 X
            PIN O XLXN_1093
        END BLOCK
        BEGIN BLOCK XLXI_354 and3b2
            PIN I0 Q(4)
            PIN I1 Q(1)
            PIN I2 Q(3)
            PIN O XLXN_1038
        END BLOCK
        BEGIN BLOCK XLXI_397 and3b2
            PIN I0 Q(2)
            PIN I1 Q(1)
            PIN I2 Q(3)
            PIN O XLXN_1039
        END BLOCK
        BEGIN BLOCK XLXI_399 and3b3
            PIN I0 Q(4)
            PIN I1 Q(2)
            PIN I2 Q(1)
            PIN O XLXN_1040
        END BLOCK
        BEGIN BLOCK XLXI_355 and4b2
            PIN I0 Q(3)
            PIN I1 Q(1)
            PIN I2 Q(4)
            PIN I3 Q(2)
            PIN O XLXN_1041
        END BLOCK
        BEGIN BLOCK XLXI_183 obuf
            PIN I XLXN_854
            PIN O Y
        END BLOCK
        BEGIN BLOCK XLXI_404 or4
            PIN I0 XLXN_1041
            PIN I1 XLXN_1040
            PIN I2 XLXN_1039
            PIN I3 XLXN_1038
            PIN O XLXN_854
        END BLOCK
        BEGIN BLOCK XLXI_405 or2
            PIN I0 XLXN_883
            PIN I1 XLXN_882
            PIN O R2
        END BLOCK
        BEGIN BLOCK XLXI_406 and3b1
            PIN I0 Q(1)
            PIN I1 Q(4)
            PIN I2 X
            PIN O XLXN_192
        END BLOCK
        BEGIN BLOCK XLXI_407 and4b1
            PIN I0 Q(1)
            PIN I1 Q(4)
            PIN I2 Q(3)
            PIN I3 Q(2)
            PIN O XLXN_1049
        END BLOCK
        BEGIN BLOCK XLXI_408 and4b4
            PIN I0 Q(3)
            PIN I1 Q(2)
            PIN I2 Q(1)
            PIN I3 X
            PIN O XLXN_194
        END BLOCK
        BEGIN BLOCK XLXI_410 and4b3
            PIN I0 Q(2)
            PIN I1 Q(4)
            PIN I2 Q(3)
            PIN I3 X
            PIN O R1
        END BLOCK
        BEGIN BLOCK XLXI_411 and5b2
            PIN I0 Q(2)
            PIN I1 Q(1)
            PIN I2 Q(4)
            PIN I3 Q(3)
            PIN I4 X
            PIN O S1
        END BLOCK
        BEGIN BLOCK XLXI_412 and3b2
            PIN I0 Q(4)
            PIN I1 Q(1)
            PIN I2 Q(3)
            PIN O XLXN_1080
        END BLOCK
        BEGIN BLOCK XLXI_231 and4b2
            PIN I0 Q(3)
            PIN I1 Q(1)
            PIN I2 Q(4)
            PIN I3 X
            PIN O XLXN_1081
        END BLOCK
        BEGIN BLOCK XLXI_414 or4
            PIN I0 XLXN_1083
            PIN I1 XLXN_1082
            PIN I2 XLXN_1081
            PIN I3 XLXN_1080
            PIN O S2
        END BLOCK
        BEGIN BLOCK XLXI_417 and4b4
            PIN I0 Q(4)
            PIN I1 Q(3)
            PIN I2 Q(2)
            PIN I3 X
            PIN O XLXN_1083
        END BLOCK
        BEGIN BLOCK XLXI_416 and4b3
            PIN I0 Q(4)
            PIN I1 Q(3)
            PIN I2 Q(2)
            PIN I3 Q(1)
            PIN O XLXN_1082
        END BLOCK
        BEGIN BLOCK XLXI_418 or4
            PIN I0 XLXN_1107
            PIN I1 XLXN_1094
            PIN I2 XLXN_1093
            PIN I3 XLXN_1092
            PIN O R3
        END BLOCK
        BEGIN BLOCK XLXI_421 and4b2
            PIN I0 Q(4)
            PIN I1 Q(1)
            PIN I2 Q(2)
            PIN I3 X
            PIN O XLXN_1092
        END BLOCK
        BEGIN BLOCK XLXI_424 and5b2
            PIN I0 Q(2)
            PIN I1 Q(1)
            PIN I2 Q(4)
            PIN I3 Q(3)
            PIN I4 X
            PIN O XLXN_1094
        END BLOCK
        BEGIN BLOCK XLXI_425 and5b2
            PIN I0 X
            PIN I1 Q(1)
            PIN I2 Q(4)
            PIN I3 Q(3)
            PIN I4 Q(2)
            PIN O XLXN_1107
        END BLOCK
        BEGIN BLOCK XLXI_426 or3
            PIN I0 XLXN_1111
            PIN I1 XLXN_1110
            PIN I2 XLXN_1109
            PIN O S3
        END BLOCK
        BEGIN BLOCK XLXI_427 and4b3
            PIN I0 Q(3)
            PIN I1 Q(1)
            PIN I2 X
            PIN I3 Q(2)
            PIN O XLXN_1109
        END BLOCK
        BEGIN BLOCK XLXI_428 and4b3
            PIN I0 Q(3)
            PIN I1 Q(2)
            PIN I2 Q(1)
            PIN I3 Q(4)
            PIN O XLXN_1110
        END BLOCK
        BEGIN BLOCK XLXI_429 and5b3
            PIN I0 Q(4)
            PIN I1 Q(3)
            PIN I2 Q(2)
            PIN I3 Q(1)
            PIN I4 X
            PIN O XLXN_1111
        END BLOCK
        BEGIN BLOCK XLXI_390 or3
            PIN I0 XLXN_1004
            PIN I1 XLXN_998
            PIN I2 XLXN_1003
            PIN O S4
        END BLOCK
        BEGIN BLOCK XLXI_430 and3b2
            PIN I0 Q(4)
            PIN I1 Q(1)
            PIN I2 X
            PIN O XLXN_1003
        END BLOCK
        BEGIN BLOCK XLXI_432 and3b2
            PIN I0 Q(4)
            PIN I1 Q(1)
            PIN I2 Q(2)
            PIN O XLXN_998
        END BLOCK
        BEGIN BLOCK XLXI_433 and4b3
            PIN I0 Q(4)
            PIN I1 Q(3)
            PIN I2 Q(2)
            PIN I3 Q(1)
            PIN O XLXN_1004
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 7040 5440
        BEGIN BRANCH X
            WIRE 640 272 784 272
            WIRE 784 272 928 272
            BEGIN DISPLAY 784 272 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_149 416 304 R0
        BEGIN BRANCH IN_X
            WIRE 384 272 400 272
            WIRE 400 272 416 272
            BEGIN DISPLAY 400 272 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_218 448 576 R0
        BEGIN BRANCH IN_RST
            WIRE 416 544 448 544
        END BRANCH
        BEGIN BRANCH RST
            WIRE 672 544 816 544
            WIRE 816 544 960 544
            BEGIN DISPLAY 816 544 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1:4)
            WIRE 5152 288 5312 288
            WIRE 5312 288 5632 288
            BEGIN DISPLAY 5312 288 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE XLXI_3 1856 560 R0
        END INSTANCE
        BEGIN BRANCH C
            WIRE 1728 528 1856 528
            BEGIN DISPLAY 1728 528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH RST
            WIRE 1616 336 1712 336
            WIRE 1712 336 1712 336
            WIRE 1712 336 1856 336
            BEGIN DISPLAY 1616 336 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 1716 336 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_152 2336 368 R0
        INSTANCE XLXI_220 2336 560 R0
        BEGIN BRANCH XLXN_12
            WIRE 2240 336 2336 336
        END BRANCH
        BEGIN BRANCH XLXN_13
            WIRE 2240 528 2336 528
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 2560 336 2736 336
            BEGIN DISPLAY 2736 336 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH NO_Q(1)
            WIRE 2560 528 2736 528
        END BRANCH
        BEGIN INSTANCE XLXI_283 2176 1472 R0
        END INSTANCE
        BEGIN BRANCH C
            WIRE 2048 1440 2176 1440
            BEGIN DISPLAY 2048 1440 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH RST
            WIRE 1936 1248 2032 1248
            WIRE 2032 1248 2032 1248
            WIRE 2032 1248 2176 1248
            BEGIN DISPLAY 1936 1248 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 2036 1248 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_284 2656 1280 R0
        INSTANCE XLXI_285 2656 1472 R0
        BEGIN BRANCH XLXN_22
            WIRE 2560 1248 2656 1248
        END BRANCH
        BEGIN BRANCH XLXN_23
            WIRE 2560 1440 2656 1440
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 2880 1248 3056 1248
            BEGIN DISPLAY 3056 1248 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH NO_Q(2)
            WIRE 2880 1440 3056 1440
        END BRANCH
        BEGIN BRANCH R1
            WIRE 1472 400 1536 400
            WIRE 1536 400 1856 400
            BEGIN DISPLAY 1536 400 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1104 304 1216 304
            BEGIN DISPLAY 1104 304 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1104 368 1216 368
            BEGIN DISPLAY 1104 368 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1104 432 1216 432
            BEGIN DISPLAY 1104 432 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1104 496 1216 496
            BEGIN DISPLAY 1104 496 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S3
            WIRE 1872 4176 1936 4176
            WIRE 1936 4176 1984 4176
            WIRE 1984 3536 2400 3536
            WIRE 1984 3536 1984 4176
            BEGIN DISPLAY 1936 4176 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R3
            WIRE 1744 3024 1808 3024
            WIRE 1808 3024 1872 3024
            WIRE 1872 3024 1872 3472
            WIRE 1872 3472 2400 3472
            BEGIN DISPLAY 1808 3024 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE XLXI_317 2400 3632 R0
        END INSTANCE
        BEGIN BRANCH C
            WIRE 2272 3600 2400 3600
            BEGIN DISPLAY 2272 3600 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH RST
            WIRE 2160 3408 2256 3408
            WIRE 2256 3408 2256 3408
            WIRE 2256 3408 2400 3408
            BEGIN DISPLAY 2160 3408 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 2260 3408 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_318 2880 3440 R0
        INSTANCE XLXI_319 2880 3632 R0
        BEGIN BRANCH XLXN_273
            WIRE 2784 3408 2880 3408
        END BRANCH
        BEGIN BRANCH XLXN_274
            WIRE 2784 3600 2880 3600
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 3104 3408 3280 3408
            BEGIN DISPLAY 3280 3408 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH NO_Q(3)
            WIRE 3104 3600 3280 3600
        END BRANCH
        BEGIN INSTANCE XLXI_320 4880 1472 R0
        END INSTANCE
        BEGIN BRANCH C
            WIRE 4752 1440 4880 1440
            BEGIN DISPLAY 4752 1440 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH RST
            WIRE 4640 1248 4736 1248
            WIRE 4736 1248 4736 1248
            WIRE 4736 1248 4880 1248
            BEGIN DISPLAY 4640 1248 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 4740 1248 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_321 5360 1280 R0
        INSTANCE XLXI_322 5360 1472 R0
        BEGIN BRANCH XLXN_281
            WIRE 5264 1248 5360 1248
        END BRANCH
        BEGIN BRANCH XLXN_282
            WIRE 5264 1440 5360 1440
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 5584 1248 5760 1248
            BEGIN DISPLAY 5760 1248 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH NO_Q(4)
            WIRE 5584 1440 5760 1440
        END BRANCH
        INSTANCE XLXI_273 4160 752 R0
        BEGIN BRANCH XLXN_192
            WIRE 3968 384 4160 384
            WIRE 4160 384 4160 560
        END BRANCH
        BEGIN BRANCH XLXN_194
            WIRE 3968 896 4160 896
            WIRE 4160 688 4160 896
        END BRANCH
        BEGIN BRANCH R4
            WIRE 4416 624 4448 624
            WIRE 4448 624 4544 624
            WIRE 4544 624 4544 1312
            WIRE 4544 1312 4880 1312
            BEGIN DISPLAY 4448 624 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S4
            WIRE 4416 1696 4448 1696
            WIRE 4448 1696 4544 1696
            WIRE 4544 1376 4544 1696
            WIRE 4544 1376 4880 1376
            BEGIN DISPLAY 4448 1696 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH NO_Q(1:4)
            WIRE 5152 544 5312 544
            WIRE 5312 544 5632 544
            BEGIN DISPLAY 5312 544 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        IOMARKER 384 272 IN_X R180 28
        IOMARKER 416 544 IN_RST R180 28
        IOMARKER 2736 528 NO_Q(1) R0 28
        IOMARKER 3056 1440 NO_Q(2) R0 28
        IOMARKER 3280 3600 NO_Q(3) R0 28
        IOMARKER 5760 1440 NO_Q(4) R0 28
        INSTANCE XLXI_150 384 704 R0
        BEGIN BRANCH IN_C
            WIRE 352 672 384 672
        END BRANCH
        BEGIN BRANCH C
            WIRE 608 672 832 672
            WIRE 832 672 832 672
            WIRE 832 672 960 672
            BEGIN DISPLAY 836 672 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        IOMARKER 352 672 IN_C R180 28
        INSTANCE XLXI_363 1136 1072 R0
        INSTANCE XLXI_364 1152 1312 R0
        BEGIN BRANCH XLXN_882
            WIRE 1392 912 1456 912
            WIRE 1456 912 1456 976
            WIRE 1456 976 1536 976
        END BRANCH
        BEGIN BRANCH XLXN_883
            WIRE 1408 1152 1472 1152
            WIRE 1472 1040 1472 1152
            WIRE 1472 1040 1536 1040
        END BRANCH
        BEGIN BRANCH X
            WIRE 1024 816 1136 816
            BEGIN DISPLAY 1024 816 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 1024 880 1136 880
            BEGIN DISPLAY 1024 880 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1024 944 1136 944
            BEGIN DISPLAY 1024 944 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1024 1008 1136 1008
            BEGIN DISPLAY 1024 1008 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1040 1056 1152 1056
            BEGIN DISPLAY 1040 1056 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1040 1120 1152 1120
            BEGIN DISPLAY 1040 1120 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1040 1184 1152 1184
            BEGIN DISPLAY 1040 1184 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 1040 1248 1152 1248
            BEGIN DISPLAY 1040 1248 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R2
            WIRE 1792 1008 1856 1008
            WIRE 1856 1008 1920 1008
            WIRE 1920 1008 1920 1312
            WIRE 1920 1312 2176 1312
            BEGIN DISPLAY 1856 1008 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S2
            WIRE 1920 1920 1984 1920
            WIRE 1984 1920 2032 1920
            WIRE 2032 1376 2032 1920
            WIRE 2032 1376 2176 1376
            BEGIN DISPLAY 1984 1920 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_377 1056 2912 R0
        BEGIN BRANCH X
            WIRE 944 2656 1056 2656
            BEGIN DISPLAY 944 2656 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 944 2720 1056 2720
            BEGIN DISPLAY 944 2720 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 944 2784 1056 2784
            BEGIN DISPLAY 944 2784 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 944 2848 1056 2848
            BEGIN DISPLAY 944 2848 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 3600 800 3712 800
            BEGIN DISPLAY 3600 800 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 3600 864 3712 864
            BEGIN DISPLAY 3600 864 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 3600 928 3712 928
            BEGIN DISPLAY 3600 928 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 3600 992 3712 992
            BEGIN DISPLAY 3600 992 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_354 3808 2848 R0
        BEGIN BRANCH Q(3)
            WIRE 3696 2656 3808 2656
            BEGIN DISPLAY 3696 2656 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 3696 2720 3808 2720
            BEGIN DISPLAY 3696 2720 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 3696 2784 3808 2784
            BEGIN DISPLAY 3696 2784 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_397 3808 3024 R0
        BEGIN BRANCH Q(3)
            WIRE 3696 2832 3808 2832
            BEGIN DISPLAY 3696 2832 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 3696 2896 3808 2896
            BEGIN DISPLAY 3696 2896 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 3696 2960 3808 2960
            BEGIN DISPLAY 3696 2960 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_399 3808 3216 R0
        BEGIN BRANCH Q(1)
            WIRE 3696 3024 3808 3024
            BEGIN DISPLAY 3696 3024 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 3696 3088 3808 3088
            BEGIN DISPLAY 3696 3088 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 3696 3152 3808 3152
            BEGIN DISPLAY 3696 3152 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_355 3808 3456 R0
        BEGIN BRANCH Q(2)
            WIRE 3696 3200 3808 3200
            BEGIN DISPLAY 3696 3200 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 3696 3264 3808 3264
            BEGIN DISPLAY 3696 3264 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 3696 3328 3808 3328
            BEGIN DISPLAY 3696 3328 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 3696 3392 3808 3392
            BEGIN DISPLAY 3696 3392 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_183 4560 3008 R0
        BEGIN BRANCH Y
            WIRE 4784 2976 4816 2976
        END BRANCH
        IOMARKER 4816 2976 Y R0 28
        BEGIN BRANCH XLXN_854
            WIRE 4448 2976 4560 2976
        END BRANCH
        INSTANCE XLXI_404 4192 3136 R0
        BEGIN BRANCH XLXN_1038
            WIRE 4064 2720 4192 2720
            WIRE 4192 2720 4192 2880
        END BRANCH
        BEGIN BRANCH XLXN_1039
            WIRE 4064 2896 4128 2896
            WIRE 4128 2896 4128 2944
            WIRE 4128 2944 4192 2944
        END BRANCH
        BEGIN BRANCH XLXN_1040
            WIRE 4064 3088 4128 3088
            WIRE 4128 3008 4128 3088
            WIRE 4128 3008 4192 3008
        END BRANCH
        BEGIN BRANCH XLXN_1041
            WIRE 4064 3296 4192 3296
            WIRE 4192 3072 4192 3296
        END BRANCH
        INSTANCE XLXI_405 1536 1104 R0
        INSTANCE XLXI_406 3712 512 R0
        INSTANCE XLXI_407 3712 784 R0
        BEGIN BRANCH Q(2)
            WIRE 3600 528 3712 528
            BEGIN DISPLAY 3600 528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 3600 592 3712 592
            BEGIN DISPLAY 3600 592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 3600 656 3712 656
            BEGIN DISPLAY 3600 656 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 3600 720 3712 720
            BEGIN DISPLAY 3600 720 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_408 3712 1056 R0
        BEGIN BRANCH X
            WIRE 3600 320 3712 320
            BEGIN DISPLAY 3600 320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 3600 384 3712 384
            BEGIN DISPLAY 3600 384 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 3600 448 3712 448
            BEGIN DISPLAY 3600 448 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_1049
            WIRE 3968 624 4160 624
        END BRANCH
        BEGIN BRANCH S1
            WIRE 1456 656 1488 656
            WIRE 1488 624 1552 624
            WIRE 1552 624 1664 624
            WIRE 1488 624 1488 656
            WIRE 1664 464 1664 624
            WIRE 1664 464 1856 464
            BEGIN DISPLAY 1552 624 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_410 1216 560 R0
        INSTANCE XLXI_411 1200 848 R0
        BEGIN BRANCH X
            WIRE 1088 528 1200 528
            BEGIN DISPLAY 1088 528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1088 592 1200 592
            BEGIN DISPLAY 1088 592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1088 656 1200 656
            BEGIN DISPLAY 1088 656 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 1088 720 1200 720
            BEGIN DISPLAY 1088 720 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1088 784 1200 784
            BEGIN DISPLAY 1088 784 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_412 1200 1632 R0
        BEGIN BRANCH Q(4)
            WIRE 1088 1568 1104 1568
            WIRE 1104 1568 1200 1568
            BEGIN DISPLAY 1088 1568 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 1088 1504 1104 1504
            WIRE 1104 1504 1200 1504
            BEGIN DISPLAY 1088 1504 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1088 1440 1104 1440
            WIRE 1104 1440 1200 1440
            BEGIN DISPLAY 1088 1440 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1056 1664 1200 1664
            BEGIN DISPLAY 1056 1664 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1056 1728 1200 1728
            BEGIN DISPLAY 1056 1728 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 1056 1792 1200 1792
            BEGIN DISPLAY 1056 1792 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1056 1856 1200 1856
            BEGIN DISPLAY 1056 1856 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_231 1200 1920 R0
        INSTANCE XLXI_414 1664 2080 R0
        BEGIN BRANCH Q(1)
            WIRE 1088 1904 1200 1904
            BEGIN DISPLAY 1088 1904 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1088 1968 1200 1968
            BEGIN DISPLAY 1088 1968 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1088 2032 1200 2032
            BEGIN DISPLAY 1088 2032 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1088 2096 1200 2096
            BEGIN DISPLAY 1088 2096 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_417 1200 2432 R0
        BEGIN BRANCH X
            WIRE 1088 2176 1200 2176
            BEGIN DISPLAY 1088 2176 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 1088 2240 1200 2240
            BEGIN DISPLAY 1088 2240 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1088 2304 1200 2304
            BEGIN DISPLAY 1088 2304 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 1088 2368 1200 2368
            BEGIN DISPLAY 1088 2368 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_416 1200 2160 R0
        BEGIN BRANCH XLXN_1080
            WIRE 1456 1504 1664 1504
            WIRE 1664 1504 1664 1824
        END BRANCH
        BEGIN BRANCH XLXN_1081
            WIRE 1456 1760 1552 1760
            WIRE 1552 1760 1552 1888
            WIRE 1552 1888 1664 1888
        END BRANCH
        BEGIN BRANCH XLXN_1082
            WIRE 1456 2000 1552 2000
            WIRE 1552 1952 1552 2000
            WIRE 1552 1952 1664 1952
        END BRANCH
        BEGIN BRANCH XLXN_1083
            WIRE 1456 2272 1664 2272
            WIRE 1664 2016 1664 2272
        END BRANCH
        INSTANCE XLXI_418 1488 3184 R0
        BEGIN BRANCH X
            WIRE 944 2432 1056 2432
            BEGIN DISPLAY 944 2432 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 944 2496 1056 2496
            BEGIN DISPLAY 944 2496 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 944 2560 1056 2560
            BEGIN DISPLAY 944 2560 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 944 2624 1056 2624
            BEGIN DISPLAY 944 2624 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_1092
            WIRE 1312 2528 1488 2528
            WIRE 1488 2528 1488 2928
        END BRANCH
        BEGIN BRANCH XLXN_1093
            WIRE 1312 2752 1392 2752
            WIRE 1392 2752 1392 2992
            WIRE 1392 2992 1488 2992
        END BRANCH
        BEGIN BRANCH XLXN_1094
            WIRE 1312 3024 1392 3024
            WIRE 1392 3024 1392 3056
            WIRE 1392 3056 1488 3056
        END BRANCH
        INSTANCE XLXI_421 1056 2688 R0
        INSTANCE XLXI_424 1056 3216 R0
        BEGIN BRANCH X
            WIRE 944 2896 1056 2896
            BEGIN DISPLAY 944 2896 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 944 2960 1056 2960
            BEGIN DISPLAY 944 2960 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 944 3024 1056 3024
            BEGIN DISPLAY 944 3024 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 944 3088 1056 3088
            BEGIN DISPLAY 944 3088 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 944 3152 1056 3152
            BEGIN DISPLAY 944 3152 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_425 1056 3504 R0
        BEGIN BRANCH Q(2)
            WIRE 944 3184 1056 3184
            BEGIN DISPLAY 944 3184 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 944 3248 1056 3248
            BEGIN DISPLAY 944 3248 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 944 3312 1056 3312
            BEGIN DISPLAY 944 3312 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 944 3376 1056 3376
            BEGIN DISPLAY 944 3376 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 944 3440 1056 3440
            BEGIN DISPLAY 944 3440 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_1107
            WIRE 1312 3312 1488 3312
            WIRE 1488 3120 1488 3312
        END BRANCH
        INSTANCE XLXI_426 1616 4304 R0
        INSTANCE XLXI_427 1104 4096 R0
        INSTANCE XLXI_428 1104 4352 R0
        INSTANCE XLXI_429 1104 4656 R0
        BEGIN BRANCH XLXN_1109
            WIRE 1360 3936 1616 3936
            WIRE 1616 3936 1616 4112
        END BRANCH
        BEGIN BRANCH XLXN_1110
            WIRE 1360 4192 1488 4192
            WIRE 1488 4176 1488 4192
            WIRE 1488 4176 1616 4176
        END BRANCH
        BEGIN BRANCH XLXN_1111
            WIRE 1360 4464 1616 4464
            WIRE 1616 4240 1616 4464
        END BRANCH
        BEGIN BRANCH X
            WIRE 992 4336 1104 4336
            BEGIN DISPLAY 992 4336 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 992 4400 1104 4400
            BEGIN DISPLAY 992 4400 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 992 4464 1104 4464
            BEGIN DISPLAY 992 4464 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 992 4528 1104 4528
            BEGIN DISPLAY 992 4528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 992 4592 1104 4592
            BEGIN DISPLAY 992 4592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 992 3840 1104 3840
            BEGIN DISPLAY 992 3840 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 992 3904 1104 3904
            BEGIN DISPLAY 992 3904 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 992 3968 1104 3968
            BEGIN DISPLAY 992 3968 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 992 4032 1104 4032
            BEGIN DISPLAY 992 4032 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 992 4096 1104 4096
            BEGIN DISPLAY 992 4096 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 992 4160 1104 4160
            BEGIN DISPLAY 992 4160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 992 4224 1104 4224
            BEGIN DISPLAY 992 4224 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 992 4288 1104 4288
            BEGIN DISPLAY 992 4288 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_998
            WIRE 3968 1696 4160 1696
        END BRANCH
        BEGIN BRANCH XLXN_1004
            WIRE 3968 1936 4160 1936
            WIRE 4160 1760 4160 1936
        END BRANCH
        BEGIN BRANCH XLXN_1003
            WIRE 3968 1440 4160 1440
            WIRE 4160 1440 4160 1632
        END BRANCH
        INSTANCE XLXI_390 4160 1824 R0
        INSTANCE XLXI_430 3712 1568 R0
        INSTANCE XLXI_432 3712 1824 R0
        INSTANCE XLXI_433 3712 2096 R0
        BEGIN BRANCH Q(1)
            WIRE 3600 1840 3712 1840
            BEGIN DISPLAY 3600 1840 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 3600 1904 3712 1904
            BEGIN DISPLAY 3600 1904 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 3600 1968 3712 1968
            BEGIN DISPLAY 3600 1968 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 3600 2032 3712 2032
            BEGIN DISPLAY 3600 2032 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 3600 1376 3712 1376
            BEGIN DISPLAY 3600 1376 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 3600 1440 3712 1440
            BEGIN DISPLAY 3600 1440 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 3600 1504 3712 1504
            BEGIN DISPLAY 3600 1504 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 3600 1632 3712 1632
            BEGIN DISPLAY 3600 1632 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 3600 1696 3712 1696
            BEGIN DISPLAY 3600 1696 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(4)
            WIRE 3600 1760 3712 1760
            BEGIN DISPLAY 3600 1760 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
    END SHEET
END SCHEMATIC
