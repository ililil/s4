#define UNICODE
#define _USE_MATH_DEFINES
#define WIN32_LEAN_AND_MEAN // Exclude rarely-used stuff from Windows headers

#include <SDKDDKVer.h>
#include <windows.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>

#define szTitle L" "
#define szWindowClass L" "

#define MAX_ITER 256
#define IMG_COUNT 3

int windowWidth = 1200, windowHeight = 1000;

HINSTANCE hInst = NULL;
HBITMAP img[IMG_COUNT];

BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	img[0] = (HBITMAP) LoadImage(NULL, L"frac.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
	img[1] = (HBITMAP) LoadImage(NULL, L"img.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
	img[2] = (HBITMAP) LoadImage(NULL, L"teapot.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);

	if (!InitInstance(hInstance, nCmdShow))
		return 1;

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int) msg.wParam;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
	WNDCLASSEXW wcex = {0};

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, IDI_WINLOGO);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	wcex.lpszMenuName = L"Menu";
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(hInstance, IDI_WINLOGO);

	RegisterClassEx(&wcex);
	hInst = hInstance;

	HWND hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, windowWidth, windowHeight, NULL, NULL, hInstance, NULL);

	if (!hWnd)
		return FALSE;

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

/*
 * channel: gray, normal, blue, green, red = -1 .. 3
 */
void blackbox(HDC hdc, HBITMAP bitmap, int x0, int y0, int channel) {
	BITMAP bmp = {0};
	GetObjectW(bitmap, sizeof(bmp), &bmp);

	BYTE *data = (BYTE*) bmp.bmBits;
	for (int x = 0; x < bmp.bmWidth + (bmp.bmWidth * 3 % 4); x++) {
		int nx = x0 + x;
		int counter = 0;

		for (int y = 0; y < bmp.bmHeight; y++) {
			int offset = (((bmp.bmWidth + (bmp.bmWidth * 3 % 4)) * y) + x) * 3; // ((width + pad) * y + x) * 3
			BYTE b = data[offset];
			BYTE g = data[offset + 1];
			BYTE r = data[offset + 2];

			BYTE color;
			if (channel <= 0)
				color = (r + g + b) / 3;
			else
				color = data[offset + channel - 1];

			if (channel == -1)
				data[offset] = data[offset + 1] = data[offset + 2] = color;
			else if (channel == 1)
				data[offset + 1] = data[offset + 2] = 0;
			else if (channel == 2)
				data[offset] = data[offset + 2] = 0;
			else if (channel == 3)
				data[offset] = data[offset + 1] = 0;
			if (color < 88)
				counter++;
		}

		for (int y = 0; y < counter; y++) {
			int ny = y0 + y;
			SetPixelV(hdc, nx, ny + bmp.bmHeight + 2, RGB(0, 0, 0));
		}
	}

	HDC imgdc = CreateCompatibleDC(hdc);
	SelectObject(imgdc, bitmap);
	BitBlt(hdc, x0, y0, bmp.bmWidth, bmp.bmHeight, imgdc, 0, 0, SRCCOPY);
	DeleteDC(imgdc);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		case WM_COMMAND: {
			int wmId = LOWORD(wParam);
			switch (wmId) {
				default: {
					return DefWindowProc(hWnd, message, wParam, lParam);
				}
			}
			break;
		}

		case WM_PAINT: {
			RECT rect;
			GetWindowRect(hWnd, &rect);
			windowWidth = rect.right - rect.left;
			windowHeight = rect.bottom - rect.top;

			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);

			blackbox(hdc, img[0], 2, 2, 0);
			blackbox(hdc, img[1], 300, 2, 0);
			blackbox(hdc, img[2], 630, 2, 0);

			blackbox(hdc, img[0], 2, 500, 3);
			blackbox(hdc, img[1], 300, 500, 1);
			blackbox(hdc, img[2], 630, 500, 2);

			EndPaint(hWnd, &ps);
			break;
		}

		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}

		default: {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	return 0;
}
