VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL XLXN_1
        SIGNAL XLXN_2
        SIGNAL x0
        SIGNAL S1
        SIGNAL x1
        SIGNAL S2
        SIGNAL x2
        SIGNAL S3
        SIGNAL x3
        SIGNAL D1in
        SIGNAL S0
        SIGNAL XLXN_10
        SIGNAL XLXN_137
        SIGNAL XLXN_75
        SIGNAL y1
        SIGNAL y2
        SIGNAL y3
        SIGNAL y0
        SIGNAL x0_in
        SIGNAL x1_in
        SIGNAL x2_in
        SIGNAL x3_in
        SIGNAL RST_INPUT
        SIGNAL CLK
        SIGNAL CE
        SIGNAL XLXN_270
        SIGNAL XLXN_271
        SIGNAL XLXN_272
        SIGNAL XLXN_273
        SIGNAL S1in
        SIGNAL R1in
        SIGNAL S2in
        SIGNAL XLXN_288
        SIGNAL XLXN_289
        SIGNAL R2in
        SIGNAL S3in
        SIGNAL XLXN_302
        SIGNAL XLXN_303
        SIGNAL R3in
        SIGNAL XLXN_313
        SIGNAL XLXN_314
        PORT Output y1
        PORT Output y2
        PORT Output y3
        PORT Output y0
        PORT Input x0_in
        PORT Input x1_in
        PORT Input x2_in
        PORT Input x3_in
        PORT Input RST_INPUT
        PORT Input CLK
        PORT Input CE
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF and3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -176 144 -176 
            LINE N 144 -80 64 -80 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 64 -64 64 -192 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF or3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 72 -128 
            LINE N 0 -192 48 -192 
            LINE N 256 -128 192 -128 
            ARC N 28 -256 204 -80 112 -80 192 -128 
            ARC N -40 -184 72 -72 48 -80 48 -176 
            LINE N 48 -64 48 -80 
            LINE N 48 -192 48 -176 
            LINE N 112 -80 48 -80 
            ARC N 28 -176 204 0 192 -128 112 -176 
            LINE N 112 -176 48 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF ibuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCKDEF bufg
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 0 
            LINE N 128 -32 64 -64 
            LINE N 64 0 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF fdce
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -32 64 -32 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            LINE N 64 -112 80 -128 
            LINE N 80 -128 64 -144 
            LINE N 192 -64 192 -32 
            LINE N 192 -32 64 -32 
            RECTANGLE N 64 -320 320 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF fjkce
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -192 64 -192 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            RECTANGLE N 64 -384 320 -64 
            LINE N 384 -256 320 -256 
            LINE N 0 -320 64 -320 
            LINE N 0 -32 64 -32 
            LINE N 0 -128 64 -128 
            LINE N 0 -256 64 -256 
        END BLOCKDEF
        BEGIN BLOCKDEF or4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 48 -256 
            LINE N 256 -160 192 -160 
            ARC N 28 -208 204 -32 192 -160 112 -208 
            LINE N 112 -208 48 -208 
            LINE N 112 -112 48 -112 
            LINE N 48 -256 48 -208 
            LINE N 48 -64 48 -112 
            ARC N -40 -216 72 -104 48 -112 48 -208 
            ARC N 28 -288 204 -112 112 -112 192 -160 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 and2b1
            PIN I0 x1
            PIN I1 S0
            PIN O XLXN_1
        END BLOCK
        BEGIN BLOCK XLXI_2 and2
            PIN I0 x3
            PIN I1 S2
            PIN O XLXN_2
        END BLOCK
        BEGIN BLOCK XLXI_12 or3
            PIN I0 x0
            PIN I1 XLXN_2
            PIN I2 XLXN_1
            PIN O D1in
        END BLOCK
        BEGIN BLOCK XLXI_7 and2
            PIN I0 x1
            PIN I1 S0
            PIN O XLXN_288
        END BLOCK
        BEGIN BLOCK XLXI_46 and3b1
            PIN I0 x1
            PIN I1 x2
            PIN I2 S1
            PIN O XLXN_289
        END BLOCK
        BEGIN BLOCK XLXI_25 obuf
            PIN I S1
            PIN O y1
        END BLOCK
        BEGIN BLOCK XLXI_26 obuf
            PIN I S2
            PIN O y2
        END BLOCK
        BEGIN BLOCK XLXI_27 obuf
            PIN I XLXN_75
            PIN O y3
        END BLOCK
        BEGIN BLOCK XLXI_23 or2
            PIN I0 S3
            PIN I1 S2
            PIN O XLXN_75
        END BLOCK
        BEGIN BLOCK XLXI_72 obuf
            PIN I S0
            PIN O y0
        END BLOCK
        BEGIN BLOCK XLXI_17 ibuf
            PIN I x0_in
            PIN O x0
        END BLOCK
        BEGIN BLOCK XLXI_18 ibuf
            PIN I x1_in
            PIN O x1
        END BLOCK
        BEGIN BLOCK XLXI_19 ibuf
            PIN I x2_in
            PIN O x2
        END BLOCK
        BEGIN BLOCK XLXI_20 ibuf
            PIN I x3_in
            PIN O x3
        END BLOCK
        BEGIN BLOCK XLXI_24 bufg
            PIN I RST_INPUT
            PIN O XLXN_313
        END BLOCK
        BEGIN BLOCK XLXI_88 fdce
            PIN C CLK
            PIN CE CE
            PIN CLR XLXN_313
            PIN D D1in
            PIN Q S0
        END BLOCK
        BEGIN BLOCK XLXI_89 fjkce
            PIN C CLK
            PIN CE CE
            PIN CLR XLXN_313
            PIN J S1in
            PIN K R1in
            PIN Q S1
        END BLOCK
        BEGIN BLOCK XLXI_90 fjkce
            PIN C CLK
            PIN CE CE
            PIN CLR XLXN_313
            PIN J S2in
            PIN K R2in
            PIN Q S2
        END BLOCK
        BEGIN BLOCK XLXI_91 fjkce
            PIN C CLK
            PIN CE CE
            PIN CLR XLXN_313
            PIN J S3in
            PIN K R3in
            PIN Q S3
        END BLOCK
        BEGIN BLOCK XLXI_4 and2
            PIN I0 x1
            PIN I1 S0
            PIN O XLXN_270
        END BLOCK
        BEGIN BLOCK XLXI_5 and2b1
            PIN I0 x2
            PIN I1 S1
            PIN O XLXN_271
        END BLOCK
        BEGIN BLOCK XLXI_43 and3b1
            PIN I0 x2
            PIN I1 x3
            PIN I2 S2
            PIN O XLXN_272
        END BLOCK
        BEGIN BLOCK XLXI_44 and2b1
            PIN I0 x3
            PIN I1 S3
            PIN O XLXN_273
        END BLOCK
        BEGIN BLOCK XLXI_28 and3
            PIN I0 x2
            PIN I1 x1
            PIN I2 S1
            PIN O R1in
        END BLOCK
        BEGIN BLOCK XLXI_101 or4
            PIN I0 XLXN_273
            PIN I1 XLXN_272
            PIN I2 XLXN_271
            PIN I3 XLXN_270
            PIN O S1in
        END BLOCK
        BEGIN BLOCK XLXI_102 and3b1
            PIN I0 x3
            PIN I1 x1
            PIN I2 S2
            PIN O R2in
        END BLOCK
        BEGIN BLOCK XLXI_106 or2
            PIN I0 XLXN_289
            PIN I1 XLXN_288
            PIN O S2in
        END BLOCK
        BEGIN BLOCK XLXI_10 and2b1
            PIN I0 x2
            PIN I1 S3
            PIN O XLXN_302
        END BLOCK
        BEGIN BLOCK XLXI_57 and3b1
            PIN I0 x1
            PIN I1 x2
            PIN I2 S2
            PIN O XLXN_303
        END BLOCK
        BEGIN BLOCK XLXI_109 or2
            PIN I0 XLXN_302
            PIN I1 XLXN_303
            PIN O S3in
        END BLOCK
        BEGIN BLOCK XLXI_56 and3
            PIN I0 x2
            PIN I1 x3
            PIN I2 S3
            PIN O R3in
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_1 384 288 R0
        INSTANCE XLXI_2 384 448 R0
        INSTANCE XLXI_12 704 480 R0
        BEGIN BRANCH XLXN_1
            WIRE 640 192 704 192
            WIRE 704 192 704 288
        END BRANCH
        BEGIN BRANCH XLXN_2
            WIRE 640 352 704 352
        END BRANCH
        BEGIN BRANCH x0
            WIRE 336 528 704 528
            WIRE 704 416 704 528
            BEGIN DISPLAY 336 528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S0
            WIRE 288 160 384 160
            BEGIN DISPLAY 288 160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x1
            WIRE 288 224 384 224
            BEGIN DISPLAY 288 224 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S2
            WIRE 288 320 384 320
            BEGIN DISPLAY 288 320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x3
            WIRE 288 384 384 384
            BEGIN DISPLAY 288 384 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D1in
            WIRE 960 352 1008 352
            WIRE 1008 352 1072 352
            BEGIN DISPLAY 1008 352 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x1
            WIRE 288 1696 352 1696
            BEGIN DISPLAY 288 1696 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S1
            WIRE 288 1760 352 1760
            BEGIN DISPLAY 288 1760 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S0
            WIRE 288 1632 352 1632
            BEGIN DISPLAY 288 1632 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_7 352 1760 R0
        BEGIN BRANCH x2
            WIRE 272 1824 352 1824
            BEGIN DISPLAY 272 1824 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x1
            WIRE 272 1888 352 1888
            BEGIN DISPLAY 272 1888 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_46 352 1952 R0
        BEGIN BRANCH S2
            WIRE 1504 2144 1648 2144
            BEGIN DISPLAY 1504 2144 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S3
            WIRE 1520 2208 1648 2208
            BEGIN DISPLAY 1520 2208 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_75
            WIRE 1904 2176 2080 2176
        END BRANCH
        BEGIN BRANCH S2
            WIRE 1776 2000 2080 2000
            BEGIN DISPLAY 1776 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S1
            WIRE 1760 1824 2080 1824
            BEGIN DISPLAY 1760 1824 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_25 2080 1856 R0
        INSTANCE XLXI_26 2080 2032 R0
        INSTANCE XLXI_27 2080 2208 R0
        BEGIN BRANCH y1
            WIRE 2304 1824 2336 1824
        END BRANCH
        BEGIN BRANCH y2
            WIRE 2304 2000 2336 2000
        END BRANCH
        BEGIN BRANCH y3
            WIRE 2304 2176 2336 2176
        END BRANCH
        INSTANCE XLXI_23 1648 2272 R0
        BEGIN BRANCH S0
            WIRE 1760 1680 2080 1680
            BEGIN DISPLAY 1760 1680 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_72 2080 1712 R0
        BEGIN BRANCH y0
            WIRE 2304 1680 2336 1680
        END BRANCH
        IOMARKER 2336 1824 y1 R0 28
        IOMARKER 2336 2000 y2 R0 28
        IOMARKER 2336 2176 y3 R0 28
        IOMARKER 2336 1680 y0 R0 28
        BEGIN BRANCH x0_in
            WIRE 2640 2080 2880 2080
        END BRANCH
        BEGIN BRANCH x1_in
            WIRE 2640 2144 2880 2144
        END BRANCH
        BEGIN BRANCH x2_in
            WIRE 2640 2208 2880 2208
        END BRANCH
        BEGIN BRANCH x3_in
            WIRE 2640 2272 2880 2272
        END BRANCH
        INSTANCE XLXI_17 2880 2112 R0
        INSTANCE XLXI_18 2880 2176 R0
        INSTANCE XLXI_19 2880 2240 R0
        INSTANCE XLXI_20 2880 2304 R0
        BEGIN BRANCH x0
            WIRE 3104 2080 3200 2080
            WIRE 3200 2080 3328 2080
            BEGIN DISPLAY 3208 2080 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x1
            WIRE 3104 2144 3200 2144
            WIRE 3200 2144 3328 2144
            BEGIN DISPLAY 3200 2144 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x2
            WIRE 3104 2208 3200 2208
            WIRE 3200 2208 3328 2208
            BEGIN DISPLAY 3200 2208 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x3
            WIRE 3104 2272 3200 2272
            WIRE 3200 2272 3328 2272
            BEGIN DISPLAY 3200 2272 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        IOMARKER 2640 2080 x0_in R180 28
        IOMARKER 2640 2144 x1_in R180 28
        IOMARKER 2640 2208 x2_in R180 28
        IOMARKER 2640 2272 x3_in R180 28
        INSTANCE XLXI_24 1760 256 R0
        BEGIN BRANCH RST_INPUT
            WIRE 1696 224 1760 224
        END BRANCH
        IOMARKER 1696 224 RST_INPUT R180 28
        BEGIN BRANCH CLK
            WIRE 1984 144 2064 144
            WIRE 2064 144 2064 336
            WIRE 2064 336 2064 736
            WIRE 2064 736 2064 1136
            WIRE 2064 1136 2064 1520
            WIRE 2064 1520 2448 1520
            WIRE 2064 1136 2448 1136
            WIRE 2064 736 2448 736
            WIRE 2064 336 2448 336
        END BRANCH
        IOMARKER 1984 144 CLK R180 28
        BEGIN BRANCH CE
            WIRE 2016 80 2128 80
            WIRE 2128 80 2144 80
            WIRE 2144 80 2144 272
            WIRE 2144 272 2448 272
            WIRE 2144 272 2144 672
            WIRE 2144 672 2448 672
            WIRE 2144 672 2144 1056
            WIRE 2144 1056 2144 1072
            WIRE 2144 1072 2144 1456
            WIRE 2144 1456 2448 1456
            WIRE 2144 1072 2448 1072
        END BRANCH
        IOMARKER 2016 80 CE R180 28
        BEGIN BRANCH D1in
            WIRE 2320 208 2448 208
            BEGIN DISPLAY 2320 208 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_88 2448 464 R0
        INSTANCE XLXI_89 2448 864 R0
        INSTANCE XLXI_90 2448 1264 R0
        INSTANCE XLXI_91 2448 1648 R0
        BEGIN BRANCH S0
            WIRE 2832 208 2912 208
            WIRE 2912 208 2960 208
            BEGIN DISPLAY 2912 208 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S1
            WIRE 2832 608 2944 608
            WIRE 2944 608 2960 608
            BEGIN DISPLAY 2940 608 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S2
            WIRE 2832 1008 2896 1008
            WIRE 2896 1008 2960 1008
            BEGIN DISPLAY 2904 1008 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S3
            WIRE 2832 1392 2928 1392
            WIRE 2928 1392 2960 1392
            BEGIN DISPLAY 2924 1392 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_4 368 736 R0
        INSTANCE XLXI_5 368 912 R0
        BEGIN BRANCH S0
            WIRE 288 608 368 608
            BEGIN DISPLAY 288 608 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x1
            WIRE 288 672 368 672
            BEGIN DISPLAY 288 672 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S1
            WIRE 304 784 368 784
            BEGIN DISPLAY 304 784 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x2
            WIRE 288 848 368 848
            BEGIN DISPLAY 288 848 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_43 336 1136 R0
        BEGIN BRANCH S2
            WIRE 256 944 336 944
            BEGIN DISPLAY 256 944 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x3
            WIRE 256 1008 336 1008
            BEGIN DISPLAY 256 1008 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x2
            WIRE 256 1072 336 1072
            BEGIN DISPLAY 256 1072 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S3
            WIRE 256 1136 336 1136
            BEGIN DISPLAY 256 1136 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x3
            WIRE 256 1200 336 1200
            BEGIN DISPLAY 256 1200 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_44 336 1264 R0
        INSTANCE XLXI_28 320 1504 R0
        BEGIN BRANCH S1
            WIRE 256 1312 320 1312
            BEGIN DISPLAY 256 1312 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x1
            WIRE 256 1376 320 1376
            BEGIN DISPLAY 256 1376 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x2
            WIRE 256 1440 320 1440
            BEGIN DISPLAY 256 1440 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R1in
            WIRE 576 1376 800 1376
            WIRE 800 1376 912 1376
            BEGIN DISPLAY 804 1376 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_101 672 1072 R0
        BEGIN BRANCH XLXN_270
            WIRE 624 640 672 640
            WIRE 672 640 672 816
        END BRANCH
        BEGIN BRANCH XLXN_271
            WIRE 624 816 640 816
            WIRE 640 816 640 880
            WIRE 640 880 672 880
        END BRANCH
        BEGIN BRANCH XLXN_272
            WIRE 592 1008 624 1008
            WIRE 624 944 624 1008
            WIRE 624 944 672 944
        END BRANCH
        BEGIN BRANCH XLXN_273
            WIRE 592 1168 672 1168
            WIRE 672 1008 672 1168
        END BRANCH
        BEGIN BRANCH S1in
            WIRE 928 912 1008 912
            WIRE 1008 912 1088 912
            BEGIN DISPLAY 1016 912 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S1in
            WIRE 2304 544 2448 544
            BEGIN DISPLAY 2304 544 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R1in
            WIRE 2304 608 2448 608
            BEGIN DISPLAY 2304 608 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_102 320 2160 R0
        BEGIN BRANCH S2
            WIRE 256 1968 320 1968
            BEGIN DISPLAY 256 1968 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x1
            WIRE 256 2032 320 2032
            BEGIN DISPLAY 256 2032 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x3
            WIRE 256 2096 320 2096
            BEGIN DISPLAY 256 2096 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S2in
            WIRE 976 1760 1008 1760
            WIRE 1008 1760 1072 1760
            BEGIN DISPLAY 1016 1760 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_106 720 1856 R0
        BEGIN BRANCH XLXN_288
            WIRE 608 1664 656 1664
            WIRE 656 1664 656 1728
            WIRE 656 1728 720 1728
        END BRANCH
        BEGIN BRANCH XLXN_289
            WIRE 608 1824 656 1824
            WIRE 656 1792 656 1824
            WIRE 656 1792 720 1792
        END BRANCH
        BEGIN BRANCH R2in
            WIRE 576 2032 688 2032
            WIRE 688 2032 736 2032
            BEGIN DISPLAY 685 2032 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x2
            WIRE 224 2432 320 2432
            BEGIN DISPLAY 224 2432 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S3
            WIRE 224 2368 320 2368
            BEGIN DISPLAY 224 2368 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_10 320 2496 R0
        INSTANCE XLXI_57 304 2368 R0
        BEGIN BRANCH S2
            WIRE 192 2176 304 2176
            BEGIN DISPLAY 192 2176 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x2
            WIRE 192 2240 304 2240
            BEGIN DISPLAY 192 2240 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x1
            WIRE 192 2304 304 2304
            BEGIN DISPLAY 192 2304 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_109 720 2400 R0
        BEGIN BRANCH S3in
            WIRE 976 2304 1056 2304
            WIRE 1056 2304 1104 2304
            BEGIN DISPLAY 1064 2304 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_302
            WIRE 576 2400 640 2400
            WIRE 640 2336 720 2336
            WIRE 640 2336 640 2400
        END BRANCH
        BEGIN BRANCH XLXN_303
            WIRE 560 2240 640 2240
            WIRE 640 2240 640 2272
            WIRE 640 2272 720 2272
        END BRANCH
        BEGIN BRANCH S3
            WIRE 208 2528 304 2528
            BEGIN DISPLAY 208 2528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x3
            WIRE 208 2592 304 2592
            BEGIN DISPLAY 208 2592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH x2
            WIRE 208 2656 304 2656
            BEGIN DISPLAY 208 2656 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_56 304 2720 R0
        BEGIN BRANCH R3in
            WIRE 560 2592 608 2592
            WIRE 608 2592 704 2592
            BEGIN DISPLAY 610 2592 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S2in
            WIRE 2304 944 2448 944
            BEGIN DISPLAY 2304 944 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R2in
            WIRE 2304 1008 2448 1008
            BEGIN DISPLAY 2304 1008 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S3in
            WIRE 2320 1328 2448 1328
            BEGIN DISPLAY 2320 1328 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R3in
            WIRE 2320 1392 2448 1392
            BEGIN DISPLAY 2320 1392 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_313
            WIRE 1984 224 1984 432
            WIRE 1984 432 1984 832
            WIRE 1984 832 1984 1232
            WIRE 1984 1232 1984 1616
            WIRE 1984 1616 2448 1616
            WIRE 1984 1232 2448 1232
            WIRE 1984 832 2448 832
            WIRE 1984 432 2448 432
        END BRANCH
    END SHEET
END SCHEMATIC
