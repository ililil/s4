#include <iostream>
#include <map>

using namespace std;

void printp(multimap<int, int> pairs) {
	for (auto p = pairs.begin(); p != pairs.end(); p++)
		cout << p->first << " " << p->second << endl;
	cout << endl;
}

int main() {

	multimap<int, int> pairs;

	pairs.insert( {1, 1});
	pairs.insert( {1, 1});
	pairs.insert( {2, 1});
	pairs.insert( {2, 1});
	pairs.insert( {3, 4});
	pairs.insert( {5, 6});
	printp(pairs);

	for (auto p = pairs.begin(); p != pairs.end();)
		if (p->first == 1)
			p = pairs.erase(p);
		else
			p++;
	printp(pairs);

	pairs.insert( {1, 4});
	pairs.insert( {1, 4});
	printp(pairs);

	multimap<int, int> pairs2;
	pairs2.insert( {9, 1});
	pairs2.insert( {10, 1});
	pairs2.insert( {11, 1});

	for (auto p = pairs.begin(); p != pairs.end();)
		if (p->first > 2)
			p = pairs.erase(p);
		else
			p++;

	pairs.insert(pairs2.begin(), pairs2.end());
	printp(pairs);
	printp(pairs2);

	return 0;

}
