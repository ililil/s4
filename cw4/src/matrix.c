#include <stdlib.h>
#include <stdio.h>
#include "matrix.h"

DoubleMatrix double_matrix_new(unsigned int sizeA, unsigned int sizeB) {
	double **arr = calloc(sizeA, sizeof(double*));
	if (arr) {
		for (unsigned int i = 0; i < sizeA; i++) {
			arr[i] = calloc(sizeB, sizeof(double));
			if (!arr[i]) {
				arr = NULL;
				break;
			}
		}
	}

	DoubleMatrix matrix = {arr, sizeA, sizeB};
	return matrix;
}

void double_matrix_del(DoubleMatrix *matrix) {
	for (unsigned int i = 0; i < matrix->sizeA; i++)
		free(matrix->arr[i]);
	free(matrix->arr);
	matrix->arr = NULL;
}

void double_matrix_resizeA(DoubleMatrix *matrix, unsigned int newSizeA) {
	if (newSizeA < matrix->sizeA)
		for (unsigned int i = newSizeA; i < matrix->sizeA; i++)
			free(matrix->arr[i]);

	double **arr = realloc(matrix->arr, newSizeA * sizeof(double*));
	if (arr && newSizeA > matrix->sizeA) {
		for (unsigned int i = matrix->sizeA; i < newSizeA; i++) {
			arr[i] = calloc(matrix->sizeB, sizeof(double));
			if (!arr[i]) {
				arr = NULL;
				break;
			}
		}
	}
	matrix->arr = arr;
	matrix->sizeA = newSizeA;
}

DoubleMatrix double_matrix_mult(DoubleMatrix matrix1, DoubleMatrix matrix2) {
	DoubleMatrix matrix_res = double_matrix_new(matrix1.sizeA, matrix2.sizeB);
	if (!matrix_res.arr)
		return matrix_res;

	for (unsigned int a1 = 0; a1 < matrix1.sizeA; a1++) {
		for (unsigned int b2 = 0; b2 < matrix2.sizeB; b2++) {
			matrix_res.arr[a1][b2] = 0;
			for (unsigned int a2 = 0; a2 < matrix2.sizeA; a2++)
				matrix_res.arr[a1][b2] += matrix1.arr[a1][a2] * matrix2.arr[a2][b2];
		}
	}
	return matrix_res;
}

IntMatrixDynamic int_matrix_dynamic_new(unsigned int sizeA, unsigned int defaultSizeB) {
	int **arr = calloc(sizeA, sizeof(int*));
	unsigned int *sizesB = calloc(sizeA, sizeof(int));
	if (arr && sizesB) {
		for (unsigned int i = 0; i < sizeA; i++) {
			sizesB[i] = defaultSizeB;
			arr[i] = calloc(defaultSizeB, sizeof(int));
			if (!arr[i]) {
				arr = NULL;
				break;
			}
		}
	}

	IntMatrixDynamic matrix = {arr, sizeA, defaultSizeB, sizesB};
	return matrix;
}

void int_matrix_dynamic_del(IntMatrixDynamic *matrix) {
	for (unsigned int i = 0; i < matrix->sizeA; i++)
		free(matrix->arr[i]);
	free(matrix->arr);
	free(matrix->sizesB);

	matrix->arr = NULL;
	matrix->sizesB = NULL;
}

void int_matrix_dynamic_resizeA(IntMatrixDynamic *matrix, unsigned int newSizeA) {
	if (newSizeA < matrix->sizeA)
		for (unsigned int i = newSizeA; i < matrix->sizeA; i++)
			free(matrix->arr[i]);

	int **arr = realloc(matrix->arr, newSizeA * sizeof(int*));
	unsigned int *sizesB = realloc(matrix->sizesB, newSizeA * sizeof(int));

	if (arr && sizesB && newSizeA > matrix->sizeA) {
		for (unsigned int i = matrix->sizeA; i < newSizeA; i++) {
			sizesB[i] = matrix->defaultSizeB;
			arr[i] = calloc(matrix->defaultSizeB, sizeof(int));
			if (!arr[i]) {
				arr = NULL;
				break;
			}
		}
	}
	matrix->arr = arr;
	matrix->sizeA = newSizeA;
	matrix->sizesB = sizesB;
}

void int_matrix_dynamic_resizeB(IntMatrixDynamic *matrix, unsigned int b, unsigned int newSizeB) {
	int *arr_b = realloc(matrix->arr[b], newSizeB * sizeof(int));
	if (arr_b && newSizeB > matrix->sizesB[b])
		for (unsigned int i = matrix->sizesB[b]; i < newSizeB; i++)
			arr_b[i] = 0;
	matrix->arr[b] = arr_b;
	matrix->sizesB[b] = newSizeB;
}
