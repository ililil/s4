#define UNICODE
#define WIN32_LEAN_AND_MEAN // Exclude rarely-used stuff from Windows headers

#include <SDKDDKVer.h>
#include <windows.h>
#include "main.h"
#include "mdl_loader.h"

#define szTitle L"3d"
#define szWindowClass L"3d"
#define modelPath "tree.obj"

int windowWidth = 1200, windowHeight = 1000;

MDL model = {0};

double deltaX = 0;
double deltaY = 0;
double deltaZ = 0;

double scaleX = 30;
double scaleY = 30;
double scaleZ = 30;

double angleX = 90;
double angleY = -90;

double offsetX = 640;
double offsetY = 650;

double f = 1000;
double R = 1;

HINSTANCE hInst = NULL;

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
	WNDCLASSEXW wcex = {0};

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, IDI_WINLOGO);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	wcex.lpszMenuName = L"Menu";
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(hInstance, IDI_WINLOGO);

	RegisterClassEx(&wcex);
	hInst = hInstance;

	HWND hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, windowWidth, windowHeight, NULL, NULL, hInstance, NULL);
	if (!hWnd)
		return FALSE;

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	FILE *file = fopen(modelPath, "r");
	if (!file) {
		perror("");
		return 1;
	}
	model = readMDL(file);
	fclose(file);

	if (!InitInstance(hInstance, nCmdShow))
		return 1;

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int) msg.wParam;
}

DoubleMatrix gen_coordinates() {
	deltaX = windowWidth / 2;
	deltaY = windowHeight / 2;

	DoubleMatrix scale_matrix = gen_scale_matrix(scaleX, scaleY, scaleZ);
	DoubleMatrix view_matrix = gen_view_matrix(angleX, angleY, R);
	DoubleMatrix perspective_matrix = gen_perspective_matrix(deltaX, deltaY, deltaZ);

	DoubleMatrix points_scaled = double_matrix_mult(model.points, scale_matrix);
	DoubleMatrix points_view = double_matrix_mult(points_scaled, view_matrix);
	DoubleMatrix coordinates = double_matrix_mult(points_view, perspective_matrix);

	double_matrix_del(&scale_matrix);
	double_matrix_del(&view_matrix);
	double_matrix_del(&perspective_matrix);

	double_matrix_del(&points_scaled);
	double_matrix_del(&points_view);

	for (unsigned int i = 0; i < coordinates.sizeA; i++) {
		coordinates.arr[i][0] += offsetX;
		coordinates.arr[i][1] += offsetY;
	}

	return coordinates;
	for (unsigned int i = 0; i < coordinates.sizeA; i++) {
		coordinates.arr[i][0] = coordinates.arr[i][0] * f / (coordinates.arr[i][2] + f);
		coordinates.arr[i][1] = coordinates.arr[i][1] * f / (coordinates.arr[i][2] + f);
	}
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		case WM_COMMAND: {
			int wmId = LOWORD(wParam);
			switch (wmId) {
				default: {
					return DefWindowProc(hWnd, message, wParam, lParam);
				}
			}
			break;
		}

		case WM_PAINT: {
			RECT rect;
			GetWindowRect(hWnd, &rect);
			windowWidth = rect.right - rect.left;
			windowHeight = rect.bottom - rect.top;

			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);

			HBRUSH brush = CreateSolidBrush(RGB(0, 0, 0));
			SelectObject(hdc, brush);
			Rectangle(hdc, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));

			HPEN hPen = CreatePen(PS_SOLID, 1, RGB(0, 255, 255));
			SelectObject(hdc, hPen);

			DoubleMatrix matrix = gen_coordinates();
			IntMatrixDynamic surfaces = model.surfaces;
			for (unsigned int a = 0; a < surfaces.sizeA; a++) {
				int *surface = surfaces.arr[a];

				MoveToEx(hdc, matrix.arr[surface[0]][0], matrix.arr[surface[0]][1], NULL);
				for (unsigned int b = 0; b < surfaces.sizesB[a]; b++) {
					int x = matrix.arr[surface[b]][0];
					int y = matrix.arr[surface[b]][1];
					LineTo(hdc, x, y);
				}
				LineTo(hdc, matrix.arr[surface[0]][0], matrix.arr[surface[0]][1]);
			}

			EndPaint(hWnd, &ps);
			break;
		}

		case WM_KEYDOWN: {
			int scaleDelta = 7;
			int rotateDelta = 10;
			int offsetDelta = 20;

			if (wParam == VK_OEM_PLUS) {
				scaleX += scaleDelta;
				scaleY += scaleDelta;
				scaleZ += scaleDelta;
			} else if (wParam == VK_OEM_MINUS) {
				scaleX -= scaleDelta;
				scaleY -= scaleDelta;
				scaleZ -= scaleDelta;
			}

			if (wParam == 'W')
				offsetY -= offsetDelta;
			else if (wParam == 'S')
				offsetY += offsetDelta;
			else if (wParam == 'A')
				offsetX -= offsetDelta;
			else if (wParam == 'D')
				offsetX += offsetDelta;

			if (wParam == VK_LEFT)
				angleX -= rotateDelta;
			else if (wParam == VK_RIGHT)
				angleX += rotateDelta;
			else if (wParam == VK_UP)
				angleY -= rotateDelta;
			else if (wParam == VK_DOWN)
				angleY += rotateDelta;

			RedrawWindow(hWnd, NULL, NULL, RDW_ERASE | RDW_INVALIDATE);
			break;
		}

		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}

		default: {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	return 0;
}
