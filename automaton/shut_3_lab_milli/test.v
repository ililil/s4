// Verilog test fixture created from schematic D:\avto\3\l3d.sch - Fri May 24 14:55:08 2024

`timescale 1ns / 1ps

module l3d_l3d_sch_tb();

// Inputs
   reg X_INPUT;
   reg CLK;
   reg RST_INPUT;
   reg CE;

// Output
   wire Y;

// Bidirs

// Instantiate the UUT
   l3d UUT (
		.X_INPUT(X_INPUT), 
		.Y(Y), 
		.CLK(CLK), 
		.RST_INPUT(RST_INPUT), 
		.CE(CE)
   );
// Initialize Inputs
   `ifdef auto_init
       initial begin
		X_INPUT = 0;
		CLK = 0;
		RST_INPUT = 0;
		CE = 0;
   `endif
endmodule
