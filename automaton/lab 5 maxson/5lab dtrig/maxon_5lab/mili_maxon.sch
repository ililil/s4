VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL X1
        SIGNAL IN_X1
        SIGNAL X2
        SIGNAL IN_X2
        SIGNAL X3
        SIGNAL IN_X3
        SIGNAL S(0:3)
        SIGNAL X0
        SIGNAL IN_X0
        SIGNAL S(1)
        SIGNAL XLXN_548
        SIGNAL XLXN_561
        SIGNAL C
        SIGNAL XLXN_360
        SIGNAL S(0)
        SIGNAL RST
        SIGNAL XLXN_21
        SIGNAL XLXN_363
        SIGNAL XLXN_143
        SIGNAL XLXN_386
        SIGNAL S(2)
        SIGNAL S(3)
        SIGNAL XLXN_457
        SIGNAL Y0
        SIGNAL XLXN_844
        SIGNAL XLXN_934
        SIGNAL XLXN_935
        SIGNAL IN_RST
        SIGNAL IN_C
        SIGNAL X4
        SIGNAL IN_X4
        SIGNAL XLXN_957
        SIGNAL XLXN_958
        SIGNAL XLXN_960
        SIGNAL XLXN_961
        SIGNAL XLXN_975
        SIGNAL XLXN_976
        SIGNAL XLXN_977
        SIGNAL XLXN_523
        SIGNAL XLXN_61
        SIGNAL XLXN_985
        SIGNAL XLXN_999
        SIGNAL XLXN_1000
        SIGNAL XLXN_705
        SIGNAL Y2
        SIGNAL XLXN_799
        SIGNAL Y1
        SIGNAL XLXN_1078
        SIGNAL XLXN_1079
        SIGNAL XLXN_1080
        SIGNAL XLXN_1081
        SIGNAL XLXN_1082
        SIGNAL Y3
        SIGNAL XLXN_1096
        SIGNAL XLXN_1097
        SIGNAL XLXN_1098
        SIGNAL XLXN_1099
        SIGNAL XLXN_1100
        SIGNAL XLXN_1101
        SIGNAL XLXN_1102
        PORT Input IN_X1
        PORT Input IN_X2
        PORT Input IN_X3
        PORT Input IN_X0
        PORT Output Y0
        PORT Input IN_RST
        PORT Input IN_C
        PORT Input IN_X4
        PORT Output Y2
        PORT Output Y1
        PORT Output Y3
        BEGIN BLOCKDEF ibuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF or3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 72 -128 
            LINE N 0 -192 48 -192 
            LINE N 256 -128 192 -128 
            ARC N 28 -256 204 -80 112 -80 192 -128 
            ARC N -40 -184 72 -72 48 -80 48 -176 
            LINE N 48 -64 48 -80 
            LINE N 48 -192 48 -176 
            LINE N 112 -80 48 -80 
            ARC N 28 -176 204 0 192 -128 112 -176 
            LINE N 112 -176 48 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF buf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
            LINE N 64 0 128 -32 
            LINE N 128 -32 64 -64 
            LINE N 64 -64 64 0 
        END BLOCKDEF
        BEGIN BLOCKDEF fdc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -128 64 -128 
            LINE N 0 -32 64 -32 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            RECTANGLE N 64 -320 320 -64 
            LINE N 64 -112 80 -128 
            LINE N 80 -128 64 -144 
            LINE N 192 -64 192 -32 
            LINE N 192 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF ibufg
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF or5
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 48 -128 
            LINE N 0 -192 72 -192 
            LINE N 0 -256 48 -256 
            LINE N 0 -320 48 -320 
            LINE N 256 -192 192 -192 
            ARC N 28 -320 204 -144 112 -144 192 -192 
            LINE N 112 -240 48 -240 
            LINE N 112 -144 48 -144 
            LINE N 48 -64 48 -144 
            LINE N 48 -320 48 -240 
            ARC N 28 -240 204 -64 192 -192 112 -240 
            ARC N -40 -248 72 -136 48 -144 48 -240 
        END BLOCKDEF
        BEGIN BLOCKDEF or4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 48 -256 
            LINE N 256 -160 192 -160 
            ARC N 28 -208 204 -32 192 -160 112 -208 
            LINE N 112 -208 48 -208 
            LINE N 112 -112 48 -112 
            LINE N 48 -256 48 -208 
            LINE N 48 -64 48 -112 
            ARC N -40 -216 72 -104 48 -112 48 -208 
            ARC N 28 -288 204 -112 112 -112 192 -160 
        END BLOCKDEF
        BEGIN BLOCK XLXI_149 ibuf
            PIN I IN_X1
            PIN O X1
        END BLOCK
        BEGIN BLOCK XLXI_220 ibuf
            PIN I IN_X2
            PIN O X2
        END BLOCK
        BEGIN BLOCK XLXI_222 ibuf
            PIN I IN_X3
            PIN O X3
        END BLOCK
        BEGIN BLOCK XLXI_328 ibuf
            PIN I IN_X0
            PIN O X0
        END BLOCK
        BEGIN BLOCK XLXI_260 or3
            PIN I0 XLXN_935
            PIN I1 XLXN_934
            PIN I2 XLXN_548
            PIN O XLXN_561
        END BLOCK
        BEGIN BLOCK XLXI_153 buf
            PIN I XLXN_360
            PIN O S(0)
        END BLOCK
        BEGIN BLOCK FF1 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_561
            PIN Q XLXN_360
        END BLOCK
        BEGIN BLOCK XLXI_152 buf
            PIN I XLXN_363
            PIN O S(1)
        END BLOCK
        BEGIN BLOCK FF2 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_21
            PIN Q XLXN_363
        END BLOCK
        BEGIN BLOCK XLXI_151 buf
            PIN I XLXN_386
            PIN O S(2)
        END BLOCK
        BEGIN BLOCK FF3 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_143
            PIN Q XLXN_386
        END BLOCK
        BEGIN BLOCK XLXI_329 and2b1
            PIN I0 X1
            PIN I1 S(0)
            PIN O XLXN_548
        END BLOCK
        BEGIN BLOCK XLXI_183 obuf
            PIN I XLXN_457
            PIN O Y0
        END BLOCK
        BEGIN BLOCK XLXI_399 or3
            PIN I0 XLXN_1000
            PIN I1 XLXN_999
            PIN I2 XLXN_844
            PIN O XLXN_457
        END BLOCK
        BEGIN BLOCK XLXI_402 and2b1
            PIN I0 X1
            PIN I1 S(0)
            PIN O XLXN_844
        END BLOCK
        BEGIN BLOCK XLXI_330 and2
            PIN I0 X4
            PIN I1 S(3)
            PIN O XLXN_935
        END BLOCK
        BEGIN BLOCK XLXI_449 and3b1
            PIN I0 X2
            PIN I1 X3
            PIN I2 S(2)
            PIN O XLXN_934
        END BLOCK
        BEGIN BLOCK XLXI_218 ibuf
            PIN I IN_RST
            PIN O RST
        END BLOCK
        BEGIN BLOCK XLXI_150 ibufg
            PIN I IN_C
            PIN O C
        END BLOCK
        BEGIN BLOCK XLXI_452 ibuf
            PIN I IN_X4
            PIN O X4
        END BLOCK
        BEGIN BLOCK XLXI_251 and2b1
            PIN I0 X2
            PIN I1 S(1)
            PIN O XLXN_957
        END BLOCK
        BEGIN BLOCK XLXI_250 and2
            PIN I0 S(0)
            PIN I1 X1
            PIN O XLXN_958
        END BLOCK
        BEGIN BLOCK XLXI_254 and3b1
            PIN I0 X2
            PIN I1 S(2)
            PIN I2 X3
            PIN O XLXN_960
        END BLOCK
        BEGIN BLOCK XLXI_458 and2
            PIN I0 X1
            PIN I1 S(1)
            PIN O XLXN_961
        END BLOCK
        BEGIN BLOCK XLXI_459 or4
            PIN I0 XLXN_961
            PIN I1 XLXN_960
            PIN I2 XLXN_958
            PIN I3 XLXN_957
            PIN O XLXN_21
        END BLOCK
        BEGIN BLOCK XLXI_460 or3
            PIN I0 XLXN_977
            PIN I1 XLXN_976
            PIN I2 XLXN_975
            PIN O XLXN_143
        END BLOCK
        BEGIN BLOCK XLXI_343 and3b1
            PIN I0 X1
            PIN I1 X2
            PIN I2 S(1)
            PIN O XLXN_975
        END BLOCK
        BEGIN BLOCK XLXI_463 and2b1
            PIN I0 X3
            PIN I1 S(2)
            PIN O XLXN_976
        END BLOCK
        BEGIN BLOCK XLXI_255 and2
            PIN I0 S(2)
            PIN I1 X2
            PIN O XLXN_977
        END BLOCK
        BEGIN BLOCK XLXI_247 buf
            PIN I XLXN_523
            PIN O S(3)
        END BLOCK
        BEGIN BLOCK FF4 fdc
            PIN C C
            PIN CLR RST
            PIN D XLXN_61
            PIN Q XLXN_523
        END BLOCK
        BEGIN BLOCK XLXI_467 or2
            PIN I0 XLXN_985
            PIN I1 X0
            PIN O XLXN_61
        END BLOCK
        BEGIN BLOCK XLXI_468 and2b1
            PIN I0 X4
            PIN I1 S(3)
            PIN O XLXN_985
        END BLOCK
        BEGIN BLOCK XLXI_403 and2
            PIN I0 X4
            PIN I1 S(3)
            PIN O XLXN_1000
        END BLOCK
        BEGIN BLOCK XLXI_470 and3b1
            PIN I0 X2
            PIN I1 X3
            PIN I2 S(2)
            PIN O XLXN_999
        END BLOCK
        BEGIN BLOCK XLXI_326 obuf
            PIN I XLXN_705
            PIN O Y2
        END BLOCK
        BEGIN BLOCK XLXI_325 obuf
            PIN I XLXN_799
            PIN O Y1
        END BLOCK
        BEGIN BLOCK XLXI_445 and2
            PIN I0 X1
            PIN I1 S(0)
            PIN O XLXN_1101
        END BLOCK
        BEGIN BLOCK XLXI_484 and2b1
            PIN I0 X2
            PIN I1 S(1)
            PIN O XLXN_1100
        END BLOCK
        BEGIN BLOCK XLXI_418 and3b1
            PIN I0 X2
            PIN I1 X3
            PIN I2 S(2)
            PIN O XLXN_1102
        END BLOCK
        BEGIN BLOCK XLXI_490 or4
            PIN I0 XLXN_1081
            PIN I1 XLXN_1080
            PIN I2 XLXN_1079
            PIN I3 XLXN_1078
            PIN O XLXN_705
        END BLOCK
        BEGIN BLOCK XLXI_437 and3b1
            PIN I0 X2
            PIN I1 X3
            PIN I2 S(2)
            PIN O XLXN_1080
        END BLOCK
        BEGIN BLOCK XLXI_434 and2
            PIN I0 S(1)
            PIN I1 X1
            PIN O XLXN_1081
        END BLOCK
        BEGIN BLOCK XLXI_435 and2
            PIN I0 X1
            PIN I1 S(0)
            PIN O XLXN_1079
        END BLOCK
        BEGIN BLOCK XLXI_436 and2b1
            PIN I0 X2
            PIN I1 S(1)
            PIN O XLXN_1078
        END BLOCK
        BEGIN BLOCK XLXI_503 obuf
            PIN I XLXN_1082
            PIN O Y3
        END BLOCK
        BEGIN BLOCK XLXI_504 or5
            PIN I0 XLXN_1099
            PIN I1 X0
            PIN I2 XLXN_1098
            PIN I3 XLXN_1097
            PIN I4 XLXN_1096
            PIN O XLXN_1082
        END BLOCK
        BEGIN BLOCK XLXI_507 and3b1
            PIN I0 X1
            PIN I1 X2
            PIN I2 S(1)
            PIN O XLXN_1096
        END BLOCK
        BEGIN BLOCK XLXI_508 and2b1
            PIN I0 X3
            PIN I1 S(2)
            PIN O XLXN_1097
        END BLOCK
        BEGIN BLOCK XLXI_509 and2b1
            PIN I0 X4
            PIN I1 S(3)
            PIN O XLXN_1099
        END BLOCK
        BEGIN BLOCK XLXI_510 and2
            PIN I0 S(2)
            PIN I1 X2
            PIN O XLXN_1098
        END BLOCK
        BEGIN BLOCK XLXI_514 or4
            PIN I0 X0
            PIN I1 XLXN_1102
            PIN I2 XLXN_1101
            PIN I3 XLXN_1100
            PIN O XLXN_799
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 7040 5440
        BEGIN BRANCH X1
            WIRE 768 704 912 704
            WIRE 912 704 1056 704
            BEGIN DISPLAY 912 704 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_149 544 736 R0
        BEGIN BRANCH IN_X1
            WIRE 464 704 528 704
            WIRE 528 704 544 704
            BEGIN DISPLAY 528 704 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 768 880 912 880
            WIRE 912 880 1056 880
            BEGIN DISPLAY 912 880 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_220 544 912 R0
        BEGIN BRANCH IN_X2
            WIRE 464 880 528 880
            WIRE 528 880 544 880
            BEGIN DISPLAY 528 880 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X3
            WIRE 768 1056 912 1056
            WIRE 912 1056 1056 1056
            BEGIN DISPLAY 912 1056 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_222 544 1088 R0
        BEGIN BRANCH IN_X3
            WIRE 464 1056 528 1056
            WIRE 528 1056 544 1056
            BEGIN DISPLAY 528 1056 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0:3)
            WIRE 384 352 544 352
            WIRE 544 352 864 352
            BEGIN DISPLAY 544 352 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X0
            WIRE 768 560 864 560
            WIRE 864 560 1056 560
            BEGIN DISPLAY 864 560 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_328 544 592 R0
        BEGIN BRANCH IN_X0
            WIRE 464 560 528 560
            WIRE 528 560 544 560
            BEGIN DISPLAY 528 560 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 2704 160 2800 160
            BEGIN DISPLAY 2704 160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X1
            WIRE 2704 224 2800 224
            BEGIN DISPLAY 2704 224 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_260 3264 512 R0
        BEGIN BRANCH XLXN_548
            WIRE 3056 192 3264 192
            WIRE 3264 192 3264 320
        END BRANCH
        BEGIN BRANCH XLXN_561
            WIRE 3520 384 3904 384
        END BRANCH
        BEGIN BRANCH C
            WIRE 3776 512 3904 512
            BEGIN DISPLAY 3776 512 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_360
            WIRE 4288 384 4432 384
        END BRANCH
        INSTANCE XLXI_153 4432 416 R0
        BEGIN BRANCH S(0)
            WIRE 4656 384 4800 384
            WIRE 4800 384 4864 384
            BEGIN DISPLAY 4800 384 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE FF1 3904 640 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 3632 608 3904 608
            BEGIN DISPLAY 3632 608 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_21
            WIRE 3504 1008 3904 1008
        END BRANCH
        BEGIN BRANCH C
            WIRE 3776 1136 3904 1136
            BEGIN DISPLAY 3776 1136 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_152 4432 1040 R0
        BEGIN BRANCH XLXN_363
            WIRE 4288 1008 4432 1008
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 4656 1008 4752 1008
            WIRE 4752 1008 4864 1008
            BEGIN DISPLAY 4752 1008 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE FF2 3904 1264 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 3664 1232 3904 1232
            BEGIN DISPLAY 3664 1232 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_143
            WIRE 3536 1568 3904 1568
        END BRANCH
        BEGIN BRANCH C
            WIRE 3760 1696 3904 1696
            BEGIN DISPLAY 3760 1696 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_151 4384 1600 R0
        BEGIN BRANCH XLXN_386
            WIRE 4288 1568 4384 1568
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 4608 1568 4704 1568
            WIRE 4704 1568 4864 1568
            BEGIN DISPLAY 4704 1568 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE FF3 3904 1824 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 3680 1792 3904 1792
            BEGIN DISPLAY 3680 1792 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_329 2800 288 R0
        INSTANCE XLXI_183 1424 1712 R0
        BEGIN BRANCH XLXN_457
            WIRE 1248 1680 1424 1680
        END BRANCH
        BEGIN BRANCH Y0
            WIRE 1648 1680 1680 1680
        END BRANCH
        INSTANCE XLXI_399 992 1808 R0
        INSTANCE XLXI_402 496 1584 R0
        BEGIN BRANCH XLXN_844
            WIRE 752 1488 992 1488
            WIRE 992 1488 992 1616
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 368 1456 496 1456
            BEGIN DISPLAY 368 1456 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X1
            WIRE 368 1520 496 1520
            BEGIN DISPLAY 368 1520 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        IOMARKER 464 704 IN_X1 R180 28
        IOMARKER 464 880 IN_X2 R180 28
        IOMARKER 464 1056 IN_X3 R180 28
        IOMARKER 464 560 IN_X0 R180 28
        IOMARKER 1680 1680 Y0 R0 28
        BEGIN BRANCH X3
            WIRE 2704 384 2800 384
            BEGIN DISPLAY 2704 384 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 2704 448 2800 448
            BEGIN DISPLAY 2704 448 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 2704 320 2800 320
            BEGIN DISPLAY 2704 320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_330 2800 656 R0
        BEGIN BRANCH S(3)
            WIRE 2704 528 2800 528
            BEGIN DISPLAY 2704 528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X4
            WIRE 2704 592 2800 592
            BEGIN DISPLAY 2704 592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_934
            WIRE 3056 384 3264 384
        END BRANCH
        BEGIN BRANCH XLXN_935
            WIRE 3056 560 3264 560
            WIRE 3264 448 3264 560
        END BRANCH
        INSTANCE XLXI_449 2800 512 R0
        INSTANCE XLXI_218 544 1280 R0
        BEGIN BRANCH IN_RST
            WIRE 480 1248 544 1248
        END BRANCH
        BEGIN BRANCH RST
            WIRE 768 1248 912 1248
            WIRE 912 1248 1056 1248
            BEGIN DISPLAY 912 1248 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_150 544 1408 R0
        BEGIN BRANCH C
            WIRE 768 1376 992 1376
            WIRE 992 1376 992 1376
            WIRE 992 1376 1056 1376
            BEGIN DISPLAY 996 1376 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH IN_C
            WIRE 448 1376 544 1376
        END BRANCH
        IOMARKER 480 1248 IN_RST R180 28
        IOMARKER 448 1376 IN_C R180 28
        BEGIN BRANCH X4
            WIRE 768 1152 912 1152
            WIRE 912 1152 1056 1152
            BEGIN DISPLAY 912 1152 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_452 544 1184 R0
        BEGIN BRANCH IN_X4
            WIRE 464 1152 528 1152
            WIRE 528 1152 544 1152
            BEGIN DISPLAY 528 1152 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        IOMARKER 464 1152 IN_X4 R180 28
        INSTANCE XLXI_251 2800 848 R0
        BEGIN BRANCH S(1)
            WIRE 2704 720 2800 720
            BEGIN DISPLAY 2704 720 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 2704 784 2800 784
            BEGIN DISPLAY 2704 784 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_250 2800 1008 R0
        BEGIN BRANCH X1
            WIRE 2704 880 2800 880
            BEGIN DISPLAY 2704 880 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 2704 944 2800 944
            BEGIN DISPLAY 2704 944 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 2704 1088 2800 1088
            BEGIN DISPLAY 2704 1088 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 2704 1152 2800 1152
            BEGIN DISPLAY 2704 1152 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X3
            WIRE 2704 1024 2800 1024
            BEGIN DISPLAY 2704 1024 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_254 2800 1216 R0
        INSTANCE XLXI_458 2800 1360 R0
        BEGIN BRANCH S(1)
            WIRE 2704 1232 2800 1232
            BEGIN DISPLAY 2704 1232 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X1
            WIRE 2704 1296 2800 1296
            BEGIN DISPLAY 2704 1296 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_459 3248 1168 R0
        BEGIN BRANCH XLXN_957
            WIRE 3056 752 3248 752
            WIRE 3248 752 3248 912
        END BRANCH
        BEGIN BRANCH XLXN_958
            WIRE 3056 912 3152 912
            WIRE 3152 912 3152 976
            WIRE 3152 976 3248 976
        END BRANCH
        BEGIN BRANCH XLXN_960
            WIRE 3056 1088 3152 1088
            WIRE 3152 1040 3152 1088
            WIRE 3152 1040 3248 1040
        END BRANCH
        BEGIN BRANCH XLXN_961
            WIRE 3056 1264 3248 1264
            WIRE 3248 1104 3248 1264
        END BRANCH
        INSTANCE XLXI_460 3280 1696 R0
        BEGIN BRANCH X2
            WIRE 2672 1472 2768 1472
            BEGIN DISPLAY 2672 1472 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X1
            WIRE 2672 1536 2768 1536
            BEGIN DISPLAY 2672 1536 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 2672 1408 2768 1408
            BEGIN DISPLAY 2672 1408 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_343 2768 1600 R0
        INSTANCE XLXI_463 2768 1760 R0
        INSTANCE XLXI_255 2768 1920 R0
        BEGIN BRANCH X2
            WIRE 2672 1792 2768 1792
            BEGIN DISPLAY 2672 1792 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 2672 1856 2768 1856
            BEGIN DISPLAY 2672 1856 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 2672 1632 2768 1632
            BEGIN DISPLAY 2672 1632 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X3
            WIRE 2672 1696 2768 1696
            BEGIN DISPLAY 2672 1696 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_975
            WIRE 3024 1472 3280 1472
            WIRE 3280 1472 3280 1504
        END BRANCH
        BEGIN BRANCH XLXN_976
            WIRE 3024 1664 3152 1664
            WIRE 3152 1568 3152 1664
            WIRE 3152 1568 3280 1568
        END BRANCH
        BEGIN BRANCH XLXN_977
            WIRE 3024 1824 3280 1824
            WIRE 3280 1632 3280 1824
        END BRANCH
        BEGIN BRANCH C
            WIRE 3760 2240 3904 2240
            BEGIN DISPLAY 3760 2240 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_247 4384 2144 R0
        BEGIN BRANCH XLXN_523
            WIRE 4288 2112 4384 2112
        END BRANCH
        BEGIN BRANCH S(3)
            WIRE 4608 2112 4704 2112
            WIRE 4704 2112 4864 2112
            BEGIN DISPLAY 4704 2112 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE FF4 3904 2368 R0
            BEGIN DISPLAY 0 -412 ATTR InstName
                FONT 28 "Arial"
            END DISPLAY
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 3680 2336 3904 2336
            BEGIN DISPLAY 3680 2336 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_61
            WIRE 3456 2112 3904 2112
        END BRANCH
        INSTANCE XLXI_467 3200 2208 R0
        BEGIN BRANCH X0
            WIRE 3104 2080 3200 2080
            BEGIN DISPLAY 3104 2080 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_468 2864 2272 R0
        BEGIN BRANCH XLXN_985
            WIRE 3120 2176 3152 2176
            WIRE 3152 2144 3152 2176
            WIRE 3152 2144 3200 2144
        END BRANCH
        BEGIN BRANCH S(3)
            WIRE 2768 2144 2864 2144
            BEGIN DISPLAY 2768 2144 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X4
            WIRE 2768 2208 2864 2208
            BEGIN DISPLAY 2768 2208 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_403 496 1952 R0
        BEGIN BRANCH S(3)
            WIRE 368 1824 496 1824
            BEGIN DISPLAY 368 1824 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X4
            WIRE 368 1888 496 1888
            BEGIN DISPLAY 368 1888 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 368 1616 496 1616
            BEGIN DISPLAY 368 1616 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X3
            WIRE 368 1680 496 1680
            BEGIN DISPLAY 368 1680 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 368 1744 496 1744
            BEGIN DISPLAY 368 1744 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_470 496 1808 R0
        BEGIN BRANCH XLXN_999
            WIRE 752 1680 992 1680
        END BRANCH
        BEGIN BRANCH XLXN_1000
            WIRE 752 1856 992 1856
            WIRE 992 1744 992 1856
        END BRANCH
        INSTANCE XLXI_326 1632 3504 R0
        BEGIN BRANCH XLXN_705
            WIRE 1456 3472 1632 3472
        END BRANCH
        BEGIN BRANCH Y2
            WIRE 1856 3472 1888 3472
        END BRANCH
        IOMARKER 1888 3472 Y2 R0 28
        INSTANCE XLXI_325 1440 2496 R0
        BEGIN BRANCH XLXN_799
            WIRE 1136 2464 1152 2464
            WIRE 1152 2464 1168 2464
            WIRE 1168 2464 1440 2464
        END BRANCH
        BEGIN BRANCH Y1
            WIRE 1664 2464 1696 2464
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 368 2256 496 2256
            BEGIN DISPLAY 368 2256 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X1
            WIRE 368 2320 496 2320
            BEGIN DISPLAY 368 2320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_445 496 2384 R0
        IOMARKER 1696 2464 Y1 R0 28
        INSTANCE XLXI_484 496 2208 R0
        BEGIN BRANCH S(1)
            WIRE 368 2080 496 2080
            BEGIN DISPLAY 368 2080 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 368 2144 496 2144
            BEGIN DISPLAY 368 2144 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 368 2400 496 2400
            BEGIN DISPLAY 368 2400 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X3
            WIRE 368 2464 496 2464
            BEGIN DISPLAY 368 2464 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 368 2528 496 2528
            BEGIN DISPLAY 368 2528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_418 496 2592 R0
        BEGIN BRANCH X0
            WIRE 432 2800 848 2800
            WIRE 848 2560 880 2560
            WIRE 848 2560 848 2800
            BEGIN DISPLAY 432 2800 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_490 1200 3632 R0
        INSTANCE XLXI_437 656 3680 R0
        BEGIN BRANCH S(2)
            WIRE 528 3488 656 3488
            BEGIN DISPLAY 528 3488 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X3
            WIRE 528 3552 656 3552
            BEGIN DISPLAY 528 3552 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 528 3616 656 3616
            BEGIN DISPLAY 528 3616 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_434 656 3824 R0
        BEGIN BRANCH X1
            WIRE 528 3696 656 3696
            BEGIN DISPLAY 528 3696 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 528 3760 656 3760
            BEGIN DISPLAY 528 3760 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_435 656 3488 R0
        BEGIN BRANCH S(0)
            WIRE 528 3360 656 3360
            BEGIN DISPLAY 528 3360 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X1
            WIRE 528 3424 656 3424
            BEGIN DISPLAY 528 3424 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_436 656 3360 R0
        BEGIN BRANCH S(1)
            WIRE 528 3232 656 3232
            BEGIN DISPLAY 528 3232 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 528 3296 656 3296
            BEGIN DISPLAY 528 3296 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_1078
            WIRE 912 3264 1200 3264
            WIRE 1200 3264 1200 3376
        END BRANCH
        BEGIN BRANCH XLXN_1079
            WIRE 912 3392 1056 3392
            WIRE 1056 3392 1056 3440
            WIRE 1056 3440 1200 3440
        END BRANCH
        BEGIN BRANCH XLXN_1080
            WIRE 912 3552 1056 3552
            WIRE 1056 3504 1056 3552
            WIRE 1056 3504 1200 3504
        END BRANCH
        BEGIN BRANCH XLXN_1081
            WIRE 912 3728 1200 3728
            WIRE 1200 3568 1200 3728
        END BRANCH
        INSTANCE XLXI_503 1712 4224 R0
        BEGIN BRANCH XLXN_1082
            WIRE 1536 4192 1712 4192
        END BRANCH
        BEGIN BRANCH Y3
            WIRE 1936 4192 1968 4192
        END BRANCH
        INSTANCE XLXI_504 1280 4384 R0
        IOMARKER 1968 4192 Y3 R0 28
        INSTANCE XLXI_507 656 4096 R0
        BEGIN BRANCH S(1)
            WIRE 528 3904 656 3904
            BEGIN DISPLAY 528 3904 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X2
            WIRE 528 3968 656 3968
            BEGIN DISPLAY 528 3968 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X1
            WIRE 528 4032 656 4032
            BEGIN DISPLAY 528 4032 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_508 656 4272 R0
        BEGIN BRANCH S(2)
            WIRE 528 4144 656 4144
            BEGIN DISPLAY 528 4144 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X3
            WIRE 528 4208 656 4208
            BEGIN DISPLAY 528 4208 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_509 656 4704 R0
        BEGIN BRANCH S(3)
            WIRE 528 4576 656 4576
            BEGIN DISPLAY 528 4576 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X4
            WIRE 528 4640 656 4640
            BEGIN DISPLAY 528 4640 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_510 656 4448 R0
        BEGIN BRANCH X2
            WIRE 528 4320 656 4320
            BEGIN DISPLAY 528 4320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 528 4384 656 4384
            BEGIN DISPLAY 528 4384 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X0
            WIRE 592 4496 1152 4496
            WIRE 1152 4256 1280 4256
            WIRE 1152 4256 1152 4496
            BEGIN DISPLAY 592 4496 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_1096
            WIRE 912 3968 1280 3968
            WIRE 1280 3968 1280 4064
        END BRANCH
        BEGIN BRANCH XLXN_1097
            WIRE 912 4176 1088 4176
            WIRE 1088 4128 1088 4176
            WIRE 1088 4128 1280 4128
        END BRANCH
        BEGIN BRANCH XLXN_1098
            WIRE 912 4352 1088 4352
            WIRE 1088 4192 1088 4352
            WIRE 1088 4192 1280 4192
        END BRANCH
        BEGIN BRANCH XLXN_1099
            WIRE 912 4608 1280 4608
            WIRE 1280 4320 1280 4608
        END BRANCH
        INSTANCE XLXI_514 880 2624 R0
        BEGIN BRANCH XLXN_1100
            WIRE 752 2112 880 2112
            WIRE 880 2112 880 2368
        END BRANCH
        BEGIN BRANCH XLXN_1101
            WIRE 752 2288 816 2288
            WIRE 816 2288 816 2432
            WIRE 816 2432 880 2432
        END BRANCH
        BEGIN BRANCH XLXN_1102
            WIRE 752 2464 816 2464
            WIRE 816 2464 816 2496
            WIRE 816 2496 880 2496
        END BRANCH
    END SHEET
END SCHEMATIC
