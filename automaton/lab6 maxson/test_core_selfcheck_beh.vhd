--------------------------------------------------------------------------------
-- Copyright (c) 1995-2003 Xilinx, Inc.
-- All Right Reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 9.1i
--  \   \         Application : ISE
--  /   /         Filename : test_core_selfcheck.vhw
-- /___/   /\     Timestamp : Fri May 24 14:27:23 2024
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: 
--Design Name: test_core_selfcheck_beh
--Device: Xilinx
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;

ENTITY test_core_selfcheck_beh IS
END test_core_selfcheck_beh;

ARCHITECTURE testbench_arch OF test_core_selfcheck_beh IS
    COMPONENT core
        PORT (
            clk : In std_logic;
            x : In std_logic_vector (3 DownTo 0);
            y : Out std_logic_vector (3 DownTo 0);
            ye : Out std_logic_vector (2 DownTo 1);
            yk : Out std_logic_vector (3 DownTo 1);
            yf6 : Out std_logic
        );
    END COMPONENT;

    SIGNAL clk : std_logic := '0';
    SIGNAL x : std_logic_vector (3 DownTo 0) := "0000";
    SIGNAL y : std_logic_vector (3 DownTo 0) := "UUUU";
    SIGNAL ye : std_logic_vector (2 DownTo 1) := "UU";
    SIGNAL yk : std_logic_vector (3 DownTo 1) := "UUU";
    SIGNAL yf6 : std_logic := 'U';

    SHARED VARIABLE TX_ERROR : INTEGER := 0;
    SHARED VARIABLE TX_OUT : LINE;

    constant PERIOD : time := 100 ns;
    constant DUTY_CYCLE : real := 0.5;
    constant OFFSET : time := 0 ns;

    BEGIN
        UUT : core
        PORT MAP (
            clk => clk,
            x => x,
            y => y,
            ye => ye,
            yk => yk,
            yf6 => yf6
        );

        PROCESS    -- clock process for clk
        BEGIN
            WAIT for OFFSET;
            CLOCK_LOOP : LOOP
                clk <= '0';
                WAIT FOR (PERIOD - (PERIOD * DUTY_CYCLE));
                clk <= '1';
                WAIT FOR (PERIOD * DUTY_CYCLE);
            END LOOP CLOCK_LOOP;
        END PROCESS;

        PROCESS
            PROCEDURE CHECK_y(
                next_y : std_logic_vector (3 DownTo 0);
                TX_TIME : INTEGER
            ) IS
                VARIABLE TX_STR : String(1 to 4096);
                VARIABLE TX_LOC : LINE;
                BEGIN
                IF (y /= next_y) THEN
                    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
                    STD.TEXTIO.write(TX_LOC, TX_TIME);
                    STD.TEXTIO.write(TX_LOC, string'("ns y="));
                    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, y);
                    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
                    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, next_y);
                    STD.TEXTIO.write(TX_LOC, string'(" "));
                    TX_STR(TX_LOC.all'range) := TX_LOC.all;
                    STD.TEXTIO.Deallocate(TX_LOC);
                    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
                    TX_ERROR := TX_ERROR + 1;
                END IF;
            END;
            PROCEDURE CHECK_ye(
                next_ye : std_logic_vector (2 DownTo 1);
                TX_TIME : INTEGER
            ) IS
                VARIABLE TX_STR : String(1 to 4096);
                VARIABLE TX_LOC : LINE;
                BEGIN
                IF (ye /= next_ye) THEN
                    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
                    STD.TEXTIO.write(TX_LOC, TX_TIME);
                    STD.TEXTIO.write(TX_LOC, string'("ns ye="));
                    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, ye);
                    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
                    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, next_ye);
                    STD.TEXTIO.write(TX_LOC, string'(" "));
                    TX_STR(TX_LOC.all'range) := TX_LOC.all;
                    STD.TEXTIO.Deallocate(TX_LOC);
                    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
                    TX_ERROR := TX_ERROR + 1;
                END IF;
            END;
            PROCEDURE CHECK_yf6(
                next_yf6 : std_logic;
                TX_TIME : INTEGER
            ) IS
                VARIABLE TX_STR : String(1 to 4096);
                VARIABLE TX_LOC : LINE;
                BEGIN
                IF (yf6 /= next_yf6) THEN
                    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
                    STD.TEXTIO.write(TX_LOC, TX_TIME);
                    STD.TEXTIO.write(TX_LOC, string'("ns yf6="));
                    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, yf6);
                    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
                    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, next_yf6);
                    STD.TEXTIO.write(TX_LOC, string'(" "));
                    TX_STR(TX_LOC.all'range) := TX_LOC.all;
                    STD.TEXTIO.Deallocate(TX_LOC);
                    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
                    TX_ERROR := TX_ERROR + 1;
                END IF;
            END;
            PROCEDURE CHECK_yk(
                next_yk : std_logic_vector (3 DownTo 1);
                TX_TIME : INTEGER
            ) IS
                VARIABLE TX_STR : String(1 to 4096);
                VARIABLE TX_LOC : LINE;
                BEGIN
                IF (yk /= next_yk) THEN
                    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
                    STD.TEXTIO.write(TX_LOC, TX_TIME);
                    STD.TEXTIO.write(TX_LOC, string'("ns yk="));
                    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, yk);
                    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
                    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, next_yk);
                    STD.TEXTIO.write(TX_LOC, string'(" "));
                    TX_STR(TX_LOC.all'range) := TX_LOC.all;
                    STD.TEXTIO.Deallocate(TX_LOC);
                    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
                    TX_ERROR := TX_ERROR + 1;
                END IF;
            END;
            BEGIN
                -- -------------  Current Time:  35ns
                WAIT FOR 35 ns;
                x <= "0001";
                -- -------------------------------------
                -- -------------  Current Time:  65ns
                WAIT FOR 30 ns;
                CHECK_y("0000", 65);
                -- -------------------------------------
                -- -------------  Current Time:  165ns
                WAIT FOR 100 ns;
                CHECK_ye("0U", 165);
                CHECK_y("0001", 165);
                CHECK_yk("000", 165);
                -- -------------------------------------
                -- -------------  Current Time:  235ns
                WAIT FOR 70 ns;
                x <= "0100";
                -- -------------------------------------
                -- -------------  Current Time:  265ns
                WAIT FOR 30 ns;
                CHECK_ye("01", 265);
                CHECK_y("1111", 265);
                -- -------------------------------------
                -- -------------  Current Time:  365ns
                WAIT FOR 100 ns;
                CHECK_ye("10", 365);
                CHECK_y("1110", 365);
                -- -------------------------------------
                -- -------------  Current Time:  435ns
                WAIT FOR 70 ns;
                x <= "1100";
                -- -------------------------------------
                -- -------------  Current Time:  465ns
                WAIT FOR 30 ns;
                CHECK_y("1010", 465);
                CHECK_yk("101", 465);
                -- -------------------------------------
                -- -------------  Current Time:  565ns
                WAIT FOR 100 ns;
                CHECK_ye("00", 565);
                CHECK_y("1110", 565);
                -- -------------------------------------
                -- -------------  Current Time:  635ns
                WAIT FOR 70 ns;
                x <= "0000";
                -- -------------------------------------
                -- -------------  Current Time:  665ns
                WAIT FOR 30 ns;
                CHECK_y("0100", 665);
                CHECK_yk("111", 665);
                -- -------------------------------------
                -- -------------  Current Time:  765ns
                WAIT FOR 100 ns;
                CHECK_y("0000", 765);
                -- -------------------------------------
                -- -------------  Current Time:  865ns
                WAIT FOR 100 ns;
                CHECK_y("0001", 865);
                CHECK_yk("000", 865);
                -- -------------------------------------
                WAIT FOR 235 ns;

                IF (TX_ERROR = 0) THEN
                    STD.TEXTIO.write(TX_OUT, string'("No errors or warnings"));
                    ASSERT (FALSE) REPORT
                      "Simulation successful (not a failure).  No problems detected."
                      SEVERITY FAILURE;
                ELSE
                    STD.TEXTIO.write(TX_OUT, TX_ERROR);
                    STD.TEXTIO.write(TX_OUT,
                        string'(" errors found in simulation"));
                    ASSERT (FALSE) REPORT "Errors found during simulation"
                         SEVERITY FAILURE;
                END IF;
            END PROCESS;

    END testbench_arch;

