#ifndef MDL_LOADER
#define MDL_LOADER

#include <stdio.h>
#include "matrix.h"

typedef struct MDL {
	struct IntMatrixDynamic surfaces;
	struct DoubleMatrix points;
} MDL;

void strreplace(char *string, char from, char to);

unsigned int countLines(FILE *file);

MDL readMDL(FILE *file);

#endif
