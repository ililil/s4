#include <iostream>
#include <string>

using namespace std;

template<class T> class MySet {

public:

	T *arr;
	int ind;
	int size_;

	MySet<T>(unsigned int size) {
		if (size > 32768)
			throw runtime_error("size > 32768");

		arr = new T[size] {(T) NULL};
		ind = 0;
		size_ = size;
	}

	T& operator[](unsigned int index) {
		if (index >= size_)
			throw runtime_error("index out of range");

		return arr[index];
	}

	MySet<T> operator-(const T &e) {
		if (arr[0] == NULL)
			throw runtime_error("MySet is empty");

		for (int i = 0; i < size_; i++)
			if (this->arr[i] == e)
				arr[i] = (T) NULL;

		return *this;
	}

	bool operator>(const MySet &e) {
		T *same = new T[size_];
		int is = 0;

		for (int i2 = 0; i2 < size_; i2++) {
			for (int i = 0; i < size_; i++) {
				if (arr[i2] == e.arr[i] && arr[i2] != (T) NULL) {
					same[is] = arr[i2];
					is++;
				}
			}
		}
		return is >= e.ind;
	}

	bool operator!=(const MySet &ms) {
		T *same = new T[size_];
		int is = 0;

		for (int i2 = 0; i2 < size_; i2++) {
			for (int i = 0; i < size_; i++) {
				if (arr[i2] == ms.arr[i] && arr[i2] != (T) NULL) {
					same[is] = arr[i2];
					is++;
				}
			}
		}
		return is != ind;
	}

	void print() {
		for (int i = 0; i < size_; i++)
			if (arr[i] != (T) NULL)
				cout << arr[i] << " ";
	}

	void add(T t) {
		if (ind + 1 > size_)
			throw runtime_error("Maximum size reached");

		for (int i = 0; i < size_; i++)
			if (arr[i] == t)
				return;

		arr[ind] = t;
		ind++;
	}
};

int main() {

	MySet<int> set12 = MySet<int>(3);
	set12.add(1);
	set12.add(2);

	cout << "set12 = ";
	set12.print();
	cout << endl;

	MySet<int> set123 = MySet<int>(3);
	set123.add(1);
	set123.add(2);
	set123.add(3);

	cout << "set123 = ";
	set123.print();
	cout << endl;

	cout << endl << "set123 > set12 = " << (set123 > set12) << endl;

	MySet<int> set123_2 = MySet<int>(3);
	set123_2.add(1);
	set123_2.add(2);
	set123_2.add(3);

	cout << "set123 != set123_2 = " << (set123 != set123_2) << endl;
	cout << "set123 != set12 = " << (set123 != set12) << endl;

	set123 - 1;
	cout << endl << "set123 = ";
	set123.print();
	cout << endl << endl;
	;

	try {
		set123.add(1);
	} catch (exception &e) {
		cout << "Exception: " << e.what() << endl;
	}

	try {
		set123[3];
	} catch (exception &e) {
		cout << "Exception: " << e.what() << endl;
	}

	try {
		MySet<int> set123_2 = MySet<int>(55555);
	} catch (exception &e) {
		cout << "Exception: " << e.what() << endl;
	}

	try {
		MySet<int> set123_2 = MySet<int>(1);
		set123_2 - 1;
	} catch (exception &e) {
		cout << "Exception: " << e.what() << endl << endl;
	}

	string inn;
	cout << "Number in base 11: ";
	cin >> inn;
	try {
		int x = stoi(inn, nullptr, 11);
		char res[32];
		itoa(x, res, 9);
		cout << "In base 9: " << res << endl;
	} catch (exception &e) {
		cout << "Exception: " << e.what() << endl;
	}
	return 0;
}
