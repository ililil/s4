VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL S1in
        SIGNAL Q3
        SIGNAL Q4
        SIGNAL X
        SIGNAL Q2
        SIGNAL Q1
        SIGNAL S2in
        SIGNAL S3in
        SIGNAL R2in
        SIGNAL R3in
        SIGNAL R1in
        BEGIN SIGNAL CLK
        END SIGNAL
        SIGNAL RST
        SIGNAL XLXN_1
        SIGNAL XLXN_2
        SIGNAL XLXN_46
        SIGNAL XLXN_45
        SIGNAL XLXN_14
        SIGNAL XLXN_15
        SIGNAL XLXN_60
        SIGNAL XLXN_61
        SIGNAL XLXN_66
        SIGNAL XLXN_67
        SIGNAL XLXN_28
        SIGNAL XLXN_29
        SIGNAL NOT_Q3
        SIGNAL XLXN_36
        SIGNAL XLXN_39
        SIGNAL S4in
        SIGNAL XLXN_73
        SIGNAL XLXN_74
        SIGNAL XLXN_75
        SIGNAL R4in
        SIGNAL NOT_Q4
        SIGNAL NOT_Q2
        SIGNAL NOT_Q1
        SIGNAL X_INPUT
        SIGNAL Y
        SIGNAL Yin
        SIGNAL RST_INPUT
        SIGNAL IN_CLK
        PORT Output NOT_Q3
        PORT Output NOT_Q4
        PORT Output NOT_Q2
        PORT Output NOT_Q1
        PORT Input X_INPUT
        PORT Output Y
        PORT Input RST_INPUT
        PORT Input IN_CLK
        BEGIN BLOCKDEF and4b2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -208 144 -208 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
        END BLOCKDEF
        BEGIN BLOCKDEF and5b4
            TIMESTAMP 2000 1 1 10 10 10
            ARC N 96 -240 192 -144 144 -144 144 -240 
            LINE N 64 -240 144 -240 
            LINE N 144 -144 64 -144 
            LINE N 64 -64 64 -320 
            LINE N 256 -192 192 -192 
            LINE N 0 -320 64 -320 
            LINE N 0 -256 40 -256 
            CIRCLE N 40 -268 64 -244 
            LINE N 0 -192 40 -192 
            CIRCLE N 40 -204 64 -180 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
            LINE N 64 -176 144 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF and4b3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 40 -192 
            CIRCLE N 40 -204 64 -180 
            LINE N 0 -256 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 64 -64 64 -256 
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
        END BLOCKDEF
        BEGIN BLOCKDEF and3b3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
            LINE N 0 -128 40 -128 
            CIRCLE N 40 -140 64 -116 
            LINE N 0 -192 40 -192 
            CIRCLE N 40 -204 64 -180 
            LINE N 256 -128 192 -128 
            LINE N 64 -176 144 -176 
            LINE N 64 -64 64 -192 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 144 -80 64 -80 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF and4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
            LINE N 64 -64 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 0 -256 64 -256 
            LINE N 0 -192 64 -192 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 64 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF and3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 256 -128 192 -128 
            LINE N 64 -176 144 -176 
            LINE N 144 -80 64 -80 
            ARC N 96 -176 192 -80 144 -80 144 -176 
            LINE N 64 -64 64 -192 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCKDEF or4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 48 -256 
            LINE N 256 -160 192 -160 
            ARC N 28 -208 204 -32 192 -160 112 -208 
            LINE N 112 -208 48 -208 
            LINE N 112 -112 48 -112 
            LINE N 48 -256 48 -208 
            LINE N 48 -64 48 -112 
            ARC N -40 -216 72 -104 48 -112 48 -208 
            ARC N 28 -288 204 -112 112 -112 192 -160 
        END BLOCKDEF
        BEGIN BLOCKDEF or3
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 72 -128 
            LINE N 0 -192 48 -192 
            LINE N 256 -128 192 -128 
            ARC N 28 -256 204 -80 112 -80 192 -128 
            ARC N -40 -184 72 -72 48 -80 48 -176 
            LINE N 48 -64 48 -80 
            LINE N 48 -192 48 -176 
            LINE N 112 -80 48 -80 
            ARC N 28 -176 204 0 192 -128 112 -176 
            LINE N 112 -176 48 -176 
        END BLOCKDEF
        BEGIN BLOCKDEF ibuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF Flip_Flop_RS
            TIMESTAMP 2024 5 2 19 51 3
            RECTANGLE N 64 -256 320 0 
            LINE N 64 -224 0 -224 
            LINE N 64 -160 0 -160 
            LINE N 64 -96 0 -96 
            LINE N 64 -32 0 -32 
            LINE N 320 -224 384 -224 
            LINE N 320 -32 384 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF ibufg
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 and4b2
            PIN I0 Q2
            PIN I1 X
            PIN I2 Q4
            PIN I3 Q3
            PIN O XLXN_1
        END BLOCK
        BEGIN BLOCK XLXI_3 and5b4
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q2
            PIN I3 Q1
            PIN I4 X
            PIN O XLXN_2
        END BLOCK
        BEGIN BLOCK XLXI_28 or2
            PIN I0 XLXN_2
            PIN I1 XLXN_1
            PIN O S1in
        END BLOCK
        BEGIN BLOCK XLXI_35 ibuf
            PIN I Q1
            PIN O R1in
        END BLOCK
        BEGIN BLOCK XLXI_10 and2b1
            PIN I0 X
            PIN I1 Q3
            PIN O XLXN_46
        END BLOCK
        BEGIN BLOCK XLXI_32 or2
            PIN I0 XLXN_46
            PIN I1 XLXN_45
            PIN O R2in
        END BLOCK
        BEGIN BLOCK XLXI_11 and2b1
            PIN I0 X
            PIN I1 Q4
            PIN O XLXN_45
        END BLOCK
        BEGIN BLOCK XLXI_4 and2
            PIN I0 Q1
            PIN I1 X
            PIN O XLXN_14
        END BLOCK
        BEGIN BLOCK XLXI_5 and3b1
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 X
            PIN O XLXN_15
        END BLOCK
        BEGIN BLOCK XLXI_29 or2
            PIN I0 XLXN_15
            PIN I1 XLXN_14
            PIN O S2in
        END BLOCK
        BEGIN BLOCK XLXI_12 and4b2
            PIN I0 X
            PIN I1 Q2
            PIN I2 Q4
            PIN I3 Q3
            PIN O XLXN_61
        END BLOCK
        BEGIN BLOCK XLXI_13 and4b2
            PIN I0 Q4
            PIN I1 X
            PIN I2 Q3
            PIN I3 Q2
            PIN O XLXN_60
        END BLOCK
        BEGIN BLOCK XLXI_14 and4
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q2
            PIN I3 X
            PIN O XLXN_66
        END BLOCK
        BEGIN BLOCK XLXI_15 and4b3
            PIN I0 Q4
            PIN I1 Q2
            PIN I2 Q1
            PIN I3 X
            PIN O XLXN_67
        END BLOCK
        BEGIN BLOCK XLXI_33 or4
            PIN I0 XLXN_67
            PIN I1 XLXN_66
            PIN I2 XLXN_60
            PIN I3 XLXN_61
            PIN O R3in
        END BLOCK
        BEGIN BLOCK XLXI_6 and4b3
            PIN I0 Q3
            PIN I1 Q2
            PIN I2 X
            PIN I3 Q4
            PIN O XLXN_28
        END BLOCK
        BEGIN BLOCK XLXI_7 and3b1
            PIN I0 Q3
            PIN I1 Q2
            PIN I2 X
            PIN O XLXN_29
        END BLOCK
        BEGIN BLOCK XLXI_30 or2
            PIN I0 XLXN_29
            PIN I1 XLXN_28
            PIN O S3in
        END BLOCK
        BEGIN BLOCK XLXI_285 Flip_Flop_RS
            PIN RST RST
            PIN R R3in
            PIN S S3in
            PIN C CLK
            PIN Q Q3
            PIN not_Q NOT_Q3
        END BLOCK
        BEGIN BLOCK XLXI_8 and3b1
            PIN I0 Q4
            PIN I1 Q2
            PIN I2 X
            PIN O XLXN_36
        END BLOCK
        BEGIN BLOCK XLXI_9 and3b3
            PIN I0 Q4
            PIN I1 Q1
            PIN I2 X
            PIN O XLXN_39
        END BLOCK
        BEGIN BLOCK XLXI_31 or2
            PIN I0 XLXN_36
            PIN I1 XLXN_39
            PIN O S4in
        END BLOCK
        BEGIN BLOCK XLXI_16 and4b3
            PIN I0 Q3
            PIN I1 Q2
            PIN I2 Q1
            PIN I3 Q4
            PIN O XLXN_73
        END BLOCK
        BEGIN BLOCK XLXI_17 and3
            PIN I0 Q4
            PIN I1 Q3
            PIN I2 Q2
            PIN O XLXN_74
        END BLOCK
        BEGIN BLOCK XLXI_34 or3
            PIN I0 XLXN_75
            PIN I1 XLXN_74
            PIN I2 XLXN_73
            PIN O R4in
        END BLOCK
        BEGIN BLOCK XLXI_18 and2
            PIN I0 Q4
            PIN I1 X
            PIN O XLXN_75
        END BLOCK
        BEGIN BLOCK XLXI_286 Flip_Flop_RS
            PIN RST RST
            PIN R R4in
            PIN S S4in
            PIN C CLK
            PIN Q Q4
            PIN not_Q NOT_Q4
        END BLOCK
        BEGIN BLOCK XLXI_284 Flip_Flop_RS
            PIN RST RST
            PIN R R2in
            PIN S S2in
            PIN C CLK
            PIN Q Q2
            PIN not_Q NOT_Q2
        END BLOCK
        BEGIN BLOCK XLXI_283 Flip_Flop_RS
            PIN RST RST
            PIN R R1in
            PIN S S1in
            PIN C CLK
            PIN Q Q1
            PIN not_Q NOT_Q1
        END BLOCK
        BEGIN BLOCK XLXI_43 ibuf
            PIN I X_INPUT
            PIN O X
        END BLOCK
        BEGIN BLOCK XLXI_45 obuf
            PIN I Yin
            PIN O Y
        END BLOCK
        BEGIN BLOCK XLXI_44 ibuf
            PIN I RST_INPUT
            PIN O RST
        END BLOCK
        BEGIN BLOCK XLXI_150 ibufg
            PIN I IN_CLK
            PIN O CLK
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_1 160 400 R0
        INSTANCE XLXI_3 160 720 R0
        INSTANCE XLXI_28 448 496 R0
        BEGIN BRANCH XLXN_1
            WIRE 416 240 432 240
            WIRE 432 240 432 368
            WIRE 432 368 448 368
        END BRANCH
        BEGIN BRANCH XLXN_2
            WIRE 416 528 432 528
            WIRE 432 432 432 528
            WIRE 432 432 448 432
        END BRANCH
        BEGIN BRANCH S1in
            WIRE 704 400 736 400
            WIRE 736 400 736 400
            WIRE 736 400 752 400
            WIRE 752 400 768 400
            BEGIN DISPLAY 744 400 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 80 144 160 144
            BEGIN DISPLAY 80 144 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 80 208 160 208
            BEGIN DISPLAY 80 208 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 272 160 272
            BEGIN DISPLAY 80 272 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 80 336 160 336
            BEGIN DISPLAY 80 336 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 400 160 400
            BEGIN DISPLAY 80 400 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 80 464 160 464
            BEGIN DISPLAY 80 464 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 80 528 160 528
            BEGIN DISPLAY 80 528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 80 592 160 592
            BEGIN DISPLAY 80 592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 80 656 160 656
            BEGIN DISPLAY 80 656 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 80 80 368 80
            BEGIN DISPLAY 80 80 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R1in
            WIRE 592 80 688 80
            BEGIN DISPLAY 688 80 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_35 368 112 R0
        INSTANCE XLXI_10 160 1088 R0
        INSTANCE XLXI_32 464 992 R0
        INSTANCE XLXI_11 160 928 R0
        BEGIN BRANCH X
            WIRE 64 1024 160 1024
            BEGIN DISPLAY 64 1024 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 64 960 160 960
            BEGIN DISPLAY 64 960 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 64 864 160 864
            BEGIN DISPLAY 64 864 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 64 800 160 800
            BEGIN DISPLAY 64 800 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R2in
            WIRE 720 896 768 896
            WIRE 768 896 768 896
            WIRE 768 896 832 896
            BEGIN DISPLAY 772 896 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_46
            WIRE 416 992 432 992
            WIRE 432 928 432 992
            WIRE 432 928 464 928
        END BRANCH
        BEGIN BRANCH XLXN_45
            WIRE 416 832 432 832
            WIRE 432 832 432 864
            WIRE 432 864 464 864
        END BRANCH
        INSTANCE XLXI_4 144 1248 R0
        INSTANCE XLXI_5 144 1440 R0
        INSTANCE XLXI_29 480 1328 R0
        BEGIN BRANCH XLXN_14
            WIRE 400 1152 416 1152
            WIRE 416 1152 432 1152
            WIRE 432 1152 432 1200
            WIRE 432 1200 480 1200
        END BRANCH
        BEGIN BRANCH XLXN_15
            WIRE 400 1312 416 1312
            WIRE 416 1312 432 1312
            WIRE 432 1264 432 1312
            WIRE 432 1264 480 1264
        END BRANCH
        BEGIN BRANCH S2in
            WIRE 736 1232 784 1232
            WIRE 784 1232 816 1232
            BEGIN DISPLAY 784 1232 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 64 1120 144 1120
            BEGIN DISPLAY 64 1120 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 64 1184 144 1184
            BEGIN DISPLAY 64 1184 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 48 1248 144 1248
            BEGIN DISPLAY 48 1248 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 64 1312 144 1312
            BEGIN DISPLAY 64 1312 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 64 1376 144 1376
            BEGIN DISPLAY 64 1376 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_12 192 1920 R0
        INSTANCE XLXI_13 176 2192 R0
        INSTANCE XLXI_14 176 2464 R0
        BEGIN BRANCH Q3
            WIRE 96 1664 192 1664
            BEGIN DISPLAY 96 1664 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 1728 192 1728
            BEGIN DISPLAY 96 1728 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 1792 192 1792
            BEGIN DISPLAY 96 1792 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 1856 192 1856
            BEGIN DISPLAY 96 1856 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 1936 176 1936
            BEGIN DISPLAY 96 1936 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 96 2000 176 2000
            BEGIN DISPLAY 96 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 2064 176 2064
            BEGIN DISPLAY 96 2064 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 2128 176 2128
            BEGIN DISPLAY 96 2128 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_60
            WIRE 432 2032 464 2032
            WIRE 464 2032 464 2160
            WIRE 464 2160 512 2160
        END BRANCH
        BEGIN BRANCH XLXN_61
            WIRE 448 1760 512 1760
            WIRE 512 1760 512 2096
        END BRANCH
        BEGIN BRANCH X
            WIRE 96 2208 176 2208
            BEGIN DISPLAY 96 2208 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 96 2272 176 2272
            BEGIN DISPLAY 96 2272 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 96 2336 176 2336
            BEGIN DISPLAY 96 2336 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 96 2400 176 2400
            BEGIN DISPLAY 96 2400 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_66
            WIRE 432 2304 464 2304
            WIRE 464 2224 464 2304
            WIRE 464 2224 512 2224
        END BRANCH
        BEGIN BRANCH XLXN_67
            WIRE 432 2560 512 2560
            WIRE 512 2288 512 2560
        END BRANCH
        BEGIN BRANCH X
            WIRE 80 2464 176 2464
            BEGIN DISPLAY 80 2464 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 80 2528 176 2528
            BEGIN DISPLAY 80 2528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 80 2592 176 2592
            BEGIN DISPLAY 80 2592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 80 2656 176 2656
            BEGIN DISPLAY 80 2656 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_15 176 2720 R0
        INSTANCE XLXI_33 512 2352 R0
        BEGIN BRANCH R3in
            WIRE 768 2192 800 2192
            WIRE 800 2192 800 2192
            WIRE 800 2192 880 2192
            BEGIN DISPLAY 808 2192 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_6 832 2544 R0
        INSTANCE XLXI_7 832 2720 R0
        INSTANCE XLXI_30 1168 2576 R0
        BEGIN BRANCH Q4
            WIRE 736 2288 832 2288
            BEGIN DISPLAY 736 2288 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 736 2352 832 2352
            BEGIN DISPLAY 736 2352 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 736 2416 832 2416
            BEGIN DISPLAY 736 2416 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 736 2480 832 2480
            BEGIN DISPLAY 736 2480 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_28
            WIRE 1088 2384 1120 2384
            WIRE 1120 2384 1120 2448
            WIRE 1120 2448 1168 2448
        END BRANCH
        BEGIN BRANCH XLXN_29
            WIRE 1088 2592 1120 2592
            WIRE 1120 2512 1120 2592
            WIRE 1120 2512 1168 2512
        END BRANCH
        BEGIN BRANCH S3in
            WIRE 1424 2480 1456 2480
            WIRE 1456 2480 1456 2480
            WIRE 1456 2480 1504 2480
            BEGIN DISPLAY 1452 2480 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 736 2528 832 2528
            BEGIN DISPLAY 736 2528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 736 2592 832 2592
            BEGIN DISPLAY 736 2592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 736 2656 832 2656
            BEGIN DISPLAY 736 2656 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE XLXI_285 1680 2576 R0
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 1600 2352 1648 2352
            WIRE 1648 2352 1680 2352
            BEGIN DISPLAY 1648 2352 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S3in
            WIRE 1600 2480 1648 2480
            WIRE 1648 2480 1680 2480
            BEGIN DISPLAY 1648 2480 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1600 2544 1648 2544
            WIRE 1648 2544 1680 2544
            BEGIN DISPLAY 1648 2544 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 2064 2352 2128 2352
            WIRE 2128 2352 2176 2352
            BEGIN DISPLAY 2128 2352 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH NOT_Q3
            WIRE 2064 2544 2112 2544
            WIRE 2112 2544 2176 2544
            BEGIN DISPLAY 2112 2544 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R3in
            WIRE 1600 2416 1648 2416
            WIRE 1648 2416 1680 2416
            BEGIN DISPLAY 1648 2416 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_8 1264 1568 R0
        INSTANCE XLXI_9 1264 1392 R0
        INSTANCE XLXI_31 1616 1440 R0
        BEGIN BRANCH X
            WIRE 1184 1200 1264 1200
            BEGIN DISPLAY 1184 1200 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 1184 1264 1264 1264
            BEGIN DISPLAY 1184 1264 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_36
            WIRE 1520 1440 1568 1440
            WIRE 1568 1376 1568 1440
            WIRE 1568 1376 1616 1376
        END BRANCH
        BEGIN BRANCH XLXN_39
            WIRE 1520 1264 1568 1264
            WIRE 1568 1264 1568 1312
            WIRE 1568 1312 1616 1312
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 1168 1328 1264 1328
            BEGIN DISPLAY 1168 1328 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1184 1376 1264 1376
            BEGIN DISPLAY 1184 1376 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 1184 1440 1264 1440
            BEGIN DISPLAY 1184 1440 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 1184 1504 1264 1504
            BEGIN DISPLAY 1184 1504 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S4in
            WIRE 1872 1344 1936 1344
            WIRE 1936 1344 1984 1344
            BEGIN DISPLAY 1936 1344 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_16 1264 1840 R0
        INSTANCE XLXI_17 1264 2016 R0
        INSTANCE XLXI_34 1584 2000 R0
        BEGIN BRANCH XLXN_73
            WIRE 1520 1680 1584 1680
            WIRE 1584 1680 1584 1808
        END BRANCH
        BEGIN BRANCH XLXN_74
            WIRE 1520 1888 1552 1888
            WIRE 1552 1872 1552 1888
            WIRE 1552 1872 1584 1872
        END BRANCH
        BEGIN BRANCH XLXN_75
            WIRE 1520 2032 1584 2032
            WIRE 1584 1936 1584 2032
        END BRANCH
        BEGIN BRANCH R4in
            WIRE 1840 1872 1888 1872
            WIRE 1888 1872 1888 1872
            WIRE 1888 1872 1936 1872
            BEGIN DISPLAY 1896 1872 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 1200 1584 1264 1584
            BEGIN DISPLAY 1200 1584 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 1200 1648 1264 1648
            BEGIN DISPLAY 1200 1648 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 1200 1712 1264 1712
            BEGIN DISPLAY 1200 1712 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 1200 1776 1264 1776
            BEGIN DISPLAY 1200 1776 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 1200 1824 1264 1824
            BEGIN DISPLAY 1200 1824 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 1200 1888 1264 1888
            BEGIN DISPLAY 1200 1888 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 1200 1952 1264 1952
            BEGIN DISPLAY 1200 1952 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1200 2000 1264 2000
            BEGIN DISPLAY 1200 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 1200 2064 1264 2064
            BEGIN DISPLAY 1200 2064 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_18 1264 2128 R0
        BEGIN INSTANCE XLXI_286 1952 1712 R0
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 1872 1488 1920 1488
            WIRE 1920 1488 1952 1488
            BEGIN DISPLAY 1920 1488 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S4in
            WIRE 1872 1616 1920 1616
            WIRE 1920 1616 1952 1616
            BEGIN DISPLAY 1920 1616 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1872 1680 1920 1680
            WIRE 1920 1680 1952 1680
            BEGIN DISPLAY 1920 1680 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q4
            WIRE 2336 1488 2400 1488
            WIRE 2400 1488 2448 1488
            BEGIN DISPLAY 2400 1488 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH NOT_Q4
            WIRE 2336 1680 2384 1680
            WIRE 2384 1680 2448 1680
            BEGIN DISPLAY 2384 1680 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R4in
            WIRE 1872 1552 1920 1552
            WIRE 1920 1552 1952 1552
            BEGIN DISPLAY 1920 1552 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE XLXI_284 1040 1120 R0
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 960 896 1008 896
            WIRE 1008 896 1040 896
            BEGIN DISPLAY 1008 896 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S2in
            WIRE 960 1024 1008 1024
            WIRE 1008 1024 1040 1024
            BEGIN DISPLAY 1008 1024 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 960 1088 1008 1088
            WIRE 1008 1088 1040 1088
            BEGIN DISPLAY 1008 1088 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q2
            WIRE 1424 896 1488 896
            WIRE 1488 896 1536 896
            BEGIN DISPLAY 1488 896 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH NOT_Q2
            WIRE 1424 1088 1472 1088
            WIRE 1472 1088 1536 1088
            BEGIN DISPLAY 1472 1088 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R2in
            WIRE 960 960 1008 960
            WIRE 1008 960 1040 960
            BEGIN DISPLAY 1008 960 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE XLXI_283 1040 752 R0
        END INSTANCE
        BEGIN BRANCH RST
            WIRE 960 528 1008 528
            WIRE 1008 528 1040 528
            BEGIN DISPLAY 1008 528 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S1in
            WIRE 960 656 1008 656
            WIRE 1008 656 1040 656
            BEGIN DISPLAY 1008 656 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 960 720 1008 720
            WIRE 1008 720 1040 720
            BEGIN DISPLAY 1008 720 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q1
            WIRE 1424 528 1488 528
            WIRE 1488 528 1536 528
            BEGIN DISPLAY 1488 528 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH NOT_Q1
            WIRE 1424 720 1472 720
            WIRE 1472 720 1536 720
            BEGIN DISPLAY 1472 720 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R1in
            WIRE 960 592 1008 592
            WIRE 1008 592 1040 592
            BEGIN DISPLAY 1008 592 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH X
            WIRE 1584 48 1680 48
            WIRE 1680 48 1680 48
            WIRE 1680 48 1696 48
            WIRE 1696 48 1760 48
            BEGIN DISPLAY 1688 48 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_43 1360 80 R0
        BEGIN BRANCH X_INPUT
            WIRE 1152 48 1360 48
        END BRANCH
        IOMARKER 1152 48 X_INPUT R180 28
        BEGIN BRANCH Y
            WIRE 1440 128 1584 128
        END BRANCH
        INSTANCE XLXI_45 1216 160 R0
        BEGIN BRANCH Yin
            WIRE 1152 128 1200 128
            WIRE 1200 128 1200 128
            WIRE 1200 128 1216 128
            BEGIN DISPLAY 1152 128 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
            BEGIN DISPLAY 1196 128 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH RST_INPUT
            WIRE 1216 352 1328 352
            WIRE 1328 352 1344 352
        END BRANCH
        INSTANCE XLXI_44 1344 384 R0
        BEGIN BRANCH RST
            WIRE 1568 352 1616 352
            WIRE 1616 352 1616 352
            WIRE 1616 352 1696 352
            BEGIN DISPLAY 1620 352 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        IOMARKER 1584 128 Y R0 28
        IOMARKER 1536 720 NOT_Q1 R0 28
        IOMARKER 1536 1088 NOT_Q2 R0 28
        IOMARKER 2448 1680 NOT_Q4 R0 28
        IOMARKER 2176 2544 NOT_Q3 R0 28
        IOMARKER 1216 352 RST_INPUT R180 28
        INSTANCE XLXI_150 1200 272 R0
        BEGIN BRANCH IN_CLK
            WIRE 1168 240 1200 240
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1424 240 1648 240
            WIRE 1648 240 1648 240
            WIRE 1648 240 1776 240
            BEGIN DISPLAY 1652 240 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        IOMARKER 1168 240 IN_CLK R180 28
    END SHEET
END SCHEMATIC
