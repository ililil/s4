#include <iostream>
#include <string>
#include <set>

using namespace std;

template<class T> class MySet: public set<T> {

public:

	using set<T>::set;

	MySet<T> operator-(const T &e) {
		this->erase(e);
		return *this;
	}

	bool operator>(const MySet &e) {
		return includes(this.begin(), this.end(), e.begin(), e.end());
	}

	bool operator!=(const MySet &ms) {
		return ((set<T> ) * this) != ((set<T> ) ms);
	}
};

int main() {

	MySet<int> set12 = {1, 2};
	cout << "set12 =";
	for (int i : set12)
		cout << " " << i;
	cout << endl;

	MySet<int> set123 = {1, 2, 3};
	cout << "set123 =";
	for (int i : set123)
		cout << " " << i;
	cout << endl;

	cout << endl << "set12 < set123 = " << (set12 < set123) << endl;

	MySet<int> set123_2 = {1, 2, 3};
	cout << "set123 != set123_2 = " << (set123 != set123_2) << endl;

	set123 - 1;
	cout << endl << "set123 =";
	for (int i : set123)
		cout << " " << i;
	cout << endl;
	return 0;
}
