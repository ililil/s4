////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2007 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: J.30
//  \   \         Application: netgen
//  /   /         Filename: l3d_timesim.v
// /___/   /\     Timestamp: Fri May 24 14:20:44 2024
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -s 4 -pcf l3d.pcf -sdf_anno true -sdf_path netgen/par -insert_glbl true -w -dir netgen/par -ofmt verilog -sim l3d.ncd l3d_timesim.v 
// Device	: 3s50tq144-4 (PRODUCTION 1.39 2006-10-19)
// Input file	: l3d.ncd
// Output file	: D:\avto\3\shut_3_lab\netgen\par\l3d_timesim.v
// # of Modules	: 1
// Design Name	: l3d
// Xilinx        : C:\Xilinx91i
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Development System Reference Guide, Chapter 23
//     Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module l3d (
  CLK, CE, Y, RST_INPUT, X_INPUT
);
  input CLK;
  input CE;
  output Y;
  input RST_INPUT;
  input X_INPUT;
  wire CE_IBUF_0;
  wire X;
  wire XLXN_97;
  wire GLOBAL_LOGIC1;
  wire XLXN_91;
  wire Q4;
  wire Q2;
  wire Q3;
  wire \XLXI_23/bound/1.0_0 ;
  wire Q1;
  wire XLXN_54_0;
  wire XLXN_15_0;
  wire \XLXI_24/bound/1.0_0 ;
  wire \XLXI_24/bound/free/1.2 ;
  wire \XLXI_23/1.0 ;
  wire XLXN_50;
  wire \CE/INBUF ;
  wire \X_INPUT/INBUF ;
  wire \RST_INPUT/INBUF ;
  wire \Y/O ;
  wire \CLK/INBUF ;
  wire \XLXI_30/S_INVNOT ;
  wire \XLXI_30/I0_INV ;
  wire \XLXI_23/bound/1.0 ;
  wire \Q1/DYMUX_1 ;
  wire D1in;
  wire \Q1/CLKINV_2 ;
  wire \Q1/CEINV_3 ;
  wire Yin;
  wire \Q4/DYMUX_4 ;
  wire D4in;
  wire \Q4/CLKINV_5 ;
  wire \Q4/CEINV_6 ;
  wire XLXN_54;
  wire XLXN_15;
  wire \Q3/DXMUX_7 ;
  wire D3in;
  wire \XLXI_24/bound/free/1.2_pack_1 ;
  wire \Q3/CLKINV_8 ;
  wire \Q3/CEINV_9 ;
  wire \Q2/DXMUX_10 ;
  wire D2in;
  wire \XLXI_23/1.0_pack_1 ;
  wire \Q2/CLKINV_11 ;
  wire \Q2/CEINV_12 ;
  wire \XLXI_24/bound/1.0 ;
  wire XLXN_50_pack_1;
  wire \Q1/FFY/RSTAND_13 ;
  wire \Q4/FFY/RSTAND_14 ;
  wire \Q3/FFX/RSTAND_15 ;
  wire \Q2/FFX/RSTAND_16 ;
  wire GND;
  wire VCC;
  initial $sdf_annotate("netgen/par/l3d_timesim.sdf");
  defparam \CE/PAD .LOC = "PAD102";
  X_IPAD \CE/PAD  (
    .PAD(CE)
  );
  defparam CE_IBUF.LOC = "PAD102";
  X_BUF CE_IBUF (
    .I(CE),
    .O(\CE/INBUF )
  );
  defparam \CE/IFF/IMUX .LOC = "PAD102";
  X_BUF \CE/IFF/IMUX  (
    .I(\CE/INBUF ),
    .O(CE_IBUF_0)
  );
  defparam \X_INPUT/PAD .LOC = "PAD101";
  X_IPAD \X_INPUT/PAD  (
    .PAD(X_INPUT)
  );
  defparam XLXI_29.LOC = "PAD101";
  X_BUF XLXI_29 (
    .I(X_INPUT),
    .O(\X_INPUT/INBUF )
  );
  defparam \X_INPUT/IFF/IMUX .LOC = "PAD101";
  X_BUF \X_INPUT/IFF/IMUX  (
    .I(\X_INPUT/INBUF ),
    .O(X)
  );
  defparam \RST_INPUT/PAD .LOC = "PAD99";
  X_IPAD \RST_INPUT/PAD  (
    .PAD(RST_INPUT)
  );
  defparam XLXI_28.LOC = "PAD99";
  X_BUF XLXI_28 (
    .I(RST_INPUT),
    .O(\RST_INPUT/INBUF )
  );
  defparam \RST_INPUT/IFF/IMUX .LOC = "PAD99";
  X_BUF \RST_INPUT/IFF/IMUX  (
    .I(\RST_INPUT/INBUF ),
    .O(XLXN_97)
  );
  defparam \Y/PAD .LOC = "PAD100";
  X_OPAD \Y/PAD  (
    .PAD(Y)
  );
  defparam XLXI_31.LOC = "PAD100";
  X_OBUF XLXI_31 (
    .I(\Y/O ),
    .O(Y)
  );
  defparam \CLK/PAD .LOC = "PAD14";
  X_IPAD \CLK/PAD  (
    .PAD(CLK)
  );
  defparam CLK_IBUFG.LOC = "PAD14";
  X_BUF CLK_IBUFG (
    .I(CLK),
    .O(\CLK/INBUF )
  );
  defparam XLXI_30.LOC = "BUFGMUX7";
  X_BUFGMUX XLXI_30 (
    .I0(\XLXI_30/I0_INV ),
    .I1(GND),
    .S(\XLXI_30/S_INVNOT ),
    .O(XLXN_91)
  );
  defparam \XLXI_30/SINV .LOC = "BUFGMUX7";
  X_INV \XLXI_30/SINV  (
    .I(GLOBAL_LOGIC1),
    .O(\XLXI_30/S_INVNOT )
  );
  defparam \XLXI_30/I0_USED .LOC = "BUFGMUX7";
  X_BUF \XLXI_30/I0_USED  (
    .I(\CLK/INBUF ),
    .O(\XLXI_30/I0_INV )
  );
  defparam \Q1/XUSED .LOC = "SLICE_X3Y7";
  X_BUF \Q1/XUSED  (
    .I(\XLXI_23/bound/1.0 ),
    .O(\XLXI_23/bound/1.0_0 )
  );
  defparam \Q1/DYMUX .LOC = "SLICE_X3Y7";
  X_BUF \Q1/DYMUX  (
    .I(D1in),
    .O(\Q1/DYMUX_1 )
  );
  defparam \Q1/CLKINV .LOC = "SLICE_X3Y7";
  X_BUF \Q1/CLKINV  (
    .I(XLXN_91),
    .O(\Q1/CLKINV_2 )
  );
  defparam \Q1/CEINV .LOC = "SLICE_X3Y7";
  X_BUF \Q1/CEINV  (
    .I(CE_IBUF_0),
    .O(\Q1/CEINV_3 )
  );
  defparam \Q4/DYMUX .LOC = "SLICE_X2Y6";
  X_BUF \Q4/DYMUX  (
    .I(D4in),
    .O(\Q4/DYMUX_4 )
  );
  defparam \Q4/CLKINV .LOC = "SLICE_X2Y6";
  X_BUF \Q4/CLKINV  (
    .I(XLXN_91),
    .O(\Q4/CLKINV_5 )
  );
  defparam \Q4/CEINV .LOC = "SLICE_X2Y6";
  X_BUF \Q4/CEINV  (
    .I(CE_IBUF_0),
    .O(\Q4/CEINV_6 )
  );
  defparam \XLXN_54/XUSED .LOC = "SLICE_X2Y7";
  X_BUF \XLXN_54/XUSED  (
    .I(XLXN_54),
    .O(XLXN_54_0)
  );
  defparam \XLXN_54/YUSED .LOC = "SLICE_X2Y7";
  X_BUF \XLXN_54/YUSED  (
    .I(XLXN_15),
    .O(XLXN_15_0)
  );
  defparam XLXI_5.INIT = 16'h1100;
  defparam XLXI_5.LOC = "SLICE_X2Y7";
  X_LUT4 XLXI_5 (
    .ADR0(Q4),
    .ADR1(Q3),
    .ADR2(VCC),
    .ADR3(Q2),
    .O(XLXN_15)
  );
  defparam \Q3/DXMUX .LOC = "SLICE_X2Y8";
  X_BUF \Q3/DXMUX  (
    .I(D3in),
    .O(\Q3/DXMUX_7 )
  );
  defparam \Q3/YUSED .LOC = "SLICE_X2Y8";
  X_BUF \Q3/YUSED  (
    .I(\XLXI_24/bound/free/1.2_pack_1 ),
    .O(\XLXI_24/bound/free/1.2 )
  );
  defparam \Q3/CLKINV .LOC = "SLICE_X2Y8";
  X_BUF \Q3/CLKINV  (
    .I(XLXN_91),
    .O(\Q3/CLKINV_8 )
  );
  defparam \Q3/CEINV .LOC = "SLICE_X2Y8";
  X_BUF \Q3/CEINV  (
    .I(CE_IBUF_0),
    .O(\Q3/CEINV_9 )
  );
  defparam \Q2/DXMUX .LOC = "SLICE_X3Y6";
  X_BUF \Q2/DXMUX  (
    .I(D2in),
    .O(\Q2/DXMUX_10 )
  );
  defparam \Q2/YUSED .LOC = "SLICE_X3Y6";
  X_BUF \Q2/YUSED  (
    .I(\XLXI_23/1.0_pack_1 ),
    .O(\XLXI_23/1.0 )
  );
  defparam \Q2/CLKINV .LOC = "SLICE_X3Y6";
  X_BUF \Q2/CLKINV  (
    .I(XLXN_91),
    .O(\Q2/CLKINV_11 )
  );
  defparam \Q2/CEINV .LOC = "SLICE_X3Y6";
  X_BUF \Q2/CEINV  (
    .I(CE_IBUF_0),
    .O(\Q2/CEINV_12 )
  );
  defparam \XLXI_24/bound/1.0/XUSED .LOC = "SLICE_X2Y9";
  X_BUF \XLXI_24/bound/1.0/XUSED  (
    .I(\XLXI_24/bound/1.0 ),
    .O(\XLXI_24/bound/1.0_0 )
  );
  defparam \XLXI_24/bound/1.0/YUSED .LOC = "SLICE_X2Y9";
  X_BUF \XLXI_24/bound/1.0/YUSED  (
    .I(XLXN_50_pack_1),
    .O(XLXN_50)
  );
  defparam XLXI_12.INIT = 16'h0004;
  defparam XLXI_12.LOC = "SLICE_X2Y9";
  X_LUT4 XLXI_12 (
    .ADR0(Q4),
    .ADR1(X),
    .ADR2(Q1),
    .ADR3(Q3),
    .O(XLXN_50_pack_1)
  );
  defparam XLXI_22.INIT = 16'h0480;
  defparam XLXI_22.LOC = "SLICE_X3Y7";
  X_LUT4 XLXI_22 (
    .ADR0(Q2),
    .ADR1(Q3),
    .ADR2(X),
    .ADR3(Q4),
    .O(D1in)
  );
  defparam XLXI_25.INIT = 16'h1719;
  defparam XLXI_25.LOC = "SLICE_X2Y6";
  X_LUT4 XLXI_25 (
    .ADR0(Q4),
    .ADR1(X),
    .ADR2(Q3),
    .ADR3(Q2),
    .O(D4in)
  );
  defparam \XLXI_24/bound/free/bound .INIT = 16'h5450;
  defparam \XLXI_24/bound/free/bound .LOC = "SLICE_X2Y8";
  X_LUT4 \XLXI_24/bound/free/bound  (
    .ADR0(X),
    .ADR1(Q2),
    .ADR2(Q1),
    .ADR3(Q3),
    .O(\XLXI_24/bound/free/1.2_pack_1 )
  );
  defparam \XLXI_23/bound .INIT = 16'hFEEE;
  defparam \XLXI_23/bound .LOC = "SLICE_X3Y6";
  X_LUT4 \XLXI_23/bound  (
    .ADR0(\XLXI_23/bound/1.0_0 ),
    .ADR1(XLXN_15_0),
    .ADR2(X),
    .ADR3(Q1),
    .O(\XLXI_23/1.0_pack_1 )
  );
  defparam \XLXI_27/I_Q0 .LOC = "SLICE_X3Y7";
  defparam \XLXI_27/I_Q0 .INIT = 1'b0;
  X_FF \XLXI_27/I_Q0  (
    .I(\Q1/DYMUX_1 ),
    .CE(\Q1/CEINV_3 ),
    .CLK(\Q1/CLKINV_2 ),
    .SET(GND),
    .RST(\Q1/FFY/RSTAND_13 ),
    .O(Q1)
  );
  defparam \Q1/FFY/RSTAND .LOC = "SLICE_X3Y7";
  X_BUF \Q1/FFY/RSTAND  (
    .I(XLXN_97),
    .O(\Q1/FFY/RSTAND_13 )
  );
  defparam \XLXI_23/bound/bound .INIT = 16'hC840;
  defparam \XLXI_23/bound/bound .LOC = "SLICE_X3Y7";
  X_LUT4 \XLXI_23/bound/bound  (
    .ADR0(Q2),
    .ADR1(X),
    .ADR2(Q3),
    .ADR3(Q4),
    .O(\XLXI_23/bound/1.0 )
  );
  defparam \XLXI_27/I_Q3 .LOC = "SLICE_X2Y6";
  defparam \XLXI_27/I_Q3 .INIT = 1'b0;
  X_FF \XLXI_27/I_Q3  (
    .I(\Q4/DYMUX_4 ),
    .CE(\Q4/CEINV_6 ),
    .CLK(\Q4/CLKINV_5 ),
    .SET(GND),
    .RST(\Q4/FFY/RSTAND_14 ),
    .O(Q4)
  );
  defparam \Q4/FFY/RSTAND .LOC = "SLICE_X2Y6";
  X_BUF \Q4/FFY/RSTAND  (
    .I(XLXN_97),
    .O(\Q4/FFY/RSTAND_14 )
  );
  defparam XLXI_26.INIT = 16'h6C6C;
  defparam XLXI_26.LOC = "SLICE_X2Y6";
  X_LUT4 XLXI_26 (
    .ADR0(Q4),
    .ADR1(X),
    .ADR2(Q3),
    .ADR3(VCC),
    .O(Yin)
  );
  defparam XLXI_8.INIT = 16'h0404;
  defparam XLXI_8.LOC = "SLICE_X2Y7";
  X_LUT4 XLXI_8 (
    .ADR0(Q4),
    .ADR1(Q3),
    .ADR2(X),
    .ADR3(VCC),
    .O(XLXN_54)
  );
  defparam XLXI_24.INIT = 16'hFFFC;
  defparam XLXI_24.LOC = "SLICE_X2Y8";
  X_LUT4 XLXI_24 (
    .ADR0(VCC),
    .ADR1(\XLXI_24/bound/1.0_0 ),
    .ADR2(XLXN_54_0),
    .ADR3(\XLXI_24/bound/free/1.2 ),
    .O(D3in)
  );
  defparam \XLXI_27/I_Q2 .LOC = "SLICE_X2Y8";
  defparam \XLXI_27/I_Q2 .INIT = 1'b0;
  X_FF \XLXI_27/I_Q2  (
    .I(\Q3/DXMUX_7 ),
    .CE(\Q3/CEINV_9 ),
    .CLK(\Q3/CLKINV_8 ),
    .SET(GND),
    .RST(\Q3/FFX/RSTAND_15 ),
    .O(Q3)
  );
  defparam \Q3/FFX/RSTAND .LOC = "SLICE_X2Y8";
  X_BUF \Q3/FFX/RSTAND  (
    .I(XLXN_97),
    .O(\Q3/FFX/RSTAND_15 )
  );
  defparam XLXI_23.INIT = 16'hF0F2;
  defparam XLXI_23.LOC = "SLICE_X3Y6";
  X_LUT4 XLXI_23 (
    .ADR0(Q2),
    .ADR1(X),
    .ADR2(\XLXI_23/1.0 ),
    .ADR3(Q4),
    .O(D2in)
  );
  defparam \XLXI_27/I_Q1 .LOC = "SLICE_X3Y6";
  defparam \XLXI_27/I_Q1 .INIT = 1'b0;
  X_FF \XLXI_27/I_Q1  (
    .I(\Q2/DXMUX_10 ),
    .CE(\Q2/CEINV_12 ),
    .CLK(\Q2/CLKINV_11 ),
    .SET(GND),
    .RST(\Q2/FFX/RSTAND_16 ),
    .O(Q2)
  );
  defparam \Q2/FFX/RSTAND .LOC = "SLICE_X3Y6";
  X_BUF \Q2/FFX/RSTAND  (
    .I(XLXN_97),
    .O(\Q2/FFX/RSTAND_16 )
  );
  defparam \XLXI_24/bound/bound .INIT = 16'hF0F8;
  defparam \XLXI_24/bound/bound .LOC = "SLICE_X2Y9";
  X_LUT4 \XLXI_24/bound/bound  (
    .ADR0(X),
    .ADR1(Q2),
    .ADR2(XLXN_50),
    .ADR3(Q3),
    .O(\XLXI_24/bound/1.0 )
  );
  X_ONE GLOBAL_LOGIC1_VCC (
    .O(GLOBAL_LOGIC1)
  );
  defparam \Y/OUTPUT/OFF/OMUX .LOC = "PAD100";
  X_BUF \Y/OUTPUT/OFF/OMUX  (
    .I(Yin),
    .O(\Y/O )
  );
  X_ZERO NlwBlock_l3d_GND (
    .O(GND)
  );
  X_ONE NlwBlock_l3d_VCC (
    .O(VCC)
  );
endmodule


`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

    wire GSR;
    wire GTS;
    wire PRLD;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

